@Library('jenkins-shared-library')_

def componentName = "kyr-frontend"
def compomentNamespace = "kyr"
def clientName = "casa"
def solutionName = "casa-kyr-kyr-solution"


def label = "${clientName}-${componentName}-${env.BUILD_NUMBER}"

timestamps {
  podTemplate(label: label, serviceAccount: 'jenkins', slaveConnectTimeout: 120, name: label,
    containers: [
      containerTemplate(name: 'cacd2-angular', image: 'docker.factory.prod.cacd2.io/cacd2/build-images/angular:1.0.0', ttyEnabled: true, command: 'cat'),
      containerTemplate(name: 'angular-chrome', image: 'docker.factory.prod.cacd2.io/cacd2/build-images/rm-frontend-angular', ttyEnabled: true, command: 'cat'),
      containerTemplate(name: 'docker', image: 'docker:stable', ttyEnabled: true, command: 'cat')
    ],
    imagePullSecrets: [ 'regcred-docker' ],
    volumes: [
      hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock'),
    ]
    ) {

    node(label) {
      def componentVersion = ""
      def branchName = ""
      def componentHash = ""
      def commitMessage = ""
      env.CACD2_DOCKER_REGISTRY = cacd2GetDockerRegistry('docker') // Patch for new Docker Registry
      env.DOCKER_REGISTRY_REPOSITORY = "${cacd2GetDockerRegistry()}/${clientName}/${compomentNamespace}/${componentName}"

      try {
        stage("Checkout") {
          def scmRet = cacd2GitCheckout()
          branchName = scmRet[0]
          componentHash = scmRet[1]
          commitMessage = scmRet[2]

          sh "env && pwd && ls -l"
          container('angular-chrome') { sh "node -v && npm -v" }
          container('docker') { sh "docker version && docker ps" }
        }

        stage("Build") {
          container('angular-chrome') {
            sh "env"
            sh "npm install"
            componentVersion = cacd2GetComponentVersion("angular")
            sh "npm run build:prod"
          }
        }

        stage("Tests and Analysis") {
          parallel testsAndSonar: {
            stage('Unit Tests') {
              container('angular-chrome') {
                sh "npm run test-headless"
              }
            }

            stage('Integration Tests') {
              echo 'Doing Integration Tests...'
            }

            stage('SonarQube Analysis') {
              cacd2ScanSonarqube("angular")
            }
          }, security: {
            if (env.forcedSecurityScan) {
              stage('Security Analysis') {
                cacd2CheckDependencies()
              }
            }
          }
        }

        stage("Tagging") {
          cacd2GitTagComponent(branchName, componentVersion)
        }

        // If explicit commit message "#docker" (Build a HASH tag docker image)
        if (commitMessage.contains("#docker")) {
          container('docker') {
            stage("Build Docker Image") {
                env.DOCKER_REGISTRY_IMAGE_TAG_HASH = "${DOCKER_REGISTRY_REPOSITORY}:${componentHash}"
                env.DOCKER_BUILD_ARGS = ""
                sh "docker build --label 'image.source.version=${componentHash}' ${DOCKER_BUILD_ARGS} -t $DOCKER_REGISTRY_IMAGE_TAG_HASH ."
            }

            stage("Push Docker Image") {
              cacd2DockerPushImages(DOCKER_REGISTRY_IMAGE_TAG_HASH)
            }

            stage("Scan Docker Image") {
              cacd2DockerScanImage(env.DOCKER_REGISTRY_IMAGE_TAG_HASH, componentHash)
            }
          }
        }

        // US branch
        if (cacd2IsUserStoryBranch(branchName)) {
          container('docker') {
            stage("Build Docker Image") {
                env.DOCKER_REGISTRY_IMAGE_TAG_US = "${DOCKER_REGISTRY_REPOSITORY}:${cacd2GetUserStoryBranchName(branchName)}"
                env.DOCKER_BUILD_ARGS = ""
                sh "docker build --label 'image.source.version=${componentHash}' ${DOCKER_BUILD_ARGS} -t $DOCKER_REGISTRY_IMAGE_TAG_US ."
            }

            stage("Push Docker Image") {
              cacd2DockerPushImages(DOCKER_REGISTRY_IMAGE_TAG_US)
            }

            if (env.forcedSecurityScan) {
              stage("Scan Docker Image") {
                cacd2DockerScanImage(env.DOCKER_REGISTRY_IMAGE_TAG_US, componentHash)
              }
            }
          }
        }

        // Tag several Docker images
        if (branchName == "master") {
          container('docker') {
            stage("Build Docker Image") {
              env.DOCKER_REGISTRY_IMAGE_TAG_VERSION = "${DOCKER_REGISTRY_REPOSITORY}:${componentVersion}"
              env.DOCKER_REGISTRY_IMAGE_TAG_LATEST = "${DOCKER_REGISTRY_REPOSITORY}:latest"
              env.DOCKER_REGISTRY_IMAGE_TAG_HASH = "${DOCKER_REGISTRY_REPOSITORY}:${componentHash}"
              env.DOCKER_REGISTRY_IMAGE_TAG_ALL_PATCHES = "${DOCKER_REGISTRY_REPOSITORY}:${cacd2GetVersionForAllPatches(componentVersion)}"
              env.DOCKER_REGISTRY_IMAGE_TAG_ALL_MINOR_FIXES = "${DOCKER_REGISTRY_REPOSITORY}:${cacd2GetVersionForAllMinorFixes(componentVersion)}"
              env.DOCKER_BUILD_ARGS = ""
              sh "docker build --label 'image.source.version=${componentHash}' ${DOCKER_BUILD_ARGS} -t $DOCKER_REGISTRY_IMAGE_TAG_VERSION -t $DOCKER_REGISTRY_IMAGE_TAG_LATEST -t $DOCKER_REGISTRY_IMAGE_TAG_HASH -t $DOCKER_REGISTRY_IMAGE_TAG_ALL_PATCHES -t $DOCKER_REGISTRY_IMAGE_TAG_ALL_MINOR_FIXES ."
            }

            stage("Push Docker Image") {
              cacd2DockerPushImages(DOCKER_REGISTRY_IMAGE_TAG_VERSION,
                                    DOCKER_REGISTRY_IMAGE_TAG_LATEST,
                                    DOCKER_REGISTRY_IMAGE_TAG_HASH,
                                    DOCKER_REGISTRY_IMAGE_TAG_ALL_PATCHES,
                                    DOCKER_REGISTRY_IMAGE_TAG_ALL_MINOR_FIXES)
            }

            if (env.forcedSecurityScan) {
              stage("Scan Docker Image") {
                cacd2DockerScanImage(env.DOCKER_REGISTRY_IMAGE_TAG_HASH, componentHash)
              }
            }
          }
        }

        // Trigger solution
        // if (cacd2IsUserStoryBranch(branchName)) {
        //   build job: solutionName,
        //       parameters: [
        //         string(name: 'forcedUserStory', value: cacd2GetUserStoryBranchName(branchName))
        //     ], wait: false
        // }

        currentBuild.result = 'SUCCESS'
      } catch (e) {
        currentBuild.result = 'FAILURE'

      } finally {
        stage("Build Notifications") {
          //cacd2NotifySlackForBuildStatus("#dev-pic", currentBuild.result)

          cacd2NotifyEmail()

          // Gitlab
          if (currentBuild.result == "SUCCESS") {
            cacd2PushGitlabCommitStatus("cacd2/${clientName}/${compomentNamespace}/${componentName}", componentHash, "${env.BUILD_URL}", "success")
          } else {
            cacd2PushGitlabCommitStatus("cacd2/${clientName}/${compomentNamespace}/${componentName}", componentHash, "${env.BUILD_URL}", "failed")
          }
        }
      }

    }
  }
}

