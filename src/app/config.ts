export interface Config {
    apiAnalysisUrl: string;
    apiUserUrl: string;
    animationAssets: any;
    keycloakUrl: string;
    keycloakRealm: string;
    keycloakClientId: string;
    keycloakSecret: string;
}
