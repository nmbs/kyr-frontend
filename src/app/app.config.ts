import { forkJoin as observableForkJoin, throwError } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Config } from './config';
import { KeycloakConfig, KeycloakService } from 'keycloak-angular';


@Injectable()
export class AppConfig {

  private _config: Config = null;
  private _checkAnimation = '/assets/animations/check.json';
  private _popinCheckAnimation = '/assets/animations/popin-check.json';
  private _createPageAnimation = '/assets/animations/create/create.json';
  private _pageNotFoundAnimation = '/assets/animations/404/page-not-found.json';
  private _logoutAnimation = '/assets/animations/logout.json';
  private _pictoCloseAnimation = '/assets/animations/picto-close.json';
  private _addContributorClick = '/assets/animations/add-contributor/add-contributor-click.json';
  private _addContributorHover = '/assets/animations/add-contributor/add-contributor-hover-in.json';
  private _addContributorHoverOut = '/assets/animations/add-contributor/add-contributor-hover-out.json';
  private _addClickAnimation = '/assets/animations/add/add-click.json';
  private _addHoverAnimation = '/assets/animations/add/add-hover.json';
  private _addHoverOutAnimation = '/assets/animations/add/add-hover-out.json';
  private _deleteClickAnimation = '/assets/animations/delete-button/delete-click.json';
  private _deleteHoverAnimation = '/assets/animations/delete-button/delete-hover.json';
  private _deleteHoverOutAnimation = '/assets/animations/delete-button/delete-hover-out.json';
  private _deleteTextLessHoverOutAnimation = '/assets/animations/delete-button/delete-textless-hover-out.json';
  private _deleteTextLessHoverAnimation = '/assets/animations/delete-button/delete-textless-hover.json';
  private _modifyTextLessHoverAnimation = '/assets/animations/modify/modify-textless-hover.json';
  private _modifyTextLessHoverOutAnimation = '/assets/animations/modify/modify-textless-hover-out.json';
  private _modifyHoverAnimation = '/assets/animations/modify/modify-hover.json';
  private _modifyHoverOutAnimation = '/assets/animations/modify/modify-hover-out.json';
  private _onboardingFeature = '/assets/animations/onboarding/onboarding-feature.json';
  private _onboardingDescription = '/assets/animations/onboarding/onboarding-description.json';
  private _onboardingRisks = '/assets/animations/onboarding/onboarding-risks.json';
  private _onboardingMeasures = '/assets/animations/onboarding/onboarding-measures.json';
  private _addMeasureClickAnimation = '/assets/animations/add-measure/add-measure-click.json';
  private _addMeasureHoverInAnimation = '/assets/animations/add-measure/add-measure-hover-in.json';
  private _addMeasureHoverOutAnimation = '/assets/animations/add-measure/add-measure-hover-out.json';

  private _keycloakConfig: KeycloakConfig;

  constructor(private readonly _http: HttpClient, private keycloakService: KeycloakService) {
  }

  public getConfig(): Config {
    return this._config;
  }

  private loadConfig() {
    return this._http.get<Config>('/assets/config/config.json');
  }

  private loadAnimationAssets(path: string) {
    return this._http.get<any>(path);
  }

  // Load config
  public load(): Promise<Config> {
    return new Promise((resolve, reject) => {
      observableForkJoin([this.loadConfig(),
      this.loadAnimationAssets(this._checkAnimation),
      this.loadAnimationAssets(this._logoutAnimation),
      this.loadAnimationAssets(this._createPageAnimation),
      this.loadAnimationAssets(this._pictoCloseAnimation),
      this.loadAnimationAssets(this._addContributorClick),
      this.loadAnimationAssets(this._addContributorHover),
      this.loadAnimationAssets(this._addContributorHoverOut),
      this.loadAnimationAssets(this._addClickAnimation),
      this.loadAnimationAssets(this._addHoverAnimation),
      this.loadAnimationAssets(this._addHoverOutAnimation),
      this.loadAnimationAssets(this._deleteClickAnimation),
      this.loadAnimationAssets(this._deleteHoverAnimation),
      this.loadAnimationAssets(this._deleteHoverOutAnimation),
      this.loadAnimationAssets(this._deleteTextLessHoverOutAnimation),
      this.loadAnimationAssets(this._deleteTextLessHoverAnimation),
      this.loadAnimationAssets(this._modifyTextLessHoverAnimation),
      this.loadAnimationAssets(this._modifyTextLessHoverOutAnimation),
      this.loadAnimationAssets(this._onboardingFeature),
      this.loadAnimationAssets(this._onboardingDescription),
      this.loadAnimationAssets(this._addMeasureClickAnimation),
      this.loadAnimationAssets(this._addMeasureHoverInAnimation),
      this.loadAnimationAssets(this._addMeasureHoverOutAnimation),
      this.loadAnimationAssets(this._pageNotFoundAnimation),
      this.loadAnimationAssets(this._modifyHoverAnimation),
      this.loadAnimationAssets(this._modifyHoverOutAnimation),
      this.loadAnimationAssets(this._popinCheckAnimation),
      this.loadAnimationAssets(this._onboardingRisks),
      this.loadAnimationAssets(this._onboardingMeasures)]).subscribe(
        res => {
          this._config = res[0];
          this._config.animationAssets = [];
          this._config.animationAssets['check'] = res[1];
          this._config.animationAssets['logout'] = res[2];
          this._config.animationAssets['create'] = res[3];
          this._config.animationAssets['picto-close'] = res[4];
          this._config.animationAssets['add-contributor-click'] = res[5];
          this._config.animationAssets['add-contributor-hover-in'] = res[6];
          this._config.animationAssets['add-contributor-hover-out'] = res[7];
          this._config.animationAssets['add-click'] = res[8];
          this._config.animationAssets['add-hover'] = res[9];
          this._config.animationAssets['add-hover-out'] = res[10];
          this._config.animationAssets['delete-click'] = res[11];
          this._config.animationAssets['delete-hover'] = res[12];
          this._config.animationAssets['delete-hover-out'] = res[13];
          this._config.animationAssets['delete-textless-hover-out'] = res[14];
          this._config.animationAssets['delete-textless-hover'] = res[15];
          this._config.animationAssets['modify-textless-hover'] = res[16];
          this._config.animationAssets['modify-textless-hover-out'] = res[17];
          this._config.animationAssets['onboarding-feature'] = res[18];
          this._config.animationAssets['onboarding-impact'] = res[19];
          this._config.animationAssets['add-measure-click'] = res[20];
          this._config.animationAssets['add-measure-hover-in'] = res[21];
          this._config.animationAssets['add-measure-hover-out'] = res[22];
          this._config.animationAssets['page-not-found'] = res[23];
          this._config.animationAssets['modify-hover'] = res[24];
          this._config.animationAssets['modify-hover-out'] = res[25];
          this._config.animationAssets['popin-check'] = res[26];
          this._config.animationAssets['onboarding-risks'] = res[27];
          this._config.animationAssets['onboarding-measures'] = res[28];
          resolve(this._config);
        },
        error => {
          return throwError(error);
        });
    });
  }

  public getKeycloakConfig() {
    return this._keycloakConfig;
  }

}
