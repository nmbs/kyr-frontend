import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER, LOCALE_ID } from '@angular/core';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { initializer } from './app-init';
import { AppConfig } from './app.config';
import { FormsModule } from '@angular/forms';
import { AnalysisGlobalService } from './analysis/analysisGlobal.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddUsersComponent } from './analysis/add-users/add-users.component';

import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { UserGlobalService } from './services/userGlobal.service';
import { GraphicModule } from './graphic-components/graphic.module';
import { UserService } from './services/user.service';
import { ProfilModule } from './profile/profile.module';
import { CacheInterceptor } from './helpers/http.interceptor';
import { MatDialogModule } from '@angular/material';
import localeFr from '@angular/common/locales/fr';
import { registerLocaleData } from '@angular/common';
import { ScrollingModule } from '@angular/cdk/scrolling';

registerLocaleData(localeFr);

@NgModule({
  declarations: [
    AppComponent,
    AddUsersComponent
    ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    KeycloakAngularModule,
    ProfilModule,
    GraphicModule,
    MatDialogModule,
    ScrollingModule
    ],
  entryComponents: [
    AddUsersComponent
  ],
  providers: [
    AppConfig,
    { provide: LOCALE_ID, useValue: 'fr-FR' },
    { provide: HTTP_INTERCEPTORS, useClass: CacheInterceptor, multi: true },
    {
      provide: APP_INITIALIZER,
      useFactory: initializer,
      multi: true,
      deps: [AppConfig, KeycloakService]
    },
    HttpClient,
    UserGlobalService,
    UserService,
    AnalysisGlobalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
