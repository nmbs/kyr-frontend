import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlidersQcmComponent } from './sliders-qcm.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SliderQcmService } from './sliders-qcm.service';
import { AppConfig } from '../../app.config';
import { RouterTestingModule } from '@angular/router/testing';
import { AnalysisGlobalService } from '../../analysis/analysisGlobal.service';
import { Impact } from 'src/app/analysis/models/impact.model';
import { analysisImpact } from '../../../assets/analysis-impact-form/impacts';
import { Slider } from 'src/app/analysis/models/slider.model';
import { of } from 'rxjs';

const mockDicp = [
  { title: 'Indisponibilité du produit', subtitle: item => `Si ${item} n’était plus accessible ?`, valueToChange: 'd', startingValue: 0 },
  { title: 'Altération des données', subtitle: item => `Si les données de ${item} étaient erronées ?`, valueToChange: 'i', startingValue: 0 },
  { title: 'Fuite des données', subtitle: item => `Si les données de ${item} étaient rendues publiques ?`, valueToChange: 'c', startingValue: 0 },
  { title: 'Absence de preuve', subtitle: item => `Si vous ne pouviez plus retracer l’historique de ${item} ?`, valueToChange: 'p', startingValue: 0 }
];

const mockEvent1 = {
  valueToChange: 'd',
  bulletNb: 2
};
const mockEvent2 = {
  valueToChange: 'i',
  bulletNb: 4
};
const mockEvent3 = {
  valueToChange: 'c',
  bulletNb: 3
};
const mockEvent4 = {
  valueToChange: 'p',
  bulletNb: 1
};
const mockImpact: Impact = {
  imp: {
    imp_id: 'imp_id',
    imp_label: 'imp_label'
  },
  ira_d: 1,
  ira_i: 2,
  ira_c: 3,
  ira_p: 4,
  ira_comment: 'commebt'
};

const mockSlider: Slider = {
  title: () => 'title',
  subtitle: () => 'subtitle',
  valueToChange: 'd',
  startingValue: 1
};

class MockSliderService {
  getMaxDicp(guid) {
    return of(mockImpact);
  }
  sendDICP(guid, body) {
    return of('OK');
  }
}

const mockQuestion = analysisImpact.steps[0].questions[0];

describe('SlidersQcmComponent', () => {
  let component: SlidersQcmComponent;
  let fixture: ComponentFixture<SlidersQcmComponent>;
  let service: SliderQcmService;

  beforeEach(async(() => {
    const appConfigSpy = jasmine.createSpyObj('AppConfig', {
      getConfig: function () {
        return '';
      }
    });

    const impactLabelSpy = jasmine.createSpyObj('getImpactByLabel', {
      getImpactByLabel: function getImpactByLabel() {
        const retVal = {
          imp_id: '',
          imp_label: 'financial'
        };
        return {
          success: function (fn) {
            fn(retVal);
          }
        };
      }
    });

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [SlidersQcmComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        AnalysisGlobalService,
        { provide: SliderQcmService, useValue: MockSliderService },
        { provide: AppConfig, useValue: appConfigSpy }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlidersQcmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    service = TestBed.get(SliderQcmService);
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    beforeEach(() => {
      component.question = mockQuestion;
      component.impact = mockImpact;
    });

    it('no setDicp', () => {
      component.readMode = true;
      component.ngOnInit();
      expect(component.dicp[0].startingValue).toEqual(0);
      expect(component.dicp[1].startingValue).toEqual(0);
      expect(component.dicp[2].startingValue).toEqual(0);
      expect(component.dicp[3].startingValue).toEqual(0);
    });

    it('setDicp', () => {
      component.readMode = false;
      component.ngOnInit();
      expect(component.dicp[0].startingValue).toEqual(mockImpact.ira_d);
      expect(component.dicp[1].startingValue).toEqual(mockImpact.ira_i);
      expect(component.dicp[2].startingValue).toEqual(mockImpact.ira_c);
      expect(component.dicp[3].startingValue).toEqual(mockImpact.ira_p);
    });
  });

  describe('getStartingValue', () => {
    beforeEach(() => {
      component.question = mockQuestion;
      component.impact = mockImpact;
    });

    it('getStartingValue no readMode', () => {
      component.readMode = false;
      component.getStartingValue(mockSlider);
      expect(component.getStartingValue(mockSlider)).toEqual(mockSlider.startingValue);
    });

    it('getStartingValue readMode', () => {
      component.readMode = true;
      component.getStartingValue(mockSlider);
      expect(component.getStartingValue(mockSlider)).toEqual(mockImpact.ira_d);
    });

  });

  describe('changeDicp', () => {
    beforeEach(() => {
      component.question = mockQuestion;
      component.impact = mockImpact;
      component.readMode = false;
      component.ngOnInit();
    });

    it('change d to 2', () => {
      component.changeDicp(mockEvent1);
      expect(component.getCurrentImpact().ira_d).toEqual(2);
    });
    it('change i to 4', () => {
      component.changeDicp(mockEvent2);
      expect(component.getCurrentImpact().ira_i).toEqual(4);
    });
    it('change c to 3', () => {
      component.changeDicp(mockEvent3);
      expect(component.getCurrentImpact().ira_c).toEqual(3);
    });
    it('change p to 1', () => {
      component.changeDicp(mockEvent4);
      expect(component.getCurrentImpact().ira_p).toEqual(1);
    });
  });

  describe('modifyInput', () => {
    beforeEach(() => {
      component.question = mockQuestion;
      component.impact = mockImpact;
      component.readMode = false;
      component.ngOnInit();
    });

    it('change comment', () => {
      component.modifyInput('testComment');
      expect(component.impact.ira_comment).toEqual('testComment');
    });
  });
});
