import { Injectable } from '@angular/core';
import { AppConfig } from '../../app.config';
import { HttpClient } from '@angular/common/http';
import { HttpHelper } from '../../helpers/http.helper';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Impact } from '../../analysis/models/impact.model';

@Injectable()
export class SliderQcmService {
  private _api: string;
  private _urlAnalysis: string;
  private _urlImpact: string;

  constructor(private _http: HttpClient, private appConfig: AppConfig, private httpHelper: HttpHelper) {
    this._api = appConfig.getConfig().apiAnalysisUrl;
    this._urlAnalysis = this._api + '/analyses';
    this._urlImpact = this._api + '/impacts';
  }

  // Update DICP
  public sendDICP(guid, body): Observable<any> {
    return this._http.patch(this._urlAnalysis + '/' + guid + '/Impacts', body,
     { headers: HttpHelper.getHeaders() }).pipe(catchError(HttpHelper.handleError));
  }

  // Get max of each dicp
  public getMaxDicp(guid): Observable<any> {
    return this._http.get<Impact>(this._urlAnalysis + '/' + guid + '/globaldicp/', { headers: HttpHelper.getHeaders() })
      .pipe(
        catchError(HttpHelper.handleError)
      );
  }
}
