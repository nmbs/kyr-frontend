import { Component, OnInit, Input } from '@angular/core';
import { Slider } from 'src/app/analysis/models/slider.model';
import { SliderQcmService } from './sliders-qcm.service';
import { Impact } from 'src/app/analysis/models/impact.model';
import { Question } from 'src/app/analysis/models/question.model';
import { AnalysisGlobalService } from 'src/app/analysis/analysisGlobal.service';

@Component({
  selector: 'app-sliders-qcm',
  templateUrl: './sliders-qcm.component.html',
  styleUrls: ['./sliders-qcm.component.scss'],
  providers: [SliderQcmService]
})
export class SlidersQcmComponent implements OnInit {

  @Input()
  public guid: string;

  @Input()
  public noSliders = false;


  @Input()
  set question(question: Question) {
    question.sliders.forEach(element => {
      this.dicp.push(Object.assign({}, element));
    });
    this._question = question;
  }

  @Input()
  public dicpValues: Array<Slider>;

  @Input()
  set readMode(value: Boolean) {
    this._readMode = value;
    if (this._readMode) {
      this.sliderService.getMaxDicp(this.guid).subscribe(res => {
        this._maxImpact = res;
        this.setDicp(this._maxImpact);
      });
    }
  }

  @Input()
  public impact: Impact;

  @Input()
  public smallVersion: boolean;

  @Input()
  public analysisName: string;

  public dicp: Array<Slider> = [];

  private _readMode: Boolean;
  private _maxImpact: Impact;
  private _question: Question;

  constructor(private sliderService: SliderQcmService, private analysisGlobalService: AnalysisGlobalService) {
  }

  ngOnInit() {
    if (!this.readMode && this._question) {
      this.setDicp(this.impact);
    }
  }

  get readMode() {
    return this._readMode;
  }

  public getStartingValue(slider: Slider) {
    let impactValue = slider.startingValue;
    if (this.readMode && this._maxImpact) {
      impactValue = this._maxImpact['ira_' + slider.valueToChange];
    }
    return impactValue;
  }

  public getLabel(value: any) {
    const impactClasses = ['Faible', 'Moyen', 'Fort', 'Critique'];

    return impactClasses[value - 1]
  }

  public getCurrentImpact() {
    return this.impact;
  }

  public changeDicp(event) {
    let hasChanged = false;
    switch (event.valueToChange) {
      case 'd':
        hasChanged = this.impact.ira_d !== event.bulletNb;
        this.impact.ira_d = event.bulletNb;
        break;
      case 'i':
        hasChanged = this.impact.ira_i !== event.bulletNb;
        this.impact.ira_i = event.bulletNb;
        break;
      case 'c':
        hasChanged = this.impact.ira_c !== event.bulletNb;
        this.impact.ira_c = event.bulletNb;
        break;
      case 'p':
        hasChanged = this.impact.ira_p !== event.bulletNb;
        this.impact.ira_p = event.bulletNb;
        break;
    }
    if (hasChanged) {
      this.sendDICP();
    }
  }

  public sendDICP() {
    const requestBody = [{
      imp_id: this.impact.imp.imp_id,
      ral_id: this.guid,
      ira_d: this.impact.ira_d,
      ira_i: this.impact.ira_i,
      ira_c: this.impact.ira_c,
      ira_p: this.impact.ira_p,
      comment: this.impact.ira_comment
    }];
    this.analysisGlobalService.isSendingDoingQuery = true;

    this.sliderService.sendDICP(this.guid, requestBody).subscribe(res => {
      this.analysisGlobalService.isSendingDoingQuery = false;
    });
  }

  public modifyInput(value: string) {
    this.impact.ira_comment = value;
    this.sendDICP();
  }

  private setDicp(impact: Impact) {
    this.dicp[0].startingValue = impact.ira_d;
    this.dicp[1].startingValue = impact.ira_i;
    this.dicp[2].startingValue = impact.ira_c;
    this.dicp[3].startingValue = impact.ira_p;
  }
}
