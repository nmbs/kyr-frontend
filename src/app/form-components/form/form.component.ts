import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, HostListener, AfterViewInit } from '@angular/core';
import { Step } from '../../analysis/models/step.model';
import { Question } from '../../analysis/models/question.model';
import { Analysis } from '../../analysis/api-models/analysis.model';
import { WhiteContainerComponent } from '../white-container/white-container.component';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  @ViewChild('formEl') formEl: WhiteContainerComponent;

  @Output() modifyEvent = new EventEmitter<boolean>();
  @Output() prevEvent = new EventEmitter<void>();
  @Output() nextEvent = new EventEmitter<void>();
  @Output() activeEvent = new EventEmitter<boolean>();
  @Output() deleteEvent = new EventEmitter<Step>();
  @Output() endFocusTitleEvent = new EventEmitter<string>();

  @Input()
  public displayType: string;
  @Input()
  public hideLottieButtons = false;
  @Input()
  public exportingPDF = false;
  @Input()
  public form: Step;

  @Input()
  public analysis: Analysis;

  @Input()
  public isModify: boolean;

  @Input()
  public isFirst: boolean;

  @Input()
  public isLast: boolean;

  @Input()
  public placeholder: boolean;

  @HostListener('window:scroll', ['$event'])
  onScroll(event) {
    if (this.isModify && this.formEl) {
      const viewHeight = window.innerHeight;

      const height = parseFloat(window.getComputedStyle(this.formEl.container.nativeElement, null).height);
      // include marginTop
      const offTop = this.formEl.container.nativeElement.offsetTop;
      const marginBottom = parseFloat(window.getComputedStyle(this.formEl.container.nativeElement, null).marginBottom);
      const paddingTop = parseFloat(window.getComputedStyle(this.formEl.container.nativeElement, null).paddingTop);
      const paddingBottom = parseFloat(window.getComputedStyle(this.formEl.container.nativeElement, null).paddingBottom);

      const componentPosition = offTop + height + paddingTop + paddingBottom + marginBottom;
      const scrollPosition = window.pageYOffset;

      this.form.isActive = scrollPosition >= (offTop - viewHeight / 2) && scrollPosition < (componentPosition - viewHeight / 2);

      this.activeEvent.emit(this.form.isActive);
    }
  }

  constructor() { }

  ngOnInit() {
  }

  // emit modifyEvent to his parent
  public modify() {
    this.modifyEvent.emit(true);
  }

  // emit previousEvent to his parent
  public prev() {
    this.prevEvent.emit();
  }

  // emit nextEvent to his parent
  public next() {
    this.nextEvent.emit();
  }

  // emit deleteEvent to his parent
  public delete() {
    this.deleteEvent.emit(this.form);
  }

  // emit modifyEvent to his parent
  public seeSummary() {
    this.modifyEvent.emit(false);
  }

  // emit the end of the focus of the title with the value of the title to his parent
  public endFocusTitle(value) {
    this.form.title = value;
    this.endFocusTitleEvent.emit(value);
  }



  public getTitle(param) {
    return typeof this.form.title === 'string' ? this.form.title : this.form.title(param);
  }

}
