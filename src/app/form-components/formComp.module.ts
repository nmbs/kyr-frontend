
// Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { FormComponent } from '../form-components/form/form.component';
import { QuestionComponent } from '../form-components/question/question.component';
import { QcmComponent } from '../form-components/qcm/qcm.component';
import { GraphicModule } from '../graphic-components/graphic.module';
import { FormsModule } from '@angular/forms';
import { SlidersQcmComponent } from './sliders-qcm/sliders-qcm.component';
import { WhiteContainerComponent } from './white-container/white-container.component';
import { DateQcmComponent } from './date-qcm/date-qcm.component';
import { ToggleQcmComponent } from './toggle-qcm/toggle-qcm.component';


@NgModule({
  declarations: [
    FormComponent,
    QuestionComponent,
    QcmComponent,
    SlidersQcmComponent,
    WhiteContainerComponent,
    DateQcmComponent,
    ToggleQcmComponent
    ],
  imports: [CommonModule, GraphicModule, FormsModule],
  providers: [],
  exports: [
    FormComponent,
    QuestionComponent,
    QcmComponent,
    SlidersQcmComponent,
    WhiteContainerComponent,
    DateQcmComponent
  ]
})
export class FormCompModule { }
