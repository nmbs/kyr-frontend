import { Component, OnInit, Input, SecurityContext } from '@angular/core';
import { QuestionType } from '../../analysis/models/questionType.model';
import { Question } from '../../analysis/models/question.model';
import { AnalysisService } from '../../analysis/analysis.service';
import { QuestionService } from './question.service';
import { Analysis } from 'src/app/analysis/api-models/analysis.model';
import { Impact } from 'src/app/analysis/models/impact.model';
import { AnalysisFeatureService } from 'src/app/analysis/analysis-feature/analysis-feature.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss'],
  providers: [AnalysisService, QuestionService, AnalysisFeatureService]
})
export class QuestionComponent implements OnInit {
  @Input()
  set isModify(value: boolean) {
    this._isModify = value;
  }

  @Input()
  set question(value: Question) {
    this._question = value;
    if (this.analysis) {
      this.getAnswers(this._question);
    }
  }

  @Input()
  public guid: string;


  @Input()
  set analysis(value: Analysis) {
    this._analysis = value;
    if (this.question) {
      this.getAnswers(this.question);
    }
  }
  @Input()
  public placeholder: boolean;

  @Input()
  public apiPath: Function;

  @Input()
  public answers: any;

  // public answers: any;
  private _isModify: boolean;
  private _question: Question;
  private _analysis: Analysis;
  constructor(private analysisService: AnalysisService,
    private questionService: QuestionService,
    private analysisFeatureService: AnalysisFeatureService) {
  }

  ngOnInit() {
  }


  // get question answers
  public getAnswers(question: Question) {
    let res = null;
    if (question.value) {
      res = question.value;
    } else if (this.analysis) {
      if (this.analysis[question.propertyName] === null && this.isQcm(this.question.type)) {
        res = [];
      } else {
        res = this.analysis[question.propertyName];
      }
    }
    if (!this.answers) {
      this.answers = res;
    }
  }
  get question() {
    return this._question;
  }
  get analysis() {
    return this._analysis;
  }
  get isModify() {
    return this._isModify;
  }
  public isQcm(type) {
    return type === QuestionType.QCM_HOR ||
      type === QuestionType.QCM_HOR_THIN ||
      type === QuestionType.QCM_VER ||
      type === QuestionType.QCM_MULTIPLE_CHOICE_HOR ||
      type === QuestionType.QCM_MULTIPLE_CHOICE_VER ||
      type === QuestionType.QCM_MULTIPLE_CHOICE_HOR_THIN;
  }

  public isSlider(type) {
    return type === QuestionType.QCM_SLIDER;
  }

  public hasToggle(type) {
    return type === QuestionType.QCM_TOGGLE;
  }

  public isTextInput(question) {
    return question.type === QuestionType.TEXT_INPUT || question.type === QuestionType.TEXT_INPUT_SMALL;
  }

  public isArea(question) {
    return question.type === QuestionType.TEXT_INPUT;
  }

  // Send a PATCH to update a field of an analysis
  public send(value: string) {
    this.answers = value;
    let requestBody;
    if (this.question.isPatch) {
      requestBody = [{
        op: 'replace',
        path: this.question.propertyName,
        value: decodeURI(value)
      }];

    } else {
      const impact = this.getImpact();
      impact.ira_comment = value;
      this.analysis[this.question.propertyName] = value;
      requestBody = [impact];
    }
    this.patch(requestBody);
  }

  // Send a PATCH to update a toggle of an analysis
  public sendToggle(value: boolean) {
    const requestBody = [{
      op: 'replace',
      path: this.question.propertyName,
      value: value
    }];
    this.patch(requestBody);
  }

  public isArray(data) {
    return Array.isArray(data);
  }

  // Check if it has answers
  public hasAnswers() {
    const hasAnswers = this.answers && this.isArray(this.answers) && this.answers.length > 0;
    return hasAnswers;
  }

  public getImpact(): Impact {
    return this.analysis.impacts.find(impact => impact.imp.imp_label + '_impact' === this.question.propertyName);
  }

  public showTitle(): boolean {
    return !(this.question.isOptional && (!this.answers || this.isArray(this.answers) && this.answers.length === 0));
  }

  public getPlaceholder(): string {
    return this.question.placeholder ? this.question.placeholder : 'Saisissez votre texte…';
  }

  public getTitle() {
    return (typeof this.question.title !== 'function') ? this.question.title : this.question.title(this.analysis.ral_name);
  }

  public getRecapTitle() {
    let titleToDisplay = '';
    if (this.question.recapTitle) {
      titleToDisplay = (typeof this.question.recapTitle !== 'function') ? this.question.recapTitle : this.question.recapTitle(this.analysis.ral_name);
    } else {
      titleToDisplay = (typeof this.question.title !== 'function') ? this.question.title : this.question.title(this.analysis.ral_name);
    }
    return titleToDisplay;
  }

  public getToggleLabel() {
    return (typeof this.question.toggleLabel !== 'function') ? this.question.toggleLabel : this.question.toggleLabel(this.analysis.ral_name);
  }

  private patch(value) {
    if (this.apiPath === null || !this.apiPath) {
      this.analysisService.patchAnalysis(this.guid, value, this.question.apiPath).subscribe(res => {
      });
    } else {
      const apiPath = this.apiPath(this.guid);
      this.analysisFeatureService.patchFeature(value, apiPath).subscribe(res => {
      });
    }
  }
}
