
import { Injectable } from '@angular/core';
import { AppConfig } from '../../app.config';
import { HttpClient } from '@angular/common/http';
import { HttpHelper } from '../../helpers/http.helper';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Impact } from '../../analysis/models/impact.model';
import { Analysis } from 'src/app/analysis/api-models/analysis.model';
import { AnalysisGlobalService } from 'src/app/analysis/analysisGlobal.service';

  @Injectable()
  export class QuestionService {
    private _api: string;
    private _urlAnalysis: string;
    private _urlImpact: string;

    constructor(private _http: HttpClient, private appConfig: AppConfig, private httpHelper: HttpHelper, private analysisGlobalService: AnalysisGlobalService) {
      this._api = appConfig.getConfig().apiAnalysisUrl;
      this._urlAnalysis = this._api + '/analyses';
      this._urlImpact = this._api + '/impacts';
    }

    // Update choices
  public sendChoices(path, choices): Observable<any> {
    this.analysisGlobalService.isSendingDoingQuery = true;
    return this._http.patch(this._urlAnalysis + path, choices,
      { headers: HttpHelper.getHeaders() })
      .pipe(
        map((res: Analysis) => {
          this.analysisGlobalService.analysis = res;
          this.analysisGlobalService.isSendingDoingQuery = false;
          return res;
        }),
        catchError(HttpHelper.handleError)
      );
  }

}
