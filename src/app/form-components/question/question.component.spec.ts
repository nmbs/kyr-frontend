import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionComponent } from './question.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { AnalysisService } from '../../analysis/analysis.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfig } from '../../app.config';
import { analysisDescription } from '../../../assets/analysis-description-form/analysis';
import { AnalysisGlobalService } from '../../analysis/analysisGlobal.service';
import { analysisImpact } from '../../../assets/analysis-impact-form/impacts';
import { RouterTestingModule } from '@angular/router/testing';

const description = analysisDescription;
const impact = analysisImpact;
const mockAnswer = {
  cho_id: '3f9bb204-4f09-11e9-9341-064a82272d84',
  ral_id: '1234',
  other: ''
};

describe('QuestionComponent', () => {
  let component: QuestionComponent;
  let fixture: ComponentFixture<QuestionComponent>;

  beforeEach(async(() => {
    const appConfigSpy = jasmine.createSpyObj('AppConfig', {
      getConfig: function () {
        return '';
      }
    });
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [QuestionComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [AnalysisService, AnalysisGlobalService, { provide: AppConfig, useValue: appConfigSpy }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('sanitize', () => {
    beforeEach(() => { });
  });

  describe('isQcm', () => {
    beforeEach(() => { });

    it('should not be QCM', () => {
      expect(component.isQcm(description.steps[0].questions[0].type)).toEqual(false);
    });
    it('should be QCM (QCM_VER)', () => {
      expect(component.isQcm(description.steps[1].questions[1].type)).toEqual(true);
    });
    it('should be QCM (QCM_MULTIPLE_CHOICE_HOR)', () => {
      expect(component.isQcm(description.steps[1].questions[0].type)).toEqual(true);
    });
    it('should be QCM (QCM_TOGGLE)', () => {
      expect(component.isQcm(description.steps[1].questions[7].type)).toEqual(false);
    });
  });

  describe('hasToggle', () => {
    beforeEach(() => { });

    it('should not have toggle', () => {
      expect(component.hasToggle(description.steps[3].questions[0].type)).toEqual(false);
    });
    it('should have toggle', () => {
      expect(component.hasToggle(description.steps[1].questions[7].type)).toEqual(true);
    });
  });

  describe('isSlider', () => {
    beforeEach(() => { });
    it('should not be SLIDER', () => {
      expect(component.isSlider(description.steps[0].questions[0].type)).toEqual(false);
    });
    it('should not be SLIDER (QCM_VER)', () => {
      expect(component.isSlider(description.steps[1].questions[1].type)).toEqual(false);
    });
    it('should not be SLIDER (QCM_MULTIPLE_CHOICE_HOR)', () => {
      expect(component.isSlider(description.steps[1].questions[0].type)).toEqual(false);
    });
    it('should not be SLIDER (QCM_TOGGLE)', () => {
      expect(component.isSlider(description.steps[1].questions[7].type)).toEqual(false);
    });
  });

  describe('isTextInput', () => {
    beforeEach(() => { });

    it('should not be a text input', () => {
      expect(component.isTextInput(description.steps[3].questions[0])).toEqual(false);
    });
    it('should  be a text input', () => {
      expect(component.isTextInput(description.steps[0].questions[0])).toEqual(true);
    });
  });

  describe('isArray', () => {
    beforeEach(() => { });

    it('should not be an array', () => {
      expect(component.isArray(description)).toEqual(false);
    });
    it('should be an array', () => {
      expect(component.isArray(description.steps)).toEqual(true);
    });
  });

  describe('hasAnswers', () => {
    beforeEach(() => { });

    it('should not have Answers', () => {
      component.answers = [];
      expect(component.hasAnswers()).toEqual(false);
    });
    it('should have Answers', () => {
      component.answers = [mockAnswer];
      expect(component.hasAnswers()).toEqual(true);
    });
  });

  describe('showTitle', () => {
    beforeEach(() => {
      component.answers = [mockAnswer];
      component.question = description.steps[0].questions[0];
      component.guid = '1234';
    });

    it('should not displayTitle', () => {
      component.question.isOptional = true;
      component.answers = [];
      expect(component.showTitle()).toEqual(false);
    });
  });
});
