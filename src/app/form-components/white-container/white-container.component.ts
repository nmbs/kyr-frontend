import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-white-container',
  templateUrl: './white-container.component.html',
  styleUrls: ['./white-container.component.scss']
})
export class WhiteContainerComponent implements OnInit {
  @ViewChild('container') container: ElementRef;

  @Input()
  public isLast: boolean;

  @Input()
  public isDisplaySquare: boolean;

  @Input()
  public isActive: boolean;

  @Input()
  public displayBlock: boolean;

  @Input()
  public isModify: boolean;

  constructor(element: ElementRef) { }

  ngOnInit() {
  }

}
