import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateQcmComponent } from './date-qcm.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

const choice1 = {
  cho_id: 'choice1',
  cho_name: 'En place',
  cho_subtitle: null,
  cho_icon: null,
  cho_help: null,
  checked: false,
  status: {
    mst_id: 'En place',
    mst_name: 'En place',
    mst_code: 'En place',
  }
};

const choice2 = {
  cho_id: 'choice2',
  cho_name: 'A venir',
  cho_subtitle: null,
  cho_icon: null,
  cho_help: null,
  checked: false,
  isDatePicker: true,
  date: new Date(),
  status: {
    mst_id: 'À venir',
    mst_name: 'À venir',
    mst_code: 'À venir'
  }
};

const choice3 = {
  cho_id: 'choice3',
  cho_name: 'N/A',
  cho_subtitle: null,
  cho_icon: null,
  cho_help: null,
  checked: false,
  status: {
    mst_id: 'N/A',
    mst_name: 'N/A',
    mst_code: 'N/A'
  }
};

describe('DateQcmComponent', () => {
  let component: DateQcmComponent;
  let fixture: ComponentFixture<DateQcmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateQcmComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateQcmComponent);
    component = fixture.componentInstance;
    component.choices = [choice1, choice2, choice3];
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Test dateSelected', () => {
    it('Check if date and isChecked is updated', () => {
      const newDate = new Date();
      const choice = {
        cho_id: 'choice2',
        cho_name: 'Test',
        cho_subtitle: null,
        cho_icon: null,
        cho_help: null,
        checked: true,
        isDatePicker: true,
        date: newDate
      };
      component.dateSelected(choice);
      expect(component.choices[1].checked).toEqual(true);
      expect(component.choices[1].date).toEqual(newDate);
    });
  });

  describe('Test select', () => {
    it('Check if isChecked is updated and others are unchecked', () => {
      const choice = {
        cho_id: 'choice2',
        cho_name: 'Test',
        cho_subtitle: null,
        cho_icon: null,
        cho_help: null,
        checked: true,
        isDatePicker: true,
        date: new Date()
      };
      component.dateSelected(choice);

      choice.cho_id = 'choice1';
      component.select(choice);
      expect(component.choices[1].checked).toEqual(false);
      expect(component.choices[0].checked).toEqual(true);
    });
  });
});
