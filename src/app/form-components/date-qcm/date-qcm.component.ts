import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Choice } from 'src/app/analysis/models/choice.model';

const monthNames = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin',
  'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'
];

@Component({
  selector: 'app-date-qcm',
  templateUrl: './date-qcm.component.html',
  styleUrls: ['./date-qcm.component.scss']
})
export class DateQcmComponent implements OnInit {
  @Output() selectEvent = new EventEmitter<Choice>();

  @Input()
  public choices: Array<Choice>;

  constructor() { }

  ngOnInit() {}

  private getDateString(date: Date): string {
      return monthNames[date.getMonth()] + ' ' + date.getFullYear();
  }

  public select(choice) {
    this.choices.find(c => c.cho_id === choice.cho_id).checked = true;
    this.choices.forEach(c => {
      if (c.cho_id !== choice.cho_id) {
        c.checked = false;
      }
    });
    this.selectEvent.emit(choice);
  }

  public dateSelected(event) {
    const selectedChoice = this.choices.find(c => c.cho_id === event.cho_id);
    selectedChoice.checked = true;
    selectedChoice.date = event.date;
    selectedChoice.cho_name = 'À venir en ' + this.getDateString(event.date);
    this.choices.forEach(c => {
      if (c.cho_id !== event.cho_id) {
        c.checked = false;
      }
    });
    this.selectEvent.emit(selectedChoice);
  }
}
