import { Component, OnInit, Input } from '@angular/core';
import { QuestionType } from '../../analysis/models/questionType.model';
import { Choice } from '../../analysis/models/choice.model';
import { QcmService } from './qcm.service';

@Component({
  selector: 'app-qcm',
  templateUrl: './qcm.component.html',
  styleUrls: ['./qcm.component.scss'],
  providers: [QcmService]
})
export class QcmComponent implements OnInit {
  public _choices: Array<Choice> = [];
  public choices: Array<Choice> = [];

  private _answers: Array<any> = [];
  private _apiPath: string;
  public placeholder = true;

  @Input()
  set apiPath(apiPath: string) {
    if (apiPath) {
      this._apiPath = apiPath;
      // Get choices for the question
      this.qcmService.getChoices(apiPath).subscribe(res => {
        this._choices = res;
        this.checkChoices();
      });
    }
  }

  @Input()
  public type: QuestionType;

  @Input()
  public guid: string;

  @Input()
  public propertyName: string;

  @Input()
  set answers(answers: Array<any>) {
    this._answers = answers;
    this.checkChoices();
  }
  @Input()
  public nbPlaceholders: number;

  @Input()
  public readMode: boolean;

  constructor(private qcmService: QcmService) { }

  ngOnInit() {
    this.placeholder = true;
  }

  public counter(i: number) {
    return new Array(i);
  }

  public isSimpleQcm(type) {
    return type === QuestionType.QCM_HOR || type === QuestionType.QCM_VER || type === QuestionType.QCM_HOR_THIN;
  }

  public isVertical() {
    return this.type === QuestionType.QCM_VER || this.type === QuestionType.QCM_MULTIPLE_CHOICE_VER
      || this.type === QuestionType.QCM_TOGGLE;
  }

  public isThin() {
    return this.type === QuestionType.QCM_VER || this.type === QuestionType.QCM_MULTIPLE_CHOICE_VER
    || this.type === QuestionType.QCM_TOGGLE || this.type === QuestionType.QCM_HOR_THIN
    || this.type === QuestionType.QCM_MULTIPLE_CHOICE_HOR_THIN;
  }

  public hasCheck() {
    return this.type !== QuestionType.QCM_HOR_THIN && this.type !== QuestionType.QCM_MULTIPLE_CHOICE_HOR_THIN;
  }

  public getChoices() {
    return this._choices;
  }
  public setChoices(value) {
    this._choices = value;
  }
  public select(event, choice) {

    if (this.isSimpleQcm(this.type)) {
      // empty answers and choice
      this._answers.length = 0;
      this._choices.forEach(cho => {
        if (cho.cho_id !== choice.cho_id) {
          cho.checked = false;
        }
      });
    }

    this._answers = this._answers ? this._answers : [];

    // Custom Object to Send
    const choiceToSend = {
      cho_id: choice.cho_id,
      ral_id: this.guid,
      other: ''
    };

    // if it is not checked we add it
    if (!choice.checked) {
      this._answers.push(choiceToSend);
    } else {
      // Delete from the answers list
      const index = this._answers.findIndex(answer => answer.cho_id === choiceToSend.cho_id &&
        answer.ral_id === choiceToSend.ral_id && answer.other === choiceToSend.other);
      if (index > -1) {
        this._answers.splice(index, 1);
      }

    }

    this.qcmService.sendChoices('/' + this.guid + this._apiPath, this._answers).subscribe(res => {
      choice.checked = !choice.checked;
    });
  }

  public getAnswers() {
    return this._answers;
  }

  // get choice name for the answer
  public GetChoiceName(answer) {
    const cho = this._choices.find(choice => choice.cho_id === answer.cho_id);
    return cho ? cho.cho_name : null;
  }

  // Check if the choices are checked
  public checkChoices() {
    if (this._choices && this._answers) {
      this._choices.forEach(choice => {
        this.checkChoice(choice);
      });
      this.choices = this._choices;
      this.placeholder = false;
    }
  }

  // Check if a choice is checked
  private checkChoice(choice) {
    if (this._answers) {
      choice.checked = this._answers.some(answer => answer.cho_id === choice.cho_id);
    }
    return choice;
  }

}
