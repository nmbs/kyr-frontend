import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QcmComponent } from './qcm.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { QcmService } from './qcm.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfig } from '../../app.config';
import { analysisDescription } from '../../../assets/analysis-description-form/analysis';
import { Choice } from 'src/app/analysis/models/choice.model';
import { QuestionType } from 'src/app/analysis/models/questionType.model';
import { AnalysisGlobalService } from '../../analysis/analysisGlobal.service';
import { RouterTestingModule } from '@angular/router/testing';

const description = analysisDescription;

const mockChoices: Array<Choice> = [{
  cho_id: '3f9bb204-4f09-11e9-9341-064a82272d84',
  cho_name: 'Entité',
  cho_subtitle: '',
  cho_icon: '',
  cho_help: '',
  checked: null
}, {
  cho_id: '3f9bb241-4f09-11e9-9341-064a82272d84',
  cho_name: 'Groupe',
  cho_subtitle: '',
  cho_icon: '',
  cho_help: '',
  checked: null
}];

describe('QcmComponent', () => {
  let component: QcmComponent;
  let fixture: ComponentFixture<QcmComponent>;

  beforeEach(async(() => {
    const appConfigSpy = jasmine.createSpyObj('AppConfig', {
      getConfig: function () {
        return '';
      }
    });
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [QcmComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [QcmService, AnalysisGlobalService, { provide: AppConfig, useValue: appConfigSpy }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QcmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('isSimpleQcm', () => {
    beforeEach(() => { });

    it('should not be simple QCM', () => {
      expect(component.isSimpleQcm(description.steps[0].questions[0].type)).toEqual(false);
    });
    it('should be simple QCM', () => {
      expect(component.isSimpleQcm(description.steps[3].questions[0].type)).toEqual(true);
    });
  });

  describe('isVertical', () => {
    beforeEach(() => { });

    it('should not be vertival QCM', () => {
      component.type = description.steps[1].questions[0].type;
      expect(component.isVertical()).toEqual(false);
    });
    it('should be vertival QCM', () => {
      component.type = description.steps[1].questions[1].type;
      expect(component.isVertical()).toEqual(true);
    });
  });

  describe('checkChoices', () => {
    beforeEach(() => {
      component.apiPath = '/ItRisk';
      component.setChoices(mockChoices);
    });

    it('should not checkChoices', () => {
      component.answers = [];
      expect(component.getChoices()[0].checked).toEqual(false);
      expect(component.getChoices()[1].checked).toEqual(false);
    });

    it('should checkChoices', () => {
      component.answers = [mockChoices[0]];
      expect(component.getChoices()[0].checked).toEqual(true);
      expect(component.getChoices()[1].checked).toEqual(false);
    });
  });

  describe('getAnswers', () => {
    beforeEach(() => { });

    it('should get answers', () => {
      component.answers = ['test'];
      expect(component.getAnswers()).toEqual(['test']);
    });
  });

  describe('getAnswer', () => {
    beforeEach(() => {
      component.setChoices(mockChoices);
    });

    it('should get no answer', () => {
      expect(component.GetChoiceName({ cho_id: '123456749', ral_id: '12346', other: '' })).toEqual(null);
    });

    it('should get answer', () => {
      expect(component.GetChoiceName({ cho_id: '3f9bb204-4f09-11e9-9341-064a82272d84', ral_id: '12346', other: '' })).toEqual(mockChoices[0].cho_name);
    });
  });

  describe('select', () => {
    beforeEach(() => {
      component.setChoices(mockChoices);
      component.guid = '123';
    });

    it('should delete answer', () => {
      component.answers = [{
        cho_id: '3f9bb204-4f09-11e9-9341-064a82272d84',
        ral_id: '123',
        other: ''
      }];
      component.select(null, {
        cho_id: '3f9bb204-4f09-11e9-9341-064a82272d84',
        cho_name: 'Entité',
        cho_subtitle: '',
        cho_icon: '',
        cho_help: '',
        checked: true
      });

      expect(component.getAnswers()).toEqual([]);
    });

    it('should add answer', () => {
      component.answers = [];
      component.select(null, {
        cho_id: '3f9bb204-4f09-11e9-9341-064a82272d84',
        cho_name: 'Entité',
        cho_subtitle: '',
        cho_icon: '',
        cho_help: '',
        checked: false
      });
      expect(component.getAnswers()).toEqual([{
        cho_id: '3f9bb204-4f09-11e9-9341-064a82272d84',
        ral_id: '123',
        other: ''
      }]);
    });

    it('should add answer simple QCM', () => {
      component.answers = [];
      component.type = QuestionType.QCM_HOR;
      component.select(null, {
        cho_id: '3f9bb204-4f09-11e9-9341-064a82272d84',
        cho_name: 'Entité',
        cho_subtitle: '',
        cho_icon: '',
        cho_help: '',
        checked: false
      });
      expect(component.getAnswers()).toEqual([{
        cho_id: '3f9bb204-4f09-11e9-9341-064a82272d84',
        ral_id: '123',
        other: ''
      }]);
    });
  });
});
