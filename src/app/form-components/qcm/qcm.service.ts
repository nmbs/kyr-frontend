import { Injectable } from '@angular/core';
import { AppConfig } from '../../app.config';
import { HttpClient } from '@angular/common/http';
import { Choice } from '../../analysis/models/choice.model';
import { HttpHelper } from '../../helpers/http.helper';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AnalysisGlobalService } from '../../analysis/analysisGlobal.service';
import { Analysis } from 'src/app/analysis/api-models/analysis.model';

@Injectable()
export class QcmService {
  private _api: string;
  private _urlAnalysis: string;
  private _urlImpact: string;

  constructor(private _http: HttpClient, private appConfig: AppConfig,
    private httpHelper: HttpHelper, private analysisGlobalService: AnalysisGlobalService) {
    this._api = appConfig.getConfig().apiAnalysisUrl;
    this._urlAnalysis = this._api + '/analyses';
    this._urlImpact = this._api + '/impacts';
  }

  // Get list of choice for a question
  public getChoices(path): Observable<Array<Choice>> {
    return this._http.get<Array<Choice>>(this._api + path, { headers: HttpHelper.getHeaders() })
      .pipe(
        map((data) => {
          const choices: Array<Choice> = [];
          if (data && data.length > 0) {
            data.forEach((datum: any) => {
              // transform it into choice object
              const choice: Choice = {
                cho_id: datum.cho_id,
                cho_name: datum.cho_name,
                cho_subtitle: datum.cho_subtitle,
                cho_icon: datum.cho_icon,
                cho_help: datum.cho_help,
                checked: false
              };
              choices.push(choice);
            });
          }
          return choices;
        }),
        catchError(HttpHelper.handleError)
      );
  }

  // Update choices
  public sendChoices(path, choices): Observable<any> {
          // Update query status in the sidebar
          this.analysisGlobalService.isSendingDoingQuery = true;
    return this._http.patch(this._urlAnalysis + path, choices,
      { headers: HttpHelper.getHeaders() })
      .pipe(
        map((res: Analysis) => {
          this.analysisGlobalService.analysis = res;
          // Update query status in the sidebar
          this.analysisGlobalService.isSendingDoingQuery = false;
          return res;
        }),
        catchError(HttpHelper.handleError)
      );
  }
}
