import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToggleQcmComponent } from './toggle-qcm.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('ToggleQcmComponent', () => {
  let component: ToggleQcmComponent;
  let fixture: ComponentFixture<ToggleQcmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToggleQcmComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToggleQcmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
