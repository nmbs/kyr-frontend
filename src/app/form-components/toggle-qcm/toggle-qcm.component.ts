import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Question } from 'src/app/analysis/models/question.model';
import { Analysis } from 'src/app/analysis/api-models/analysis.model';
@Component({
  selector: 'app-toggle-qcm',
  templateUrl: './toggle-qcm.component.html',
  styleUrls: ['./toggle-qcm.component.scss']
})
export class ToggleQcmComponent implements OnInit {
  private _isModify: boolean;

  @Output() checkEvent = new EventEmitter<boolean>();

  @Input()
  set isModify(value: boolean) {
    this._isModify = value;
  }

  @Input()
  public question: Question;

  @Input()
  public toggleLabel: string;

  @Input()
  public guid: string;

  @Input()
  public answers: any;

  @Input()
  public analysis: Analysis;

  @Input()
  public apiPath: Function;
  
  constructor() { }

  ngOnInit() { }

  get isModify() {
    return this._isModify;
  }

  // get question answers
  public getAnswers(question: Question) {
    return question.value ? question.value : this.analysis ? this.analysis[question.propertyName] : null;
  }

  public checkToggle() {
    this.analysis[this.question.propertyName] = this.analysis[this.question.propertyName].toString() !== 'true';
    this.checkEvent.emit(this.analysis[this.question.propertyName]);
  }
  public getRecapTitle() {
    let titleToDisplay = '';
    if (this.question.recapTitle) {
      titleToDisplay = (typeof this.question.recapTitle !== 'function') ? this.question.recapTitle : this.question.recapTitle(this.analysis.ral_name);
    } else {
      titleToDisplay = (typeof this.question.title !== 'function') ? this.question.title : this.question.title(this.analysis.ral_name);
    }
    return titleToDisplay;
  }

  public getRecapAnswers() {
    return this.getAnswers(this.question).toString() === 'true' ? 'Oui' : 'Non';
  }
}
