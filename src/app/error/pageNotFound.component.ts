
import { Component, OnInit, Input } from '@angular/core';
import { AppConfig } from '../app.config';
import { Router } from '@angular/router';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './pageNotFound.component.html',
  styleUrls: ['./pageNotFound.component.scss']
})
export class PageNotFoundComponent implements OnInit {
  public lottieConfig: Object;
  private anim: any;

  constructor(private readonly router: Router, private appConfig: AppConfig) { }

  ngOnInit() {
    this.lottieConfig = {
      animationData: this.appConfig.getConfig().animationAssets['page-not-found'],
      renderer: 'svg',
      autoplay: true,
      loop: false
    };
  }

  public handleAnimation(anim: any) {
    this.anim = anim;
    this.anim.setSpeed(1);
  }

  public play() {
    if (this.anim) {
      this.anim.stop();
      this.anim.play();
    }
  }

  refresh() {
    location.reload();
  }

  public backToHome() {
    this.router.navigate(['/']);
  }
}
