/* **************************************************************************
/*
/*   Project : KYR
/*   File : error.component.ts
/*
/*   By: CACD2 <developer@cacd2.fr>
/*   Created: 12/12/2018, 10:54:45 AM
/*
/* *************************************************************************/


import { Component, OnInit } from '@angular/core';
import {ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {
  public errorCode: string;
  public errorMessage: string;

  constructor(private readonly route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.errorCode = params.get('errorcode');
      this.errorMessage = '';
    });
  }
}
