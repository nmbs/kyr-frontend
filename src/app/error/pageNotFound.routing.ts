
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ErrorComponent } from './error.component';

const homeRoutes: Routes = [
  {
    path: '',
    component: ErrorComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(homeRoutes)],
  exports: [RouterModule]
})
export class PageNotFoundRoutingModule { }
