import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-default-error',
  templateUrl: './default-error.component.html',
  styleUrls: ['./default-error.component.scss']
})
export class DefaultErrorComponent implements OnInit {

  constructor(private readonly router: Router) { }

  ngOnInit() {
  }

  public backToHome() {
    this.router.navigate(['/']);
  }
}
