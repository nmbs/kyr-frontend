
import { NgModule } from '@angular/core';
import { PageNotFoundComponent } from './pageNotFound.component';
import { ErrorComponent } from './error.component';
import { PageNotFoundRoutingModule } from './pageNotFound.routing';
import { GraphicModule } from '../graphic-components/graphic.module';
import { CommonModule } from '@angular/common';
import { DefaultErrorComponent } from './default-error/default-error.component';
import { LottieAnimationViewModule } from 'ng-lottie';


@NgModule({
  imports: [PageNotFoundRoutingModule,
            GraphicModule,
            CommonModule,
            LottieAnimationViewModule.forRoot()],
  declarations: [PageNotFoundComponent,
                  ErrorComponent,
                  DefaultErrorComponent],
  exports: [PageNotFoundComponent]
})
export class PageNotFoundModule { }
