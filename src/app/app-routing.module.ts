import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppAuthGuard } from './app.authguard';

const appRoutes: Routes = [
  // Home route
  {
    path: '',
    loadChildren: './home/home.module#HomeModule',
    canActivate: [AppAuthGuard]
  },
  {
    path: 'analyses',
    loadChildren: './analysis/analysis.module#AnalysisModule',
    canActivate: [AppAuthGuard]
  },
  // Page not found route
  {
    path: 'error/:errorcode',
    loadChildren: './error/pageNotFound.module#PageNotFoundModule',
  },
  {
    path: '**',
    loadChildren: './error/pageNotFound.module#PageNotFoundModule',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
  providers: [AppAuthGuard]
})
export class AppRoutingModule { }
