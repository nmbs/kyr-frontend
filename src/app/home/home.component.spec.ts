import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HomeComponent } from './home.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { AppConfig } from '../app.config';
import { AnalysisLight } from './models/analysisLight';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { UserGlobalService } from 'src/app/services/userGlobal.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';


let component: HomeComponent;

describe('HomeComponent', () => {
  beforeEach(async(() => {
    const appConfigSpy = {
      getConfig: () => {
        return { animationAssets: {} };
      }
    };
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        ScrollingModule,
        HttpClientTestingModule
      ],
      declarations: [
        HomeComponent
      ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: AppConfig, useValue: appConfigSpy },
        UserGlobalService
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(HomeComponent);
    const app = fixture.debugElement.componentInstance;
    component = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
