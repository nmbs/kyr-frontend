import { Injectable } from '@angular/core';
import { AppConfig } from '../app.config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AnalysisLight } from './models/analysisLight';
import { HttpHelper } from '../helpers/http.helper';
import { catchError, map } from 'rxjs/operators';
import { User } from '../models/user';
import { Role } from '../models/role';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  private _api: string;
  private _urlAnalysis: string;

  constructor(private _http: HttpClient, private appConfig: AppConfig) {
    this._api = appConfig.getConfig().apiAnalysisUrl;
    this._urlAnalysis = this._api + '/analyses';
  }

  // Get all analyses
  public getAnalyses(): Observable<Array<AnalysisLight>> {
    return this._http.get<Array<AnalysisLight>>(this._urlAnalysis, { headers: HttpHelper.getHeaders() })
      .pipe(
        map(res => {
          const AnalysisLightlist: Array<AnalysisLight> = [];

          res.forEach(ral => {

            const t: AnalysisLight = {
              id: ral.id,
              name: ral.name,
              entity: ral.entity,
              isBlocked: ral['analysis_isBlocked'],
              isAccepted: ral['decision_isAccepted'],
              decision_date: ral['decision_date'],
              users: []
            };

            ral.users.forEach(usr => {
              const userRole: Role = {
                rol_id: usr['role']['rol_id'],
                rol_label: usr['role']['rol_label'],
                rol_code: usr['role']['rol_code'],
                rol_description: usr['role']['rol_description']
              };

              const user: User = {
                guid: usr['user']['usr_id'],
                lastName: usr['user']['usr_lastname'],
                firstName: usr['user']['usr_firstname'],
                email: usr['user']['usr_email'],
                role: userRole
              };

              t.users.push(user);
            });
            AnalysisLightlist.push(t);
          });

          return AnalysisLightlist;
        }),
        catchError(HttpHelper.handleError)
      );
  }
}
