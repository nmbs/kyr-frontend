
// Modules
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home.routing';

import { NgModule } from '@angular/core';

import { HomeComponent } from './home.component';
import { GraphicModule } from '../graphic-components/graphic.module';
import { FormCompModule } from '../form-components/formComp.module';
import { LottieAnimationViewModule } from 'ng-lottie';


@NgModule({
  declarations: [
    HomeComponent
    ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    GraphicModule,
    FormCompModule,
    LottieAnimationViewModule.forRoot()
  ],
  providers: [
  ]
})
export class HomeModule { }
