import { User } from '../../models/user';

export interface AnalysisLight {
    id: string;
    name: string;
    entity: string;
    isBlocked: boolean;
    isAccepted: boolean;
    decision_date: string;
    users: Array<User>;
}
