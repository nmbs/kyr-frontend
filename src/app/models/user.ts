import { Role } from './role';

export class User {
    guid: string;
    email: string;
    firstName: string;
    lastName: string;
    role: Role;
}
