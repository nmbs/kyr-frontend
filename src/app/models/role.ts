export class Role {
    rol_id: string;
    rol_label: string;
    rol_code: string;
    rol_description: string;
}
