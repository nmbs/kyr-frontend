export class RolesPolicies {
    policies: Array<RolePolicy>;
}

export class RolePolicy {
    role_code: string;
    read_only: boolean;
    add_users: boolean;
}