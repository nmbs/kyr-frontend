export class Tag {
    id: string;
    label: string;
    tooltip: string;
}
