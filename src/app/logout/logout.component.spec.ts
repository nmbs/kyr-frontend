import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogoutComponent } from './logout.component';

import { KeycloakService } from 'keycloak-angular';
import { MatDialogRef } from '@angular/material';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('LogoutComponent', () => {
  let component: LogoutComponent;
  let fixture: ComponentFixture<LogoutComponent>;

  beforeEach(async(() => {
    const matDialogRefSpy = {};
    TestBed.configureTestingModule({
      declarations: [LogoutComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [KeycloakService,
        { provide: MatDialogRef, useValue: matDialogRefSpy }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
