import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {
  public isDisplayed = true;

  constructor(private keycloakService: KeycloakService,
    public dialogRef: MatDialogRef<LogoutComponent>) { }

  ngOnInit() {
  }

  public close() {
    this.isDisplayed = false;
    setTimeout(() => {
      this.dialogRef.close();
    }, 500);
  }

  public logout() {
    this.keycloakService.clearToken();
    this.keycloakService.logout();
  }
}
