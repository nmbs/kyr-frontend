import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { UserGlobalService } from 'src/app/services/userGlobal.service';
import { _MatTabHeaderMixinBase } from '@angular/material/tabs/typings/tab-header';
import { AppConfig } from 'src/app/app.config';
import { ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material';
import { LogoutComponent } from '../logout/logout.component';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProfileComponent implements OnInit {
  private _userEmail: string;
  private _userName: string;
  private anim: any;

  public lottieConfig: Object;

  get userEmail(): string {
    return this._userEmail;
  }

  get userName(): string {
    return this._userName;
  }

  constructor(private userGlobalService: UserGlobalService,
    private appConfig: AppConfig,
    public dialog: MatDialog) {
    this._userEmail = this.userGlobalService.user.email;
    this._userName = this.userGlobalService.formatName(this.userGlobalService.user.firstName, this.userGlobalService.user.lastName);
  }

  ngOnInit() {
    this.lottieConfig  = {
      animationData:  this.appConfig.getConfig().animationAssets['logout'],
      renderer: 'svg',
      autoplay: false,
      loop: false
    };
  }

  public handleAnimation(anim: any) {
    this.anim = anim;
    this.anim.setSpeed(1);
  }

  public play() {
    if (this.anim) {
      this.anim.stop();
      this.anim.play();
    }
  }

   public click() {
    this.dialog.open(LogoutComponent, {});
  }
}
