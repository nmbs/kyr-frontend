import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProfileComponent } from './profile.component';
import { CommonModule } from '@angular/common';
import { GraphicModule } from '../graphic-components/graphic.module';
import { LottieAnimationViewModule } from 'ng-lottie';
import { UserGlobalService } from '../services/userGlobal.service';
import { AppConfig } from '../app.config';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { User } from '../models/user';
import { MatDialog } from '@angular/material';
import { HttpClientTestingModule } from '@angular/common/http/testing';

const mockUser: User = { guid: 'guid', email: 'email', firstName: 'firstName', lastName: 'lastName', role: null };

describe('ProfilComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;

  class MockUserService {
    private _user: User;

    constructor() {
      this._user = mockUser;
    }

    public get user() {
      return this._user;
    }

    public formatName(): string {
      let name = '';
      if (this._user.firstName) {
        name += this._user.firstName + ' ';
      }

      if (this._user.lastName) {
        name += this._user.lastName;
      }

      return name.trim();
    }
  }


  beforeEach(async(() => {
    const appConfigSpy = {
      getConfig: () => {
        return { animationAssets: {} };
      }
    };

    const matDialogSpy = {};

    TestBed.configureTestingModule({
      declarations: [ProfileComponent],
      imports: [
        CommonModule,
        GraphicModule,
        HttpClientTestingModule,
        LottieAnimationViewModule.forRoot()
      ],
      providers: [
        { provide: UserGlobalService, useValue: new MockUserService() },
        { provide: AppConfig, useValue: appConfigSpy },
        { provide: MatDialog, useValue: matDialogSpy },
        HttpClient,
        HttpHandler
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
