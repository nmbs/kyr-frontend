import { ProfileComponent } from './profile.component';
import { NgModule } from '@angular/core';
import { UserGlobalService } from '../services/userGlobal.service';
import { GraphicModule } from '../graphic-components/graphic.module';
import { LottieAnimationViewModule } from 'ng-lottie';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material';
import { LogoutComponent } from '../logout/logout.component';

@NgModule({
    declarations: [
        ProfileComponent,
        LogoutComponent
      ],
    imports: [
        CommonModule,
        GraphicModule,
        MatDialogModule,
        LottieAnimationViewModule.forRoot()
    ],
    providers: [
        UserGlobalService
    ],
    entryComponents: [
        LogoutComponent
    ],
    exports: [
        ProfileComponent
    ]
  })
  export class ProfilModule { }
