import { Injectable } from '@angular/core';
import { UserGlobalService } from './userGlobal.service';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from '../app.config';
import { Observable, of } from 'rxjs';
import { User } from '../models/user';
import { HttpHelper } from '../helpers/http.helper';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class UserService {

  private _urlApiUser: string;
  private _urlApiAnalysis: string;
  private _urlCreateUser: string;
  private _urlGetUsers: string;

  constructor(private _http: HttpClient,
    appConfig: AppConfig,
    private userGlobalService: UserGlobalService) {
    this._urlApiAnalysis = appConfig.getConfig().apiAnalysisUrl + '/analyses';
    this._urlApiUser = appConfig.getConfig().apiUserUrl + '/users';
    this._urlCreateUser = this._urlApiUser + '/email';
    this._urlGetUsers = this._urlApiUser + '/analysis';
  }

  // Get user by email
  public getUser(user_email: string): Observable<User> {
    return this._http.get<User>(this._urlCreateUser + '/' + encodeURI(user_email), { headers: HttpHelper.getHeaders() })
      .pipe(
        map(res => {
          const user = {
            guid: res['usr_id'],
            email: res['usr_email'],
            firstName: res['usr_firstname'],
            lastName: res['usr_lastname'],
            role: null
          };
          this.userGlobalService.user = user;
          return user;
        }),
        catchError(HttpHelper.handleError)
      );
  }

  // Get all Users
  public getUsers(): Observable<any> {
    return this._http.get<Array<any>>(this._urlApiUser, { headers: HttpHelper.getHeaders() })
      .pipe(
        map(users => {
          const userList: Array<User> = [];
          users.forEach(usr => {
            const user: User = {
              guid: usr['usr_id'],
              lastName: usr['usr_lastname'],
              firstName: usr['usr_firstname'],
              email: usr['usr_email'],
              role: null
            };
            userList.push(user);
          });
          return userList;
        }),
        catchError(HttpHelper.handleError)
      );
  }

  public addUsersToAnalyse(analyseGuid: string, usersBody) {
    return this._http.patch<any>(this._urlApiAnalysis + '/' + analyseGuid + '/users', usersBody, { headers: HttpHelper.getHeaders() })
      .pipe(catchError(HttpHelper.handleError));
  }

  public getUsersByAnalyseId(analyse_id): Observable<Array<User>> {
    return this._http.get<Array<User>>(this._urlGetUsers + '/' + encodeURI(analyse_id), { headers: HttpHelper.getHeaders() })
    .pipe(
      map((res: Array<any>) => {
        const users: Array<User> = [];
        res.forEach(usr => {
            const user = {
              guid: usr.user['usr_id'],
              email: usr.user['usr_email'],
              firstName: usr.user['usr_firstname'],
              lastName: usr.user['usr_lastname'],
              role: usr.role
            };
            users.push(user);
          });
        return users;
      }),
      catchError(HttpHelper.handleError)
    );
  }
}
