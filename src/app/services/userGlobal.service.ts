import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { Analysis } from '../analysis/api-models/analysis.model';
import { User } from '../models/user';

@Injectable()
export class UserGlobalService {

  public userSubject = new ReplaySubject<User>();
  public usersSubject = new ReplaySubject<Array<User>>();
  private _user: User;
  private _users: Array<User>;

  constructor() {
  }

  public set user(user: User) {
    this._user = user;
    this.userSubject.next(user);
  }

  public get user() {
    return this._user;
  }

  public set users(users: Array<User>) {
    this._users = users;
    this.usersSubject.next(users);
  }

  public get users() {
    return this._users;
  }

  public formatName(firstName: string, lastName: string): string {
    let name = '';
    if (firstName) {
      name +=  firstName + ' ';
    }

    if (lastName) {
      name +=  lastName;
    }

    return name.trim();
  }
}
