import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from '../app.config';
import { Observable, of } from 'rxjs';
import { HttpHelper } from '../helpers/http.helper';
import { catchError } from 'rxjs/operators';
import { Role } from '../models/role';

@Injectable()
export class RoleService {

  private _urlApiUser: string;
  private _urlGetRoles: string;

  constructor(private _http: HttpClient,
    appConfig: AppConfig) {
    this._urlApiUser = appConfig.getConfig().apiUserUrl;
    this._urlGetRoles = this._urlApiUser + '/roles';
  }

  public getRoles(): Observable<Array<Role>> {
    return this._http.get<Array<Role>>(this._urlGetRoles, { headers: HttpHelper.getHeaders() }).pipe(catchError(HttpHelper.handleError));
  }
}
