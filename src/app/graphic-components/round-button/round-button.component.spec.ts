import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RoundButtonComponent } from './round-button.component';

describe('RoundButtonComponent', () => {
  let component: RoundButtonComponent;
  let fixture: ComponentFixture<RoundButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoundButtonComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoundButtonComponent);
    component = fixture.componentInstance;
    component.text = 'Romain Huber';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Test getRightFormatText', () => {
    it('should return RH', () => {
      expect(component.getRightFormatText()).toEqual('RH');
    });

    it('should return H if userName is Huber', () => {
      component.text = 'Huber';
      expect(component.getRightFormatText()).toEqual('H');
    });

    it('should return without modification when starting with + (+3)', () => {
      component.text = '+3';
      expect(component.getRightFormatText()).toEqual('+3');
    });

    it('should return without modification when starting with + (+13)', () => {
      component.text = '+13';
      expect(component.getRightFormatText()).toEqual('+13');
    });
  });
});
