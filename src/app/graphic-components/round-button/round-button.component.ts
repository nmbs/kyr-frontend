import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-round-button',
  templateUrl: './round-button.component.html',
  styleUrls: ['./round-button.component.scss']
})
export class RoundButtonComponent implements OnInit {

  @Input()
  public tooltipText: string;

  @Input()
  public text: string;

  @Input()
  public width: string;

  @Input()
  public height: string;

  @Input()
  public backgroundColor: string;

  @Input()
  public borderColor: string;

  public displayTooltip = false;

  constructor() { }

  ngOnInit() {
  }

  public getRightFormatText() {
    if (this.text.startsWith('+')) {
      return this.text;
    }
    try {
      return this.text.split(' ').map((n) => n[0]).join('').toUpperCase();
    } catch (e) {
      return this.text;
    }
  }

  triggerHelp() {
    this.displayTooltip = !this.displayTooltip;
  }

}
