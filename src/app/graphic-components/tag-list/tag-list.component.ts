import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Tag } from 'src/app/models/tag';

@Component({
  selector: 'app-tag-list',
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.scss']
})
export class TagListComponent implements OnInit {
  /**
   * Array label / tooltip
   */
  @Input()
  public listTag: Array<Tag>;

  @Output() removeTagEvent = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  // emit removeEvent to his parent
  public removeTag(id: string) {
    this.removeTagEvent.emit(id);
  }
}
