import { Component, OnInit, Output, Input, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit {
  @ViewChild('input') input: ElementRef;
  @Output() blurEvent = new EventEmitter<void>();
  @Output() changeEvent = new EventEmitter<string>();
  @Output() changeTextAreaEvent = new EventEmitter<string>();
  @Output() enterEvent = new EventEmitter<void>();

  @Input()
  public text: string;

  @Input()
  public placeholder: string;

  @Input()
  public isArea: boolean;

  @Input()
  public rows: number;

  @Input()
  public value: any;

  @Input()
  public type: string;

  @Input()
  public maxlength: number;

  @Input()
  public verticalPadding: number;

  @Input()
  public fontSize: number;

  @Input()
  public hasNoActiveMode: boolean;

  @Input()
  public icon: string;

  @Input()
  public width: number;

  @Input()
  public disabled: boolean;

  constructor() { }

  ngOnInit() {
  }

  public onBlur() {
    this.blurEvent.emit();
  }

  public onEnter() {
    this.enterEvent.emit();
  }

  public onChange(value) {
    this.changeEvent.emit(value);
  }

  public onChangeTextArea(value) {
    this.changeTextAreaEvent.emit(value);
  }

  getStyle() {
    const style = {};
    if (this.icon && !this.isArea) {
      style['background-image'] = 'url(\'' + this.icon + '\')';
      style['background-repeat'] = 'repeat-y';
      style['background-position'] = '8px center';
    }
    if (this.verticalPadding) {
      style['padding-top'] = this.verticalPadding + 'px';
      style['padding-bottom'] = (this.verticalPadding - 2) + 'px';
    }
    if (this.fontSize) {
      style['font-size'] = this.fontSize + 'px';
    }
    return style;
  }

  public getBackgroundImageStyle() {
    let style = '';
    if (this.icon && !this.isArea) {
      style = 'url(' + this.icon + ')';
    }
    return style;
  }

}
