import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyButtonComponent } from './modify-button.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { AppConfig } from 'src/app/app.config';

describe('ModifyButtonComponent', () => {
  let component: ModifyButtonComponent;
  let fixture: ComponentFixture<ModifyButtonComponent>;

  beforeEach(async(() => {
    const appConfigSpy = {
      getConfig: () => {
        return { animationAssets: {} };
      }
    };
    TestBed.configureTestingModule({
      declarations: [ ModifyButtonComponent ],
      providers: [{ provide: AppConfig, useValue: appConfigSpy }],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
