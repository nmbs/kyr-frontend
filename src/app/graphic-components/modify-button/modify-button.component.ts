import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { AppConfig } from 'src/app/app.config';

enum AnimEvent {
  In = 1,
  Out = 2,
  Click = 3
}

@Component({
  selector: 'app-modify-button',
  templateUrl: './modify-button.component.html',
  styleUrls: ['./modify-button.component.scss']
})
export class ModifyButtonComponent implements OnInit {

  @Output() clickEvent = new EventEmitter<void>();

  @Input()
  public withText: boolean;

  @Input()
  public height: number;
  @Input()
  public width: number;


  public position = AnimEvent.In;

  public lottieConfigTextlessHover: Object;
  public lottieConfigTextlessHoverOut: Object;
  public lottieConfigHover: Object;
  public lottieConfigHoverOut: Object;

  private animHoverTextless: any;
  private animHoverOutTextless: any;
  private animHover: any;
  private animHoverOut: any;

  constructor(private appConfig: AppConfig) { }

  ngOnInit() {
    this.lottieConfigTextlessHoverOut  = {
      animationData:  this.appConfig.getConfig().animationAssets['modify-textless-hover-out'],
      renderer: 'svg',
      autoplay: false,
      loop: false
    };

    this.lottieConfigTextlessHover  = {
      animationData:  this.appConfig.getConfig().animationAssets['modify-textless-hover'],
      renderer: 'svg',
      autoplay: false,
      loop: false
    };

    this.lottieConfigHover  = {
      animationData:  this.appConfig.getConfig().animationAssets['modify-hover'],
      renderer: 'svg',
      autoplay: false,
      loop: false
    };

    this.lottieConfigHoverOut  = {
      animationData:  this.appConfig.getConfig().animationAssets['modify-hover-out'],
      renderer: 'svg',
      autoplay: false,
      loop: false
    };
  }

  public handleAnimationTextlessHover(anim: any) {
    this.animHoverTextless = anim;
    this.animHoverTextless.setSpeed(1);
  }
  public handleAnimationTextlessHoverOut(anim: any) {
    this.animHoverOutTextless = anim;
    this.animHoverOutTextless.setSpeed(1);
  }
  public handleAnimationHover(anim: any) {
    this.animHover = anim;
    this.animHover.setSpeed(1);
  }
  public handleAnimationHoverOut(anim: any) {
    this.animHoverOut = anim;
    this.animHoverOut.setSpeed(1);
  }

  public playHoverInTextless() {
    this.position = AnimEvent.In;
    if (this.animHoverTextless) {
      this.animHoverTextless.stop();
      this.animHoverTextless.play();
    }
  }
  public playHoverOutTextless() {
    this.position = AnimEvent.Out;
    if (this.animHoverOutTextless) {
      this.animHoverOutTextless.stop();
      this.animHoverOutTextless.play();
    }
  }

  public playHoverIn() {
    this.position = AnimEvent.In;
    if (this.animHover) {
      this.animHover.stop();
      this.animHover.play();
    }
  }

  public playHoverOut() {
    this.position = AnimEvent.Out;
    if (this.animHoverOut) {
      this.animHoverOut.stop();
      this.animHoverOut.play();
    }
  }

  public playClick() {
    this.clickEvent.emit();
  }

}
