import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { AppConfig } from 'src/app/app.config';

enum AnimEvent {
  In = 1,
  Out = 2,
  Click = 3
}

@Component({
  selector: 'app-delete-button',
  templateUrl: './delete-button.component.html',
  styleUrls: ['./delete-button.component.scss']
})
export class DeleteButtonComponent implements OnInit {
  @Output() clickEvent = new EventEmitter<void>();

  @Input()
  public withText: boolean;
  @Input()
  public height: number;
  @Input()
  public width: number;
  @Input()
  public disableOnClick: boolean;

  public position = AnimEvent.In;
  public lottieConfigClick: Object;
  public lottieConfigHover: Object;
  public lottieConfigHoverOut: Object;

  public lottieConfigTextlessHover: Object;
  public lottieConfigTextlessHoverOut: Object;

  private animClick: any;
  private animHover: any;
  private animHoverOut: any;

  private animHoverTextless: any;
  private animHoverOutTextless: any;


  public alreadyClicked = false;
  constructor(private appConfig: AppConfig) { }

  ngOnInit() {
    this.lottieConfigClick  = {
      animationData:  this.appConfig.getConfig().animationAssets['delete-click'],
      renderer: 'svg',
      autoplay: false,
      loop: false
    };

    this.lottieConfigHover  = {
      animationData:  this.appConfig.getConfig().animationAssets['delete-hover'],
      renderer: 'svg',
      autoplay: false,
      loop: false
    };

    this.lottieConfigHoverOut  = {
      animationData:  this.appConfig.getConfig().animationAssets['delete-hover-out'],
      renderer: 'svg',
      autoplay: false,
      loop: false
    };

    this.lottieConfigTextlessHoverOut  = {
      animationData:  this.appConfig.getConfig().animationAssets['delete-textless-hover-out'],
      renderer: 'svg',
      autoplay: false,
      loop: false
    };

    this.lottieConfigTextlessHover  = {
      animationData:  this.appConfig.getConfig().animationAssets['delete-textless-hover'],
      renderer: 'svg',
      autoplay: false,
      loop: false
    };

  }

  public handleAnimationClick(anim: any) {
    this.animClick = anim;
    this.animClick.setSpeed(1);
  }

  public handleAnimationHover(anim: any) {
    this.animHover = anim;
    this.animHover.setSpeed(1);
  }
  public handleAnimationHoverOut(anim: any) {
    this.animHoverOut = anim;
    this.animHoverOut.setSpeed(1);
  }

  public handleAnimationHoverTextless(anim: any) {
    this.animHoverTextless = anim;
    this.animHoverTextless.setSpeed(1);
  }
  public handleAnimationHoverOutTextless(anim: any) {
    this.animHoverOutTextless = anim;
    this.animHoverOutTextless.setSpeed(1);
  }

  public playHoverIn() {
    this.position = AnimEvent.In;
    if (this.animHover) {
      this.animHover.stop();
      this.animHover.play();
    }
  }
  public playHoverOut() {
    this.position = AnimEvent.Out;
    if (this.animHoverOut) {
      this.animHoverOut.stop();
      this.animHoverOut.play();
    }
  }

  public playHoverInTextless() {
    this.position = AnimEvent.In;
    if (this.animHoverTextless) {
      this.animHoverTextless.stop();
      this.animHoverTextless.play();
    }
  }
  public playHoverOutTextless() {
    this.position = AnimEvent.Out;
    if (this.animHoverOutTextless) {
      this.animHoverOutTextless.stop();
      this.animHoverOutTextless.play();
    }
  }

  public playClick() {
    this.position = AnimEvent.Click;
    if (this.animClick) {
      this.animClick.stop();
      this.animClick.play();
    }
    if (!this.alreadyClicked || !this.disableOnClick) {
      this.clickEvent.emit();
      this.alreadyClicked = true;
    }
  }
}
