import { Component, OnInit, Renderer2, Output, EventEmitter, Input, ElementRef, ViewChild } from '@angular/core';
import { ImpactDegree } from 'src/app/analysis/models/slider.model';


@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {

  @Output() changeBulletValue = new EventEmitter();

  @Input()
  public smallVersion: boolean;

  @Input()
  public valueToChange: string;

  @Input()
  public size: number;

  @Input()
  public keys: Array<any>;

  public startingValue: number;

  @Input()
  set setStartingValue(value: number) {
    this.startingValue = value;
    this.updateSlider(this.startingValue);
    this.floatingPoint.nativeElement.setAttribute('data-position', this.startingValue - 1);
  }

  @Input()
  public readMode: boolean;

  @ViewChild('point1') point1: ElementRef;
  @ViewChild('point2') point2: ElementRef;
  @ViewChild('point3') point3: ElementRef;
  @ViewChild('point4') point4: ElementRef;
  @ViewChild('floatingPoint') floatingPoint: ElementRef;
  @ViewChild('background') background: ElementRef;

  constructor(public renderer: Renderer2) { }

  ngOnInit() {
    if (this.keys && this.keys.length !== 4) {
      this.keys = [];
    }
  }

  public clickBullet(event, bulletNumber) {
    this.sendChanges(bulletNumber);
    this.updateSlider(bulletNumber);
  }

  updateSlider(startRange) {
    this.floatingPoint.nativeElement.setAttribute('data-position', startRange - 1);
    const points = [this.point1.nativeElement, this.point2.nativeElement, this.point3.nativeElement, this.point4.nativeElement];
    const impactClasses = ['no-impact', 'low-impact', 'medium-impact', 'high-impact'];

    points.forEach(point => {
      points.slice(startRange, 4).includes(point)
        ? this.renderer.removeClass(point, 'active')
        : this.renderer.addClass(point, 'active');
    });

    impactClasses.forEach(impact => {
      this.renderer.removeClass(this.background.nativeElement, impact);
    });

    this.renderer.addClass(this.background.nativeElement, impactClasses[startRange - 1]);
  }

  sendChanges(bulletNumber: number) {
    this.changeBulletValue.emit({ bulletNb: bulletNumber, valueToChange: this.valueToChange });
  }

  public getImpactDegree(): string {
    const impactValue = this.startingValue > 0 ? this.startingValue - 1 : this.startingValue;
    return ImpactDegree[impactValue];
  }

  public getScore() {
    return this.startingValue > 0 ? this.startingValue : this.startingValue;
  }

  public calcLeftTitle(index: number): number {
    const sizePoints = 2 * ( 2 * index + 1);

    return index * ( this.size / 3 ) - sizePoints;
  }

}
