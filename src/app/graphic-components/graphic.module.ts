import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ToggleComponent } from './toggle/toggle.component';
import { TileComponent } from './tile/tile.component';
import { ButtonComponent } from './button/button.component';
import { InputComponent } from './input/input.component';
import { CheckComponent } from './check/check.component';
import { SliderComponent } from './slider/slider.component';
import { TooltipComponent } from './tooltip/tooltip.component';
import { HelpComponent } from './help/help.component';
import { LottieAnimationViewModule } from 'ng-lottie';
import { GaugeComponent } from './gauge/gauge.component';
import { RoundButtonComponent } from './round-button/round-button.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { MatMenuModule, MatButtonModule, MatDatepickerModule, MatFormFieldModule, MatNativeDateModule, MatIconModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalComponent } from './modal/modal.component';
import { TagComponent } from './tag/tag.component';
import { TagListComponent } from './tag-list/tag-list.component';
import { AddButtonComponent } from './add-button/add-button.component';
import { BottomBarComponent } from './bottom-bar/bottom-bar.component';
import { InputTitleComponent } from './input-title/input-title.component';
import { DeleteButtonComponent } from './delete-button/delete-button.component';
import { ModifyButtonComponent } from './modify-button/modify-button.component';
import { StateBarComponent } from './state-bar/state-bar.component';
import { DatePickerHeaderComponent } from './date-picker-header/date-picker-header.component';
import { ListSearchComponent } from './list-search/list-search.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { DownloadButtonComponent } from './download-button/download-button.component';


@NgModule({
  declarations: [
    TileComponent,
    ToggleComponent,
    ButtonComponent,
    InputComponent,
    CheckComponent,
    SliderComponent,
    TooltipComponent,
    HelpComponent,
    GaugeComponent,
    RoundButtonComponent,
    DropdownComponent,
    ModalComponent,
    TagComponent,
    TagListComponent,
    AddButtonComponent,
    BottomBarComponent,
    InputTitleComponent,
    DeleteButtonComponent,
    ModifyButtonComponent,
    StateBarComponent,
    DatePickerHeaderComponent,
    ListSearchComponent,
    DownloadButtonComponent
  ],
  imports: [CommonModule,
    FormsModule,
    LottieAnimationViewModule.forRoot(),
    MatMenuModule,
    MatButtonModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatIconModule,
    ScrollingModule
  ],
  entryComponents: [
    DatePickerHeaderComponent
  ],
  providers: [
  ],
  exports: [
    TileComponent,
    ToggleComponent,
    ButtonComponent,
    InputComponent,
    CheckComponent,
    SliderComponent,
    TooltipComponent,
    HelpComponent,
    GaugeComponent,
    RoundButtonComponent,
    DropdownComponent,
    ModalComponent,
    TagComponent,
    TagListComponent,
    AddButtonComponent,
    BottomBarComponent,
    InputTitleComponent,
    DeleteButtonComponent,
    ModifyButtonComponent,
    StateBarComponent,
    ListSearchComponent,
    DownloadButtonComponent
  ]
})
export class GraphicModule { }
