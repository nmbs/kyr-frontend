import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatePickerHeaderComponent } from './date-picker-header.component';
import { NO_ERRORS_SCHEMA, ViewContainerRef } from '@angular/core';
import { MatCalendar, DateAdapter, MatDatepicker, MatDialog, MAT_DATEPICKER_SCROLL_STRATEGY, MAT_DIALOG_SCROLL_STRATEGY, MAT_DATE_FORMATS } from '@angular/material';
import { Subject } from 'rxjs';
import { Overlay } from '@angular/cdk/overlay';

class MockAnalysisMeasureService {
  public stateChanges: Subject<void> = new Subject();
}

describe('DatePickerHeaderComponent', () => {
  let component: DatePickerHeaderComponent;
  let fixture: ComponentFixture<DatePickerHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DatePickerHeaderComponent],
      providers: [DateAdapter, MatDatepicker, MatDialog, Overlay, ViewContainerRef,
        { provide: MatCalendar, useValue: new MockAnalysisMeasureService() },
        { provide: MAT_DATEPICKER_SCROLL_STRATEGY, useValue: {} },
        { provide: MAT_DIALOG_SCROLL_STRATEGY, useValue : {}},
        { provide: MAT_DATE_FORMATS, useValue : {}}],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatePickerHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
