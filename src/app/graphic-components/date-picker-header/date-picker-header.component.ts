import { Component, ChangeDetectionStrategy, OnDestroy, Host, Inject, ChangeDetectorRef } from '@angular/core';
import { Moment } from 'moment';
import { MatDatepicker, MatCalendar, DateAdapter, MAT_DATE_FORMATS, MatDateFormats } from '@angular/material';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

const monthNames = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin',
  'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'
];

@Component({
  selector: 'app-datepicker-header',
  templateUrl: './date-picker-header.component.html',
  styleUrls: ['./date-picker-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  })
export class DatePickerHeaderComponent implements OnDestroy {

  private destroyed = new Subject<void>();

  constructor(@Host() private calendar: MatCalendar<Moment>,
    private dateAdapter: DateAdapter<Moment>,
    private datepicker: MatDatepicker<Moment>,
    @Inject(MAT_DATE_FORMATS) private dateFormats: MatDateFormats,
    cdr: ChangeDetectorRef) {
    calendar.stateChanges
      .pipe(takeUntil(this.destroyed))
      .subscribe(() => cdr.markForCheck());
  }

  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
  }

  get periodLabel(): string {
    if (this.calendar.activeDate) {
      return this.getDateString(new Date(this.calendar.activeDate.toJSON()));
    } else {
      return this.getDateString(new Date());
    }
  }

  public previousViewClicked() {
    this.calendar.currentView = 'multi-year';
  }

  public previousMultiYearClicked(mode: 'year') {
    this.calendar.activeDate = mode === 'year' ?
      this.dateAdapter.addCalendarYears(this.calendar.activeDate, -25) : null;
  }

  public nextMultiYearClicked(mode: 'year') {
    this.calendar.activeDate = mode === 'year' ?
      this.dateAdapter.addCalendarYears(this.calendar.activeDate, 25) : null;
  }

  public closeClicked() {
    this.datepicker.close();
  }

  public isYearView(): boolean {
    return this.calendar.currentView === 'multi-year';
  }

  private getDateString(date: Date): string {
    return this.calendar.currentView === 'multi-year' ?
      monthNames[date.getMonth()] + ' ' + date.getFullYear() :
      date.getFullYear().toString();
  }
}