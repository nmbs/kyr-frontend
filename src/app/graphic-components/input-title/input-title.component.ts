import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-input-title',
  templateUrl: './input-title.component.html',
  styleUrls: ['./input-title.component.scss']
})
export class InputTitleComponent implements OnInit, AfterViewInit {
  @ViewChild('inputTitle', {read: ElementRef}) inputTitle: ElementRef;

  constructor() { }

  @Output() public endFocusEvent = new EventEmitter<string>();

  @Input()
  public value: string;

  @Input()
  public placeHolder: string;

  @Input()
  public maxLength: number;

  @Input()
  public focused: boolean;

  @Input()
  public displayError = false;

  @Input()
  public errorText: string;

  ngOnInit() {
  }

  public endFocus(value: string) {
    this.endFocusEvent.emit(value.trim());
  }

  ngAfterViewInit() {
    if (this.focused) {
      this.inputTitle.nativeElement.focus();
    }
  }
}
