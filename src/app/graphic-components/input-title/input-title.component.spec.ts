import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputTitleComponent } from './input-title.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('InputTitleComponent', () => {
  let component: InputTitleComponent;
  let fixture: ComponentFixture<InputTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InputTitleComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
