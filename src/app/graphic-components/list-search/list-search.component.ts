import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import * as unorm from 'unorm';
import { UserGlobalService } from '../../services/userGlobal.service';
import { Tag } from 'src/app/models/tag';
import { SearchListItem } from './SearchListItem';
import * as _ from 'lodash';

@Component({
  selector: 'app-list-search',
  templateUrl: './list-search.component.html',
  styleUrls: ['./list-search.component.scss']
})
export class ListSearchComponent implements OnInit {

  @Input()
  title: string;

  @Input()
  subTitle: string;

  @Input()
  entryList: Array<SearchListItem>;

  @Input()
  inputPlaceholder: string;

  @Input()
  roundButtonDisplayed = true;

  @Input()
  canCreateEntry = false;

  @Input()
  searchAllFields = true;

  @Output()
  validateEvent = new EventEmitter<Array<any>>();

  @Output()
  validateCustomEntriesEvent = new EventEmitter<Array<string>>();

  public searchInput: string;
  public entryTagToAdd: Array<Tag> = [];
  public selectedEntriesToAdd: Array<any> = [];
  public createdEntriesToAdd: Array<string> = [];

  public displayedItems: Array<SearchListItem> = [];

  // Used to track custom tags in order to delete them individualy when clicking the cross icon
  // We use a negative increment to be clear that this is not an official ID, but a way to handle the object locally
  public idCustomEntryTag = -1;
  constructor(private userGlobalService: UserGlobalService) { }

  ngOnInit() {
  }

  // Emit one or two event containing the full objects added to the list
  // And the list of custom label created by the user
  public validateClickEvent() {
    this.validateEvent.emit(this.selectedEntriesToAdd);
    if (this.canCreateEntry) {
      this.validateCustomEntriesEvent.emit(this.createdEntriesToAdd);
    }
  }


  public addNewEntry() {
    // If a custom object with the same name has not already been created
    // add it to the tag list and to the createdEntriesToAdd list
    if (!this.createdEntriesToAdd.some(x => x === this.searchInput)) {
      this.createdEntriesToAdd.push(this.searchInput);
      const tag: Tag = { id: this.idCustomEntryTag.toString(), tooltip: this.searchInput, label: this.searchInput };
      this.entryTagToAdd.push(tag);
      this.idCustomEntryTag--;
    }
  }

  public addSelectedEntry(entryObj: SearchListItem) {
    // If it's not already in, add in the tag list
    if (!this.entryTagToAdd.some(x => x.id === entryObj.guid)) {
      const tag: Tag = { id: entryObj.guid, tooltip: entryObj.subStr, label: entryObj.str1 };
      this.entryTagToAdd.push(tag);
      this.selectedEntriesToAdd.push(entryObj.obj);
    }
  }

  // Cross icon has been click, we check wich type of object we need to remove from the entryObj lists
  public deleteTag(id: string) {
    const t = this.entryTagToAdd.find(tag => tag.id === id);
    this.entryTagToAdd = this.entryTagToAdd.filter(tag => tag.id !== id);
    this.selectedEntriesToAdd = this.selectedEntriesToAdd.filter(measure => measure.mea_id !== id);
    if (t) {
      this.createdEntriesToAdd = this.createdEntriesToAdd.filter(str => str !== t.label);
    }
  }

  // Check if the current input is in the list of created entries
  public isEntryInList() {
    return this.createdEntriesToAdd.some(x => x === this.searchInput);
  }

  // An item with more than 35 character cannot be created
  public isInputTooLong() {
    return this.searchInput.length > 35;
  }

  // Check if a given object is present in the list of entry object already added
  public isAdded(obj: SearchListItem) {
    return this.selectedEntriesToAdd.some(x => x.mea_id === obj.guid);
  }

  // Check if one of the result list has at least one element
  public canValidate() {
    return this.selectedEntriesToAdd.length < 1 && this.createdEntriesToAdd.length < 1;
  }

  // Check if the 'create custom entry' is displayed
  // Can be displayed if the input string is not exactly equal to an existing object 'str1'
  // And the input has at least 2 characters
  public canCreateNewEntry(): boolean {
    let res = false;
    if (this.searchInput && this.searchInput.length >= 2) {
      if (_.find(this.displayedItems, { str1: this.searchInput }) == null) {
        res = true;
      }
    }
    return res;
  }

  // Returns the current searchInput
  public getNewEntry() {
    return this.searchInput;
  }

  // Highlight part of the "text" present in the "searchInput"
  public getHighlight(text: string) {
    if (this.searchInput && text) {
      // Get started index of the searchInput in the text
      const startIndex = this.getNoAccentNoCaseWord(text).indexOf(this.getNoAccentNoCaseWord(this.searchInput));
      if (startIndex !== -1) {
        // Set the found text style to bold
        const endLength = this.searchInput.length;
        const matchingString = text.substr(startIndex, endLength);
        return text.replace(matchingString, '<b>' + matchingString + '</b>');
      }
    }
    return text;
  }

  // Normalize text without accent and capital letters
  private getNoAccentNoCaseWord(str: String) {
    return unorm.nfd(str.toLowerCase()).replace(/[\u0300-\u036f]/g, '');
  }
  // Concat and trim Firstname and Lastname
  public formatEntries(str1: string, str2: string) {
    return this.userGlobalService.formatName(str1, str2);
  }
  // Get list filtered users
  public getDisplayedEntries() {
    this.displayedItems = this.entryList;
    // if search input filter the user list by it with no Accent and no Case
    if (this.searchInput) {
      const formatSearchInput = this.getNoAccentNoCaseWord(this.searchInput);

      // Filter by Email, Lastname or Firstname
      if (this.searchAllFields) {
        this.displayedItems = this.entryList.filter(usr => (usr.subStr && this.getNoAccentNoCaseWord(usr.subStr).includes(formatSearchInput)) ||
          (usr.str1 && this.getNoAccentNoCaseWord(usr.str1).includes(formatSearchInput)) ||
          (usr.str2 && this.getNoAccentNoCaseWord(usr.str2).includes(formatSearchInput)));
      } else {
        this.displayedItems = this.entryList.filter(usr => (usr.str1 && this.getNoAccentNoCaseWord(usr.str1).includes(formatSearchInput)) ||
          (usr.str2 && this.getNoAccentNoCaseWord(usr.str2).includes(formatSearchInput)));
      }

    }
    return this.displayedItems;
  }
}
