export interface SearchListItem {
    str1: string;
    str2?: string;
    subStr: string;
    guid: string;
    obj: any;
  }