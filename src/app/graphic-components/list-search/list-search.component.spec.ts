import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { UserGlobalService } from '../../services/userGlobal.service';
import { Measure } from '../../analysis/models/measure/measure';
import { ListSearchComponent } from './list-search.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SearchListItem } from './SearchListItem';

describe('ListSearchComponent', () => {
  let component: ListSearchComponent;
  let fixture: ComponentFixture<ListSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ScrollingModule],
      declarations: [ListSearchComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [UserGlobalService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Test addNewEntry', () => {
    it('component.createdEntriesToAdd.length should equal to 1', () => {
      component.searchInput = 'test';
      component.addNewEntry();
      expect(component.createdEntriesToAdd.length).toEqual(1);
    });
  });

  describe('Test addSelectedEntry', () => {
    it('component.selectedEntriesToAdd.length should equal to 1', () => {
      const entryObj: SearchListItem = {
        guid: 'guid',
        obj: null,
        str1: '',
        str2: '',
        subStr: ''
      };
      component.addSelectedEntry(entryObj);
      expect(component.selectedEntriesToAdd.length).toEqual(1);
    });
  });

  describe('Test deleteTag', () => {
    it('component.selectedEntriesToAdd.length should equal to 0', () => {
      const entryObj: SearchListItem = {
        guid: 'guid',
        obj: {
          mea_id: 'guid2'
        },
        str1: '',
        str2: '',
        subStr: ''
      };
      component.addSelectedEntry(entryObj);
      component.deleteTag('guid2');
      expect(component.selectedEntriesToAdd.length).toEqual(0);
    });
    it('component.createdEntriesToAdd.length should equal to 0', () => {
      component.searchInput = 'test';
      component.addNewEntry();
      component.deleteTag('-1');
      expect(component.createdEntriesToAdd.length).toEqual(0);
    });
  });

  describe('Test isEntryInList', () => {
    it('isEntryInList should return true', () => {
      component.searchInput = 'test';
      component.addNewEntry();
      const res = component.isEntryInList();
      expect(res).toEqual(true);
    });
  });

  describe('Test isInputTooLong', () => {
    it('isInputTooLong should return true', () => {
      component.searchInput = '012345678901234567890123456789123456';
      const res = component.isInputTooLong();
      expect(res).toEqual(true);
    });
  });

  describe('Test isAdded', () => {
    it('isAdded should return true', () => {
      const entryObj: SearchListItem = {
        guid: 'guid',
        obj: {
          mea_id: 'guid'
        },
        str1: '',
        str2: '',
        subStr: ''
      };
      component.addSelectedEntry(entryObj);
      const res = component.isAdded(entryObj);
      expect(res).toEqual(true);
    });
  });

  describe('Test canValidate', () => {
    it('canValidate should return false', () => {
      component.searchInput = 'test';
      component.addNewEntry();
      const res = component.canValidate();
      expect(res).toEqual(false);
    });
  });

  describe('Test canCreateNewEntry', () => {
    it('canCreateNewEntry should return true', () => {
      component.searchInput = 'test';
      component.addNewEntry();
      const res = component.canCreateNewEntry();
      expect(res).toEqual(true);
    });
  });

  describe('Test getNewEntry', () => {
    it('getNewEntry should return test', () => {
      component.searchInput = 'test';
      component.addNewEntry();
      const res = component.getNewEntry();
      expect(res).toEqual('test');
    });
  });

});
