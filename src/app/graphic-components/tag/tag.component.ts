import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Tag } from 'src/app/models/tag';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.scss']
})
export class TagComponent implements OnInit {
  @Input()
  public tag: Tag;

  @Output() removeEvent = new EventEmitter<string>();

  public displayTooltip = false;

  constructor() { }

  ngOnInit() {
  }

  public getLabel(): string {
    return this.tag.label;
  }

  public getTooltip(): string {
    return this.tag.tooltip;
  }

  // emit removeEvent to his parent
  public remove() {
    this.removeEvent.emit(this.tag.id);
  }

  triggerHelp() {
    this.displayTooltip = !this.displayTooltip;
  }
}
