import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent implements OnInit {

  @Input()
  public text: string;

  @Input()
  public items: Array<string>;

  @Input()
  public icon: Array<string>;

  constructor() { }

  ngOnInit() {
  }

}
