import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StateBarComponent } from './state-bar.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { AnalysisGlobalService } from 'src/app/analysis/analysisGlobal.service';

describe('StateBarComponent', () => {
  let component: StateBarComponent;
  let fixture: ComponentFixture<StateBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StateBarComponent ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [AnalysisGlobalService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StateBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
