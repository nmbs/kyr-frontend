import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { AnalysisGlobalService } from 'src/app/analysis/analysisGlobal.service';

@Component({
  selector: 'app-state-bar',
  templateUrl: './state-bar.component.html',
  styleUrls: ['./state-bar.component.scss']
})
export class StateBarComponent implements OnInit {

  public isSaving = false;

  @Output() clickEventRightButton = new EventEmitter<void>();

  @Output() clickEventLeftButton = new EventEmitter<void>();

  @Input()
  public rightButtonText: string;

  @Input()
  public leftButtonText: string;

  @Input()
  public leftButtondisabled = false;

  @Input()
  public rightButtondisabled = false;

  @Input()
  public leftText: string;

  @Input()
  public diplaySaveText = true;
  constructor(private analysisGlobalService: AnalysisGlobalService) { }

  @Input()
  public centerElements = false;
  ngOnInit() {
    this.analysisGlobalService.isSendingDoingQuerySubject.subscribe(res => {
      this.isSaving = res;
    });
  }

  public getSavingText() {
    if (!this.isSaving) {
      return 'Enregistré';
    } else {
      return 'En cours d\'enregistrement...';
    }
  }

  clickRightButton() {
    this.clickEventRightButton.emit();
  }

  clickLeftButton() {
    this.clickEventLeftButton.emit();
  }
}
