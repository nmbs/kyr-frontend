import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.scss']
})
export class ToggleComponent implements OnInit {

  @Output() clickEvent = new EventEmitter<void>();

  @Input()
  public active: boolean;

  constructor() { }

  ngOnInit() {
  }

  click() {
    this.clickEvent.emit();
  }

}
