import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { AppConfig } from 'src/app/app.config';

enum AnimEvent {
  In = 1,
  Out = 2,
  Click = 3
}

@Component({
  selector: 'app-add-button',
  templateUrl: './add-button.component.html',
  styleUrls: ['./add-button.component.scss']
})
export class AddButtonComponent implements OnInit {
  @Output() clickEvent = new EventEmitter<void>();

  @Input()
  public buttonWithText = false;

  @Input()
  public disabled = false;

  @Input()
  width: number;
  @Input()
  height: number;
  // IE
  @Input()
  isSmall = false;

  public clicked = false;
  public isIE = true;

  public position = AnimEvent.In;
  public lottieConfigClick: Object;
  public lottieConfigHoverIn: Object;
  public lottieConfigHoverOut: Object;
  public lottieConfigTextHover: Object;
  public lottieConfigTextClick: Object;
  public lottieConfigTextHoverOut: Object;

  private animClick: any;
  private animHover: any;
  private animHoverOut: any;

  constructor(private appConfig: AppConfig) { }

  ngOnInit() {
    const uA = window.navigator.userAgent;
    this.isIE = uA.indexOf('MSIE') > -1 || uA.indexOf('Trident/') > -1;
    this.lottieConfigClick = {
      animationData: this.appConfig.getConfig().animationAssets['add-contributor-click'],
      renderer: 'svg',
      autoplay: false,
      loop: false
    };

    this.lottieConfigHoverIn = {
      animationData: this.appConfig.getConfig().animationAssets['add-contributor-hover-in'],
      renderer: 'svg',
      autoplay: false,
      loop: false
    };

    this.lottieConfigHoverOut = {
      animationData: this.appConfig.getConfig().animationAssets['add-contributor-hover-out'],
      renderer: 'svg',
      autoplay: false,
      loop: false
    };

    this.lottieConfigTextHover = {
      animationData: this.appConfig.getConfig().animationAssets['add-hover'],
      renderer: 'svg',
      autoplay: false,
      loop: false
    };
    this.lottieConfigTextHoverOut = {
      animationData: this.appConfig.getConfig().animationAssets['add-hover-out'],
      renderer: 'svg',
      autoplay: false,
      loop: false
    };
    this.lottieConfigTextClick = {
      animationData: this.appConfig.getConfig().animationAssets['add-click'],
      renderer: 'svg',
      autoplay: false,
      loop: false
    };
  }

  public handleAnimationClick(anim: any) {
    this.animClick = anim;
    this.animClick.setSpeed(1);
  }

  public handleAnimationHoverIn(anim: any) {
    this.animHover = anim;
    this.animHover.setSpeed(1);
  }

  public handleAnimationHoverOut(anim: any) {
    this.animHoverOut = anim;
    this.animHoverOut.setSpeed(1);
  }

  public playClick() {
    if (!this.disabled) {
      this.position = AnimEvent.Click;
      if (this.animClick) {
        this.animClick.stop();
        this.animClick.play();
      }
      this.clickEvent.emit();
    }
  }

  public playHoverIn() {
    if (!this.disabled) {
      this.position = AnimEvent.In;
      if (this.animHover) {
        this.animHover.stop();
        this.animHover.play();
      }
    }
  }

  public playHoverOut() {
    if (!this.disabled) {
      // Does not play if after click
      if (this.position !== AnimEvent.Click && this.animHoverOut) {
        this.position = AnimEvent.Out;
        this.animHoverOut.stop();
        this.animHoverOut.play();
      }
    }
  }

  public clickedIE() {
    if (!this.disabled) {
      this.clicked = true;
      this.clickEvent.emit();
      setTimeout(() => {
        this.clicked = false;
      }, 500);
    }
  }
}
