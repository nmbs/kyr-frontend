import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TileComponent } from './tile.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Choice } from 'src/app/analysis/models/choice.model';

const mockChoices: Array<Choice> = [{
  cho_id: '3f9bb204-4f09-11e9-9341-064a82272d84',
  cho_name: 'Entité',
  cho_subtitle: '',
  cho_icon: 'icon',
  cho_help: '',
  checked: true
}, {
  cho_id: '3f9bb241-4f09-11e9-9341-064a82272d84',
  cho_name: 'Groupe',
  cho_subtitle: '',
  cho_icon: 'icon',
  cho_help: '',
  checked: false
}];

describe('TileComponent', () => {
  let component: TileComponent;
  let fixture: ComponentFixture<TileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TileComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('getChoiceIcon', () => {
    beforeEach(() => {
    });

    it('should choice active icon', () => {
      expect(component.getChoiceIcon(mockChoices[0])).toEqual('/assets/img/form/' + mockChoices[0].cho_icon + '-active.svg');
    });
    it('should choice icon', () => {
      expect(component.getChoiceIcon(mockChoices[1])).toEqual('/assets/img/form/' + mockChoices[0].cho_icon + '.svg');
    });
  });

  describe('select', () => {
    beforeEach(() => {
    });

    it('should do nothing', () => {
      spyOn(component.selectEvent, 'emit');
      component.choice = mockChoices[1];
      component.disabled = true;
      component.select();
      expect(component.selectEvent.emit).not.toHaveBeenCalled();
    });
    it('should emit select event (disabled)', () => {
      spyOn(component.selectEvent, 'emit');
      component.choice = mockChoices[0];
      component.disabled = true;
      component.select();
      expect(component.selectEvent.emit).toHaveBeenCalledWith([mockChoices[0], mockChoices[0].checked]);
    });
    it('should emit select event', () => {
      spyOn(component.selectEvent, 'emit');
      component.choice = mockChoices[0];
      component.disabled = false;
      component.select();
      expect(component.selectEvent.emit).toHaveBeenCalledWith([mockChoices[0], mockChoices[0].checked]);
    });
  });
});
