import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Choice } from 'src/app/analysis/models/choice.model';
import * as lozad from 'lozad';
import { Moment } from 'moment';
import { MatDatepicker, MAT_DATE_FORMATS } from '@angular/material';
import { FormControl } from '@angular/forms';
import { DatePickerHeaderComponent } from '../date-picker-header/date-picker-header.component';

export const datePickerFormat = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-tile',
  templateUrl: './tile.component.html',
  styleUrls: ['./tile.component.scss'],
  providers: [
    { provide: MAT_DATE_FORMATS, useValue: datePickerFormat }
  ]
})
export class TileComponent implements OnInit {
  @ViewChild('picker') datePicker: MatDatepicker<Date>;

  @Output() selectEvent = new EventEmitter<[any, boolean]>();

  @Output() dateEvent = new EventEmitter<Choice>();

  @Input()
  public choice: Choice;

  @Input()
  public disabled: any;

  @Input()
  public thin: boolean;

  @Input()
  public placeholder: boolean;

  @Input()
  public hasCheck: boolean;

  @Input()
  public fullBackground = false;

  @Input()
  public radioButton: boolean;

  @Input()
  public isRed: boolean;

  @Input()
  public isGreen: boolean;

  public isLazyLoaded = false;
  private _formImgPath = '/assets/img/form/';
  public dateControle;
  public datePickerHeader = DatePickerHeaderComponent;
  public minDate = new Date();
  constructor() { }

  ngOnInit() {
    // Lozad handle the images LazyLoad
    const lazyLoadObserver = lozad('.lozad');
    lazyLoadObserver.observe();

    this.updateDateControle();
  }

  // Get icon path
  public getChoiceIcon(choiceInput: Choice) {
    if (choiceInput.checked) {
      return this._formImgPath + choiceInput.cho_icon + '-active.svg';
    } else {
      return this._formImgPath + choiceInput.cho_icon + '.svg';
    }
  }
  // When the image is loaded, this css class is added
  public imagesLoaded(event) {
    event.target.classList.add('loaded');
    this.isLazyLoaded = true;
  }
  public showTile() {
    let show = false;
    if (this.choice && this.choice.cho_icon) {
      if (this.isLazyLoaded) {
        show = true;
      }
    } else {
      show = true;
    }
    return show;
  }

  public select() {
    if (this.choice.isDatePicker) {
      if (this.choice.date) {
        this.selectEvent.emit([this.choice, this.choice.checked]);
      } else {
        this.datePicker.open();
      }
    } else {
      if (this.choice.checked || !this.disabled) {
        this.selectEvent.emit([this.choice, this.choice.checked]);
      }
    }
  }

  public chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    this.choice.date = new Date(normalizedMonth.toJSON());
    this.updateDateControle();
    this.dateEvent.emit(this.choice);
    datepicker.close();
  }

  public previousClicked(datepicker: MatDatepicker<Moment>) {
    datepicker.startView = 'multi-year';
  }

  private updateDateControle() {
    if (this.choice) {
      if (this.choice.date) {
        this.dateControle = new FormControl(this.choice.date);
      } else {
        this.dateControle = new FormControl(new Date());
      }
    }
  }
}
