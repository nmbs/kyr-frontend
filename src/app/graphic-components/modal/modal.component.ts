import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppConfig } from 'src/app/app.config';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  @Input()
  public set displayed(displayed: boolean) {
    this._displayed = displayed;
  }

  @Input()
  public canCloseByClickingOutside = true;

  @Input()
  public isFull = false;

  @Input()
  public isBig = false;

  @Input()
  public hasBottomBar = false;

  @Output() public closeEvent = new EventEmitter<boolean>();

  private _displayed: boolean;
  private anim: any;
  public lottieConfig: Object;

  constructor(private appConfig: AppConfig) { }

  ngOnInit() {
    this.loadConfig();
  }

  public get displayed() {
    return this._displayed;
  }

  private loadConfig() {
    this.lottieConfig = {
      animationData: this.appConfig.getConfig().animationAssets['picto-close'],
      renderer: 'svg',
      autoplay: true,
      loop: false
    };
  }

  public handleAnimation(anim: any) {
    this.anim = anim;
    this.anim.setSpeed(1);
  }

  public play() {
    if (this.lottieConfig === undefined) {
      this.loadConfig();
    }
    if (this.anim) {
      this.anim.stop();
      this.anim.play();
    }
  }

  public pause() {
    if (this.anim) {
      this.anim.stop();
    }
  }

  public close(isOutside: boolean) {
    if (isOutside && this.canCloseByClickingOutside) {
      this.closeEvent.emit(false);
    } else if (!isOutside) {
      this.closeEvent.emit(false);
    }
  }
}
