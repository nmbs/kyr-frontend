import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LottieAnimationViewModule } from 'ng-lottie';
import { ModalComponent } from './modal.component';
import { AppConfig } from '../../app.config';

describe('ModalComponent', () => {
  let component: ModalComponent;
  let fixture: ComponentFixture<ModalComponent>;

  beforeEach(async(() => {
    const appConfigSpy = {
      getConfig: () => {
        return { animationAssets: { } };
      }
    };

    TestBed.configureTestingModule({
      declarations: [ModalComponent],
      imports: [LottieAnimationViewModule],
      providers: [
        { provide: AppConfig, useValue: appConfigSpy },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
