import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-gauge',
  templateUrl: './gauge.component.html',
  styleUrls: ['./gauge.component.scss']
})
export class GaugeComponent implements OnInit {

  private _percent = 0;

  @Input()
  set percent(percent: number) {
    this._percent = percent;
  }

  @Input()
  public radius: number;

  @Input()
  public strokeThickeness: number;

  constructor() { }

  ngOnInit() {
  }

  public getStrokeStyle() {
    return this._percent ? { 'stroke-dasharray': this._percent + ', 100' } : {};
  }

}
