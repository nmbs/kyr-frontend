import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent implements OnInit {

  @Input()
  public help_text: string;

  public displayTooltip = false;

  constructor() { }

  ngOnInit() {
  }


  triggerHelp() {
    this.displayTooltip = !this.displayTooltip;
  }
}
