import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss']
})
export class TooltipComponent implements OnInit {

  @Input()
  public help_text: string;

  @Input()
  public direction: any;

  @Input()
  public display: boolean;

  @Input()
  public width: number;

  @Input()
  public nowrap: boolean;

  constructor() { }

  ngOnInit() {
  }

}
