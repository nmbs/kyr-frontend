import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LottieAnimationViewModule } from 'ng-lottie';
import { AppConfig } from '../../app.config';

import { CheckComponent } from './check.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('CheckComponent', () => {
  let component: CheckComponent;
  let fixture: ComponentFixture<CheckComponent>;

  beforeEach(async(() => {
    const appConfigSpy = {
      getConfig: () => {
        return { animationAssets: { } };
      }
    };

  TestBed.configureTestingModule({
    declarations: [CheckComponent],
    imports: [LottieAnimationViewModule],
    schemas: [NO_ERRORS_SCHEMA],
    providers: [
      { provide: AppConfig, useValue: appConfigSpy },
    ]
  })
    .compileComponents();
}));

beforeEach(() => {
  fixture = TestBed.createComponent(CheckComponent);
  component = fixture.componentInstance;
  fixture.detectChanges();
});

it('should create', () => {
  expect(component).toBeTruthy();
});
});
