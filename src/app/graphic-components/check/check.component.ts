import { Component, OnInit, Input } from '@angular/core';
import { AppConfig } from '../../app.config';

@Component({
  selector: 'app-check',
  templateUrl: './check.component.html',
  styleUrls: ['./check.component.scss']

})
export class CheckComponent implements OnInit {

  public isChecked: boolean;
  public hasRadioButton: boolean;

  @Input()
  set checked(value: boolean) {
    this.isChecked = value;
    if (this.isChecked) {
      this.play();
    }
  }
  @Input()
  set radioButton(value: boolean) {
    this.hasRadioButton = value;
  }

  public lottieConfig: Object;
  private anim: any;

  constructor(private appConfig: AppConfig) {
  }

  ngOnInit() {
    this.loadConfig();

  }
  private loadConfig() {
    this.lottieConfig = {
      animationData: this.appConfig.getConfig().animationAssets['check'],
      renderer: 'svg',
      autoplay: true,
      loop: false
    };
  }

  public handleAnimation(anim: any) {
    this.anim = anim;
    this.anim.setSpeed(2);
  }
  public play() {
    if (this.lottieConfig === undefined)
    {
      this.loadConfig();
    }
    if (this.anim) {
      this.anim.stop();
      this.anim.play();
    }
  }
}
