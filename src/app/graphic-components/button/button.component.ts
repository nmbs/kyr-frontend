import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Output() clickEvent = new EventEmitter<void>();

  @Input()
  public text: string;

  @Input()
  public big: boolean;

  @Input()
  public midSize: boolean;

  @Input()
  public thin: boolean;

  @Input()
  public small: boolean;

  @Input()
  public inverted: boolean;

  @Input()
  public disabled: boolean;

  @Input()
  public icon: string;

  @Input()
  public after: boolean;

  @Input()
  public isBlue: boolean;

  constructor() { }

  ngOnInit() {
  }

  click() {
    if (!this.disabled) {
      this.clickEvent.emit();
    }
  }

}
