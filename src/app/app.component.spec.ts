import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { AnalysisComponent } from './analysis/analysis.component';
import { of, Observable } from 'rxjs';
import { User } from './models/user';
import { UserService } from './services/user.service';
import { AppConfig } from './app.config';
import { UserGlobalService } from './services/userGlobal.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { KeycloakService } from 'keycloak-angular';

const mockUser: User = { guid: 'guid', email: 'email', firstName: 'firstName', lastName: 'lastName', role: null };
const mockUserMapping = { usr_id: 'guid', usr_email: 'email', usr_firstname: 'firstName', usr_lastname: 'lastName', role: null };

class MockKeycloakService {
  loadUserProfile(): Promise<User> {
    return new Promise(function (resolve) {
      resolve(mockUser);
    });
  }
}

class MockUserService {
  getUser(email: string): Observable<any> {
    return of(mockUserMapping);
  }
}


describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    const appConfigSpy = {
      getConfig: () => {
        return { animationAssets: {} };
      }
    };
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([{ path: 'analysis', component: AnalysisComponent },
        ])
      ],
      declarations: [
        AppComponent,
        AnalysisComponent
      ],
      providers: [UserGlobalService, { provide: AppConfig, useValue: appConfigSpy },
        { provide: KeycloakService, useValue: new MockKeycloakService() },
        { provide: UserService, useValue: new MockUserService() }],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  it('should ceate the app', () => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
});
