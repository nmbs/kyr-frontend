
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AnalysisComponent } from './analysis.component';
import { AnalysisListComponent } from './analysis-list/analysis-list.component';
import { AnalysisCreateComponent } from './analysis-create/analysis-create.component';
import { AnalysisDescriptionComponent } from './analysis-description/analysis-description.component';
import { AnalysisImpactComponent } from './analysis-impact/analysis-impact.component';
import { AnalysisFeatureComponent } from './analysis-feature/analysis-feature.component';
import { AnalysisMeasureComponent } from './analysis-measure/analysis-measure.component';
import { AnalysisResidualRiskComponent } from './analysis-residual-risk/analysis-residual-risk.component';

const analysisRoutes: Routes = [
  {
    path: '',
    component: AnalysisListComponent
  },
  { path: 'create', component: AnalysisCreateComponent },
  {
    path: ':guid', component: AnalysisComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'description'
      },
      { path: 'description', component: AnalysisDescriptionComponent },
      { path: 'impact', component: AnalysisImpactComponent },
      { path: 'feature', component: AnalysisFeatureComponent },
      { path: 'measure', component: AnalysisMeasureComponent },
      { path: 'residual-risk', component: AnalysisResidualRiskComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(analysisRoutes)],
  exports: [RouterModule]
})
export class AnalysisRoutingModule { }
