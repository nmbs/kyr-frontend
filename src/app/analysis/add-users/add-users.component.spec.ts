import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { AddUsersComponent } from './add-users.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserService } from '../../services/user.service';
import { RoleService } from '../../services/role.service';
import { UserGlobalService } from '../../services/userGlobal.service';
import { AnalysisGlobalService } from '../analysisGlobal.service';
import { AppConfig } from 'src/app/app.config';
import { MatDialogRef } from '@angular/material';
import { User } from 'src/app/models/user';
import { Role } from 'src/app/models/role';
import { Tag } from 'src/app/models/tag';
import { Analysis } from '../api-models/analysis.model';
import { of } from 'rxjs';

const mockUser: User = {
  guid: 'guid',
  email: 'email',
  firstName: 'firstName',
  lastName: 'lastName',
  role: null
};
const mockUser2: User = {
  guid: 'guid46',
  email: 'email564',
  firstName: 'firstName456',
  lastName: 'lastName46',
  role: null
};
const mockDefaultRole: Role = {
  rol_id: 'rolId',
  rol_label: 'default',
  rol_code: 'DEF',
  rol_description: 'defaultdsc'
};
const mockOwnerRole: Role = {
  rol_id: 'rolId',
  rol_label: 'propriétaire',
  rol_code: 'OWNER',
  rol_description: 'propriétaire'
};
const mockCisoRole: Role = {
  rol_id: 'rolId',
  rol_label: 'ciso',
  rol_code: 'CISO',
  rol_description: 'ciso'
};
const mockAnalysis: Analysis = {
  ral_id: 'string',
  ral_name: 'string',
  ral_created_date: null,
  ral_updated_date: null,
  finalUsers: 'string',
  nbUser: 'string',
  dataSensibility: 'string',
  dataTypes: 'string',
  userAccesses: 'string',
  availabilityWeeks: 'string',
  availabilityDays: 'string',
  traceKeepingLaw: 'string',
  traceKeepingApp: 'string',
  solutionTypes: 'string',
  maxInterruptionTime: 'string',
  impactDataLoss: 'string',
  ral_isBlocked: true,
  features: [
    {
      fet_id: 'string',
      ral_id: 'string',
      fet_label: 'string',
      fet_description: 'string',
      fet_sensitive_nature: 'string',
      fet_created_date: 'string'
    }],
  impacts: null,
  opinion: null,
  decision: null,
  residualRisk: null
};

class MockAnalysisGlobalService {
  analysis: Analysis = mockAnalysis;
}

class MockUserService {
  addUsersToAnalyse(analyseGuid: string, usersBody) {
    return of(mockUser);
  }

  getUsers() {
    return of([mockUser, mockUser2]);
  }
}

class MockRoleService {
  public getRoles() {
    return of([mockDefaultRole, mockOwnerRole]);
  }
}

describe('AddUsersComponent', () => {
  let component: AddUsersComponent;
  let fixture: ComponentFixture<AddUsersComponent>;

  beforeEach(async(() => {
    const matDialogRefSpy = {
      close: function() {}
    };
    const appConfigSpy = jasmine.createSpyObj('AppConfig', {
      getConfig: function () {
        return '';
      }
    });
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, ScrollingModule],
      declarations: [AddUsersComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [UserGlobalService,
        { provide: RoleService, useValue: new MockRoleService() },
        { provide: UserService, useValue: new MockUserService() },
        { provide: AnalysisGlobalService, useValue: new MockAnalysisGlobalService() },
        { provide: MatDialogRef, useValue: matDialogRefSpy },
        { provide: AppConfig, useValue: appConfigSpy }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('getPlaceholder', () => {
    beforeEach(() => {
      component.users = [];
    });

    it('one user', () => {
      component.users.push(mockUser);
      expect(component.getPlaceholder()).toEqual('Recherche parmi 1 collaborateur...');
    });

    it('two users', () => {
      component.users.push(mockUser);
      component.users.push(mockUser);
      expect(component.getPlaceholder()).toEqual('Recherche parmi 2 collaborateurs...');
    });
  });

  describe('getDisplayedUsers', () => {
    beforeEach(() => {
      component.users = [];
      component.users.push(mockUser);
    });

    it('no one', () => {
      component.searchInput = 'noOne';
      expect(component.getDisplayedUsers()).toEqual([]);
    });


    it('email', () => {
      component.searchInput = 'em';
      expect(component.getDisplayedUsers()).toEqual([mockUser]);
    });

    it('firstname', () => {
      component.searchInput = 'fir';
      expect(component.getDisplayedUsers()).toEqual([mockUser]);
    });

    it('with accent', () => {
      component.searchInput = 'firstNamé';
      expect(component.getDisplayedUsers()).toEqual([mockUser]);
    });

    it('without Case', () => {
      component.searchInput = 'firstname';
      expect(component.getDisplayedUsers()).toEqual([mockUser]);
    });

    it('lastname', () => {
      component.searchInput = 'last';
      expect(component.getDisplayedUsers()).toEqual([mockUser]);
    });
  });

  describe('getFilteredRoles', () => {
    beforeEach(() => {
      component.roles = [];
      component.roles.push(mockDefaultRole);
      component.roles.push(mockOwnerRole);
    });

    it('filtered without owner', () => {
      expect(component.getFilteredRoles()).toEqual([mockDefaultRole]);
    });
  });

  describe('addUser', () => {
    beforeEach(() => {
    });

    it('add a user with a role and change add the same user with a new role', () => {
      const userAdded: User = Object.assign({}, mockUser);
      userAdded.role = Object.assign({}, mockDefaultRole);
      const userTag: Tag = { id: 'guid', tooltip: 'default', label: 'firstName lastName' };

      component.addUser(mockUser, mockDefaultRole);
      expect(component.usersTagToAdd).toEqual([userTag]);
      expect(component.analysisUsers).toEqual([userAdded]);

      const newUserAdded: User = Object.assign({}, mockUser);
      newUserAdded.role = Object.assign({}, mockOwnerRole);
      const newUserTag: Tag = { id: 'guid', tooltip: 'propriétaire', label: 'firstName lastName' };

      component.addUser(mockUser, mockOwnerRole);
      expect(component.usersTagToAdd).toEqual([newUserTag]);
      expect(component.analysisUsers).toEqual([newUserAdded]);
    });
  });

  describe('deleteUserTag', () => {
    beforeEach(() => {
      component.users = [];
      component.users.push(mockUser);

      const userAdded: User = Object.assign({}, mockUser);
      userAdded.role = Object.assign({}, mockDefaultRole);
      const userTag: Tag = { id: 'guid', tooltip: 'default', label: 'firstName lastName' };

      component.analysisUsers.push(userAdded);
      component.usersTagToAdd.push(userTag);
    });

    it('delete a user ', () => {
      component.deleteUserTag(mockUser.guid);
      expect(component.usersTagToAdd).toEqual([]);
      expect(component.analysisUsers).toEqual([]);
    });
  });

  describe('addUsers', () => {
    beforeEach(() => {
      component.users = [];
      component.users.push(mockUser);

      const ownerUser: User = Object.assign({}, mockUser);
      ownerUser.role = Object.assign({}, mockOwnerRole);
      component.owner = ownerUser;

      const userAdded: User = Object.assign({}, mockUser);
      userAdded.role = Object.assign({}, mockDefaultRole);
      component.analysisUsers.push(userAdded);
    });

    it('add Users, close modal have been called in return', () => {
      component.addUsers();
    });
  });

  describe('isUserAdded', () => {
    beforeEach(() => {
      const userTag: Tag = { id: mockUser.guid, tooltip: 'default', label: 'firstName lastName' };
      component.usersTagToAdd.push(userTag);
    });

    it('user added', () => {
      expect(component.isUserAdded(mockUser)).toEqual(true);
    });
    it('user not added', () => {
      expect(component.isUserAdded(mockUser2)).toEqual(false);
    });
  });

  describe('getHighlight', () => {
    beforeEach(() => {
      component.searchInput = 'first';
    });

    it('highlight first', () => {
      expect(component.getHighlight('firstname')).toEqual('<b>first</b>name');
    });
  });

  describe('tooltipIsDisplayed', () => {
    beforeEach(() => {
      component.tootltip.id = 1;
    });

    it('is Displayed', () => {
      expect(component.tooltipIsDisplayed(1)).toEqual(true);
    });

    it('is not Displayed', () => {
      expect(component.tooltipIsDisplayed(2)).toEqual(false);
    });
  });

  describe('tooltipIsTop', () => {
    beforeEach(() => {
      component.users = [];
      component.users.push(mockUser);
    });

    it('is not top', () => {
      expect(component.tooltipIsTop(1)).toEqual(false);
    });

    it('is top', () => {
      component.users.push(mockUser);
      expect(component.tooltipIsTop(1)).toEqual(true);
    });
  });

  describe('getTooltipDirection', () => {
    beforeEach(() => {
      component.users = [];
      component.users.push(mockUser);
    });

    it('is bottom', () => {
      expect(component.getTooltipDirection(1)).toEqual('bottom');
    });

    it('is top', () => {
      component.users.push(mockUser);
      expect(component.getTooltipDirection(1)).toEqual('top');
    });
  });

  describe('sortUsers', () => {
    const ownerUser1: User = Object.assign({}, mockUser);
    ownerUser1.role = Object.assign({}, mockOwnerRole);
    ownerUser1.email = 'b@a.com';

    const ownerUser2: User = Object.assign({}, mockUser);
    ownerUser2.role = Object.assign({}, mockOwnerRole);
    ownerUser2.email = 'a@a.com';

    const cisoUser1: User = Object.assign({}, mockUser);
    cisoUser1.role = Object.assign({}, mockCisoRole);
    cisoUser1.email = 'b@a.com';

    const cisoUser2: User = Object.assign({}, mockUser);
    cisoUser2.role = Object.assign({}, mockCisoRole);
    cisoUser2.email = 'a@a.com';

    const userDefaultRole1: User = Object.assign({}, mockUser);
    userDefaultRole1.role = Object.assign({}, mockDefaultRole);
    userDefaultRole1.email = 'b@a.com';

    const userDefaultRole2: User = Object.assign({}, mockUser);
    userDefaultRole2.role = Object.assign({}, mockDefaultRole);
    userDefaultRole2.email = 'a@a.com';

    const userWithoutRole1: User = Object.assign({}, mockUser);
    userWithoutRole1.email = 'b@a.com';

    const userWithoutRole2: User = Object.assign({}, mockUser);
    userWithoutRole2.email = 'a@a.com';

    const mockUserList = [userWithoutRole2, userDefaultRole1, cisoUser1, ownerUser1, cisoUser2, ownerUser2, userDefaultRole2, userWithoutRole1];

    beforeEach(() => { });

    it('sort users', () => {
      component.sortUsers(mockUserList);

      expect(mockUserList[0]).toEqual(ownerUser2);
      expect(mockUserList[1]).toEqual(ownerUser1);
      expect(mockUserList[2]).toEqual(cisoUser2);
      expect(mockUserList[3]).toEqual(cisoUser1);
      expect(mockUserList[4]).toEqual(userDefaultRole2);
      expect(mockUserList[5]).toEqual(userDefaultRole1);
      expect(mockUserList[6]).toEqual(userWithoutRole2);
      expect(mockUserList[7]).toEqual(userWithoutRole1);
    });
  });

  describe('close', () => {
    beforeEach(() => {
      const userAdded: User = Object.assign({}, mockUser);
      userAdded.role = Object.assign({}, mockDefaultRole);
      const userTag: Tag = { id: 'guid', tooltip: 'default', label: 'firstName lastName' };

      component.analysisUsers.push(userAdded);
      component.usersTagToAdd.push(userTag);
      component.isDisplayed = true;
    });

    it('no reset', () => {
      component.close();

      expect(component.isDisplayed).toEqual(false);
      expect(component.analysisUsers.length).toEqual(1);
      expect(component.usersTagToAdd.length).toEqual(1);
    });

    it('reset', inject([UserGlobalService], (userGlobalService: UserGlobalService) => {
      userGlobalService.users = [{ guid: 'guid', email: 'email', firstName: 'firstName', lastName: 'lastName', role: null }];
      userGlobalService.user = { guid: 'guid', email: 'email', firstName: 'firstName', lastName: 'lastName', role: null };
      component.close(true);
      expect(component.isDisplayed).toEqual(false);
    }));
  });


});
