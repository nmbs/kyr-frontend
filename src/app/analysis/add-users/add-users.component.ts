import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import * as unorm from 'unorm';

import { MatDialogRef } from '@angular/material';
import { forkJoin as observableForkJoin } from 'rxjs';

import { AnalysisService } from '../analysis.service';
import { UserService } from '../../services/user.service';
import { UserGlobalService } from '../../services/userGlobal.service';
import { RoleService } from '../../services/role.service';

import { AnalysisGlobalService } from '../analysisGlobal.service';
import { User } from '../../models/user';
import { Tag } from '../../models/tag';
import { Role } from '../../models/role';

const CROSS_SIZE = 32;
const MARGIN_ROLE = 8;

@Component({
  selector: 'app-add-users',
  templateUrl: './add-users.component.html',
  styleUrls: ['./add-users.component.scss'],
  providers: [AnalysisService, UserService, RoleService]
})
export class AddUsersComponent implements OnInit {
  @Input() displayed: boolean;
  @Output() closeEvent = new EventEmitter<boolean>();

  public isDisplayed = true;
  public users: Array<User>;
  public roles: Array<Role>;
  public searchInput: string;
  public tootltip: any = { id: null, text: null };
  public buttonHoverIndex = null;

  private _usersTagToAdd: Array<Tag> = [];
  private _analysisUsers: Array<User> = [];
  private _specialRoles = ['owner', 'ciso'];
  private _owner: User;

  constructor(private analysisGlobalService: AnalysisGlobalService,
    private userService: UserService, private roleService: RoleService,
    private userGlobalService: UserGlobalService,
    public dialogRef: MatDialogRef<AddUsersComponent>) {
  }

  ngOnInit() {
    // Get all users except you
    observableForkJoin(this.userService.getUsers(), this.roleService.getRoles()).subscribe(res => {
      this.users = res[0];
      this._owner = this.userGlobalService.users.find(usr => usr.role && usr.role.rol_code.toLowerCase() === 'owner');

      this.userGlobalService.users.forEach((analysisUser: User) => {
        // Set Role to users
        this.users[this.users.findIndex(usr => usr.guid === analysisUser.guid)].role = analysisUser.role;
        // Add all users except curent user
        if (analysisUser.guid !== this._owner.guid) {
          this.addUser(analysisUser);
        }
      });
      this.sortUsers(this.users);
      this.roles = res[1];
    });
  }

  get owner() {
    return this._owner;
  }

  set owner(user: User) {
    this._owner = user;
  }

  set usersTagToAdd(entryList: Array<Tag>) {
    this._usersTagToAdd = entryList;
  }

  get usersTagToAdd() {
    return this._usersTagToAdd;
  }

  set analysisUsers(entryList: Array<User>) {
    this._analysisUsers = entryList;
  }

  get analysisUsers() {
    return this._analysisUsers;
  }

  public getPlaceholder() {
    let placeholder = '';
    if (this.users) {
      placeholder = 'Recherche parmi ' + this.users.length + ' collaborateur';
      placeholder += (this.users.length > 1) ? 's...' : '...';
    }
    return placeholder;
  }

  // Get list filtered users
  public getDisplayedUsers() {
    let displayedUsers = this.users;
    // if search input filter the user list by it with no Accent and no Case
    if (this.searchInput) {
      const formatSearchInput = this.getNoAccentNoCaseWord(this.searchInput);

      // Filter by Email, Lastname or Firstname
      displayedUsers = this.users.filter(usr => (usr.email && this.getNoAccentNoCaseWord(usr.email).includes(formatSearchInput)) ||
        (usr.firstName && this.getNoAccentNoCaseWord(usr.firstName).includes(formatSearchInput)) ||
        (usr.lastName && this.getNoAccentNoCaseWord(usr.lastName).includes(formatSearchInput)));
    }
    return displayedUsers;
  }

  public getFilteredRoles() {
    return this.roles.filter(role => role.rol_code.toLowerCase() !== 'owner');
  }

  // Insert a user in the list of users to Add to Analysis or change its role
  public addUser(user: User, role: Role = null) {
    this.closeTooltip();
    if (role) {
      user.role = Object.assign({}, role);
    }
    // If it's not already in the list push it in user list and tag list
    if (!this._usersTagToAdd.some(x => x.id === user.guid)) {
      const roleLabel = (user.role) ? user.role.rol_label : '';
      const userTag: Tag = { id: user.guid, tooltip: roleLabel, label: this.formatName(user.firstName, user.lastName) };
      this._usersTagToAdd.push(userTag);
      this._analysisUsers.push(user);
      // Else just change its role and refresh it in several list
    } else {
      const roleLabel = (user.role) ? user.role.rol_label : '';
      this._analysisUsers.find(ut => ut.guid === user.guid).role = role;
      this._usersTagToAdd.find(ut => ut.id === user.guid).tooltip = roleLabel;
    }
  }

  public deleteUserTag(id: string) {
    // reset role
    this.users.find(usr => usr.guid === id).role = null;
    this._usersTagToAdd = this._usersTagToAdd.filter(userTag => userTag.id !== id);
    this._analysisUsers = this._analysisUsers.filter(user => user.guid !== id);

    this.closeAll();
  }

  // Add users to analyse
  public addUsers() {
    const usersGuidList = [];
    // Add all users
    this._analysisUsers.forEach((usr: User) => {
      const rolGuid = (usr.role) ? usr.role.rol_id : null;
      usersGuidList.push({ usrGuid: usr.guid, roleGuid: rolGuid });
    });
    // Add the owner too
    usersGuidList.push({ usrGuid: this._owner.guid, roleGuid: this._owner.role.rol_id });
    this.userService.addUsersToAnalyse(this.analysisGlobalService.analysis.ral_id, usersGuidList).subscribe(res => {
      this.userGlobalService.users = Object.assign([], this._analysisUsers);
      // re-add owner
      this.userGlobalService.users.push(Object.assign({}, this._owner));

      // close modal
      this.close();
    });
  }

  public closeAll() {
    // Find the button container to close all the opened button
    const buttonContainers = document.querySelectorAll('#modalAddUser .buttonContainer');
    for (let _i = 0; _i < buttonContainers.length; _i++) {
      buttonContainers[_i].classList.remove('opened');
    }
  }

  // Returns true if the user is in list 'this_userToAdd'
  public isUserAdded(user: User): boolean {
    return this._usersTagToAdd.some(x => x.id === user.guid);
  }

  public isSpecialRole(user: User) {
    return user.role && this._specialRoles.some(rol => rol === user.role.rol_code.toLowerCase());
  }

  // Concat and trim Firstname and Lastname
  public formatName(firstname: string, lastname: string) {
    return this.userGlobalService.formatName(firstname, lastname);
  }

  // Highlight part of the 'text' present in the 'searchInput'
  public getHighlight(text: string) {
    if (this.searchInput && text) {
      // Get started index of the searchInput in the text
      const startIndex = this.getNoAccentNoCaseWord(text).indexOf(this.getNoAccentNoCaseWord(this.searchInput));
      if (startIndex !== -1) {
        // Set the found text style to bold
        const endLength = this.searchInput.length;
        const matchingString = text.substr(startIndex, endLength);
        return text.replace(matchingString, '<b>' + matchingString + '</b>');
      }
    }
    return text;
  }

  public close(reset = false) {
    this.closeEvent.emit();
    this.isDisplayed = false;
    setTimeout(() => {
      this.dialogRef.close();
      if (reset) {
        // Empty the lists
        this._usersTagToAdd = [];
        this._analysisUsers = [];
        this.userGlobalService.users.forEach((analysisUser: User) => {
          // Add all users except curent user
          if (analysisUser.guid !== this.userGlobalService.user.guid) {
            this.addUser(analysisUser);
          }
        });
      }
    }, 500);
  }

  public open(el: HTMLElement, user: User) {
    if (!this.isSpecialRole(user)) {
      if (el.classList.contains('opened')) {
        // Remove the class to close it
        el.classList.remove('opened');
      } else {
        // Add the class to open it
        el.classList.add('opened');
      }
    }
  }

  public openTooltip(i: number, role: Role, j: number, button: HTMLElement, buttonWrapper: HTMLElement) {
    this.tootltip.text = role.rol_description;
    this.tootltip.id = i;
    const tooltip = buttonWrapper.querySelector('.tooltip');
    if (tooltip) {
      if (button.classList.contains('opened')) {
        let leftOffset = 0;
        const roles = button.querySelectorAll('.role');
        // Get all role before the selected role to set the offset
        for (let _i = 0; _i < j; _i++) {
          const rolWidth = roles[_i].clientWidth;
          // 8 for the margin
          leftOffset += rolWidth + MARGIN_ROLE;
        }
        leftOffset += (roles[j].clientWidth / 2);
        leftOffset += 2 * MARGIN_ROLE;
        tooltip['style'].left = leftOffset + 'px';
      } else {
        tooltip['style'].left = '50%';
      }
    }
  }

  public closeTooltip() {
    this.tootltip.id = null;
  }

  public tooltipIsDisplayed(i) {
    return this.tootltip.id === i;
  }

  public tooltipIsTop(i) {
    return this.getDisplayedUsers().length !== 1 && i === (this.getDisplayedUsers().length - 1);
  }

  public getTooltipDirection(i) {
    return this.tooltipIsTop(i) ? 'top' : 'bottom';
  }

  public sortUsers(users: Array<User>) {
    users.sort((user1, user2) => {
      // Role before no Role and Special Role before other role
      if (user1.role && !user2.role) {
        return -1;
      } else if (!user1.role && user2.role) {
        return 1;
      } else if (user1.role && user2.role) {
        return this.sortRole(user1, user2);
      } else {
        return this.sortByEmail(user1, user2);
      }
    });
  }

  // Normalize text without accent and capital letters
  private getNoAccentNoCaseWord(str: String) {
    return unorm.nfd(str.toLowerCase()).replace(/[\u0300-\u036f]/g, '');
  }

  private sortRole(user1: User, user2: User) {
    // Specials roles before others
    if (this.isSpecialRole(user1) && !this.isSpecialRole(user2)) {
      return -1;
    } else if (!this.isSpecialRole(user1) && this.isSpecialRole(user2)) {
      return 1;
    } else if (this.isSpecialRole(user1) && this.isSpecialRole(user2)) {
      return this.sortSpecialRole(user1, user2);
    } else {
      return this.sortByEmail(user1, user2);
    }
  }

  private sortSpecialRole(user1: User, user2: User) {
    // Owner before all
    if (user1.role.rol_code.toLowerCase() === 'owner' && user2.role.rol_code.toLowerCase() !== 'owner') {
      return -1;
    } else if (user2.role.rol_code.toLowerCase() === 'owner' && user1.role.rol_code.toLowerCase() !== 'owner') {
      return 1;
    } else {
      return this.sortByEmail(user1, user2);
    }
  }

  private sortByEmail(user1: User, user2: User) {
    return (user1.email > user2.email) ? 1 : -1;
  }
}
