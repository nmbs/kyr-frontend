import { MeasureState } from './measure/measureState';

export interface Choice {
    cho_id: string;
    cho_name: string;
    cho_subtitle: string;
    cho_icon: string;
    cho_help: string;
    checked: boolean;
    isDatePicker?: boolean;
    date?: Date;
    status?: MeasureState;
}
