import { Question } from './question.model';

export interface Step {
    id?: string;
    title: string | Function;
    maxCharacterTitle?: number;
    inputTitle: boolean;
    isTitleFocused: boolean;
    placeHolderTitle?: string;
    subtitle?: string;
    route?: string;
    errorText?: string;
    isErrorDisplayed: boolean;
    apiPath?: Function;
    deleteButton: boolean;
    isActive: boolean;
    paging?: Function;
    questions: Array<Question>;
}
