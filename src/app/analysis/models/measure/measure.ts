import { MeasureCategory } from './measureCategory';

export interface Measure {
    mea_id: string;
    mea_name: string;
    mea_description: string;
    mea_isCustom: boolean;
    measureCategory: MeasureCategory;
}
