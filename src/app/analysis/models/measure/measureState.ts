export interface MeasureState {
    mst_id: string;
    mst_name: string;
    mst_code: string;
}
