import { MeasureState } from './measureState';
import { Measure } from './measure';

export interface AnalysisMeasure {
    ram_id: string;
    ral_id: string;
    fet_id: string;
    ram_isDisponibility: boolean;
    ram_isIntegrity: boolean;
    ram_isConfidenciality: boolean;
    ram_isProof: boolean;
    ram_cost: number;
    ram_dt: string;
    measureState: MeasureState;
    measure: Measure;
    isChecked?: boolean;
}
