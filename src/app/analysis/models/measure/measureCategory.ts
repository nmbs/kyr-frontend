export interface MeasureCategory {
    mca_id: string;
    mca_name: string;
    mca_logo: string;
}
