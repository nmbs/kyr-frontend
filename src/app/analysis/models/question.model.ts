import { QuestionType } from './questionType.model';
import { Slider } from './slider.model';

export interface Question {
    title?: Function | string;
    recapTitle?: string | Function;
    subTitle?: Function;
    toggleLabel?: Function | string;
    apiPath?: string;
    type: QuestionType;
    // Loading CSS element in form of component placeholders
    nbPlaceholder: number;
    // Property to populate in DB
    propertyName: string;
    placeholder?: string;
    sliders?: Array<Slider>;
    value?: string;
    isPatch: boolean;
    isOptional: boolean;
    countProgress?: boolean;
    questions?: Array<Question>;
}
