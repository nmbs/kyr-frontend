export interface Slider {
    title: Function;
    subtitle: Function;
    valueToChange: string;
    startingValue: Number;
}

export const ImpactDegree = [
    'Impact faible',
    'Impact moyen',
    'Impact fort',
    'Impact critique'
];
