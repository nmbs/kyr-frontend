import { ImpactDetails } from "./impactDetails.model";

export interface Impact {
    imp: ImpactDetails;
    ira_d: Number;
    ira_i: Number;
    ira_c: Number;
    ira_p: Number;
    ira_comment: string;
}
