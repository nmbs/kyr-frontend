import { Step } from './step.model';

export interface Description {
    title: string | Function;
    isModify: boolean;
    iconPath: string;
    steps: Array<Step>;
}
