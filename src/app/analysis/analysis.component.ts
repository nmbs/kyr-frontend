import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { UserGlobalService } from '../services/userGlobal.service';
import { UserService } from '../services/user.service';
import { AnalysisGlobalService } from './analysisGlobal.service';

@Component({
  selector: 'app-analysis',
  templateUrl: './analysis.component.html',
  styleUrls: ['./analysis.component.scss'],
  providers: [],
  encapsulation: ViewEncapsulation.None
})
export class AnalysisComponent implements OnInit {

  private _analysis_id: string;
  public show = true;

  constructor(private readonly route: ActivatedRoute,
    private readonly router: Router,
    private userGlobalService: UserGlobalService,
    private userService: UserService,
    public analysisGlobalService: AnalysisGlobalService) { }

  ngOnInit() {
    this.analysisGlobalService.hideSidebar.subscribe(res => {
      this.show = res;
    });
    this.route.paramMap.subscribe(params => {
      this._analysis_id = params.get('guid');
      this.userService.getUsersByAnalyseId(this._analysis_id).subscribe(res => {
        this.userGlobalService.users = res;
      });
    });
  }
}
