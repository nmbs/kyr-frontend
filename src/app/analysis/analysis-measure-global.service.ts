import { Injectable } from '@angular/core';
import { ReplaySubject, BehaviorSubject } from 'rxjs';
import { AnalysisMeasure } from './models/measure/analysisMeasure';

@Injectable()
export class AnalysisMeasureGlobalService {

  public analysisMeasureListSubject = new ReplaySubject<Array<AnalysisMeasure>>();

  private _analysisMeasureList: Array<AnalysisMeasure>;

  constructor() {
  }

  public set analysisMeasureList(analysisMeasureList: Array<AnalysisMeasure>) {
    this._analysisMeasureList = analysisMeasureList;
    this.analysisMeasureListSubject.next(analysisMeasureList);
  }

  public get analysisMeasureList(): Array<AnalysisMeasure> {
    return this._analysisMeasureList;
  }
}
