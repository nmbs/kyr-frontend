import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalysisCreateComponent } from './analysis-create.component';
import {AnalysisService} from '../analysis.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppConfig } from '../../app.config';
import { AnalysisGlobalService } from '../../analysis/analysisGlobal.service';

describe('AnalysisCreateComponent', () => {
  let component: AnalysisCreateComponent;
  let fixture: ComponentFixture<AnalysisCreateComponent>;

  beforeEach(async(() => {
    const appConfigSpy = jasmine.createSpyObj('AppConfig', {
      getConfig: function () {
        return '';
      }
    });
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [ AnalysisCreateComponent ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [AnalysisService, AnalysisGlobalService, { provide: AppConfig, useValue: appConfigSpy }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalysisCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
