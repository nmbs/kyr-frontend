import { Component, OnInit } from '@angular/core';
import { AnalysisService } from '../analysis.service';
import { Analysis } from '../api-models/analysis.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-analysis-create',
  templateUrl: './analysis-create.component.html',
  styleUrls: ['./analysis-create.component.scss'],
  providers: [AnalysisService]
})
export class AnalysisCreateComponent implements OnInit {
  public analysis: any = {
    ral_name: null
  };
  private _analysisPath = '/analyses/';
  private _analysisDescriptionPath = '/description';

  constructor(private readonly router: Router, private analysisService: AnalysisService) { }

  ngOnInit() {
  }

  // Create the analysis and redirect to the description page in modification mode
  public create() {
    this.analysisService.createAnalysis(this.analysis).subscribe(res => {
      this.analysis = res;
      this.router.navigate([this._analysisPath + this.analysis.ral_id + this._analysisDescriptionPath, { isModify: true }]);
    });
  }

}
