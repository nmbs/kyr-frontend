import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AnalysisService } from '../analysis.service';
import { Analysis } from '../api-models/analysis.model';
import { analysisDescription } from '../../../assets/analysis-description-form/analysis';
import { Step } from '../models/step.model';
import { DescriptionGlobalService } from '../descriptionGlobal.service';
import { MatDialog } from '@angular/material';
import { OnboardingImpactComponent } from '../modals/onboarding-impact/onboarding-impact.component';
import { UserGlobalService } from 'src/app/services/userGlobal.service';
import html2canvas from '../../../assets/html2canvas/html2canvas.js';
import * as jsPDF from 'jspdf/dist/jspdf.debug.js';
import { AnalysisGlobalService } from '../analysisGlobal.service';
import Promise from 'promise-polyfill';


@Component({
  selector: 'app-analysis-description',
  templateUrl: './analysis-description.component.html',
  styleUrls: ['./analysis-description.component.scss'],
  providers: [AnalysisService]
})
export class AnalysisDescriptionComponent implements OnInit {
  private _analysis_id: string;
  public description = analysisDescription;
  public analysis: Analysis;
  private _analysisPath = '/analyses/';
  public placeholder = true;
  public hasError = false;
  public onBoardingImpact: string;
  public exportingPDF = false;

  constructor(private readonly route: ActivatedRoute,
    private readonly router: Router,
    private analysisService: AnalysisService,
    private descriptionGlobalService: DescriptionGlobalService,
    public dialog: MatDialog,
    private userGlobalService: UserGlobalService,
    public analysisGlobalService: AnalysisGlobalService) {
    this.descriptionGlobalService.tellStory = analysisDescription;
  }

  ngOnInit() {
    // Get GUID from the route params and get the analysis
    this.route.parent.paramMap.subscribe(params => {
      this._analysis_id = params.get('guid');
      this.analysisService.getAnalysis(this._analysis_id).subscribe(res => {
        this.analysis = res;

        this.placeholder = false;
        // Get isModify param from the route params
        this.route.paramMap.subscribe(parameters => {
          const isModifyParamm = parameters.get('isModify');
          const indexParam = +parameters.get('index');
          if (isModifyParamm) {
            this.description.isModify = isModifyParamm === 'true' && !this.analysis.ral_isBlocked;
            this.scrollToTop();
          }
          if (indexParam !== null && this.description.isModify) {
            this.modify(this.description.isModify, indexParam, this.description.steps[indexParam]);
          }
        });
      },
        error => {
          this.hasError = true;
        });
    });

    this.descriptionGlobalService.stepIndexSubject.subscribe((stepIndex: number) => {
      this.scrollToForm(stepIndex);
    });

    if (this.userGlobalService.user) {
      this.onBoardingImpact = this.userGlobalService.user.email + '-onboarding-impact';
    }
  }

  // Go to recap page by setting isModify to false
  public goToRecap() {
    this.description.isModify = false;
  }

  // Go to impact page in modification mode
  public goToImpactModification() {
    this.descriptionGlobalService.stepIndex = 0;
    this.router.navigate([this._analysisPath + this.analysis.ral_id + '/impact', { isModify: true }]);
    if (!localStorage.getItem(this.onBoardingImpact) ||
      localStorage.getItem(this.onBoardingImpact) === 'false') {
      this.openDialog();
    }
  }

  public openDialog(): void {
    if (!this.analysis.ral_isBlocked) {
      this.scrollToTop();
      this.dialog.open(OnboardingImpactComponent);
    }
  }

  // Scroll to the previous block
  public prev(index: number) {
    this.scrollToForm(index - 1);
  }

  // Scroll to the next block
  public next(index: number) {
    this.scrollToForm(index + 1);
  }

  public setActive(index: number, isActive: boolean) {
    this.descriptionGlobalService.tellStory.steps[index].isActive = isActive;
  }

  // Switch in modify mode and scroll to top
  public modify(isModify: boolean, index: number, step: Step) {
    this.description.isModify = isModify;
    const currentUrl = this.router.url.split(';');
    this.router.navigate([currentUrl[0], { isModify: true }]);
    if (!isModify || (isModify && index === 0)) {
      this.scrollToTop();

      if (index === 0 && isModify) {
        step.isActive = true;
        this.descriptionGlobalService.tellStory.steps.forEach((s: Step, i: number) => {
          if (i === index) {
            s.isActive = true;
          } else {
            s.isActive = false;
          }
        });
        this.descriptionGlobalService.tellStory.steps[0].isActive = true;
      }
    } else {
      setTimeout(() => {
        this.scrollToForm(index);
      }, 100);
    }
    this.descriptionGlobalService.tellStory.isModify = this.description.isModify;
  }

  private scrollToForm(index) {
    const element_to_scroll_to = document.getElementById('form' + index);
    if (element_to_scroll_to) {
      element_to_scroll_to.scrollIntoView({ behavior: 'smooth' });
    }
  }

  private scrollToTop() {
    window.scrollTo(0, 0);
    window.scroll(0, 0);
    scroll(0, 0);
  }
  // Returns an empty Array to iterate through
  public counter(i: number) {
    return new Array(i);
  }


  public exportPDF() {
    // Making sure exported data are updated
    this.analysisService.getAnalysis(this._analysis_id).subscribe(res => {
      this.analysis = res;


      // exportingPDF display the html that will be selected and imported into the PDF document
      // Even when true, the HTML is invisible because displayed out of the page in position absolute
      // and in top and left position greater than the size of the page
      if (!this.exportingPDF) {
        this.exportingPDF = true;

        // Adding promise and html2canvas to window in order for the html2canvas and jsPDF libs to work
        (<any>window).html2canvas = html2canvas;
        (<any>window).promise = Promise;

        // Creating the base blank PDF
        const doc = new jsPDF({
          unit: 'pt',
          format: [595.28, 841.89],
          orientation: 'portrait'
        });

        // Defining the height of an A4 page (841.89 * 2)
        const a4HeightPixels = 1684;

        // Set timeout is nescessary in order for the PDF obj to be created before the generation begins
        setTimeout(() => {
          // Below is the calculus of the page breaks
          // 3 cases are possible: the block fit in the page
          // Init the base height to 60 to compensate the base margin-top that will be applied at the beginning of the first page
          let height = 60;

          // Selecting the html to export
          const elementToExport = document.getElementById('pdfContainer').children;

          // Creating an empty HTML with pdfContainer id to then fill it with each html element of the selected full html
          // and integrate page breaks when the height of an A4 page is reached
          const htmlToConvert: HTMLElement = <HTMLElement>document.getElementById('pdfContainer').cloneNode(false);

          Array.from(elementToExport).forEach(node => {
            // Adding the height of the current element to the global height
            height += node.clientHeight;

            if (height < a4HeightPixels) {
              // Adding element to the html to export, because the total height is lesser than the A4 page height
              htmlToConvert.appendChild(<HTMLElement>node.cloneNode(true));
            } else {
              // The element makes the total height be greater than the A4 page height
              // We parse the child 'questionContainer' elements to treat them separatly
              // This is specific to the 'description' page, because the parent HTML element usually have a big height
              const questionElements = node.querySelectorAll('app-white-container > .container > .questionsContainer > app-question > .questionContainer');

              // Removing the parent element height from the total height, we are now processing its children elements
              height -= node.clientHeight;

              // If the first child of the parent element makes the height go over the A4 height, we do a page break
              if (Array.from(questionElements)[0].clientHeight + height > a4HeightPixels) {
                // Creating an empty div element
                const pageBreak = document.createElement('DIV');
                // Calculating the margin nescessary to begin at the next A4 page and adding 60px of top margin to it
                const margin = ((a4HeightPixels - height) + 60).toString() + 'px';
                // Applying the page break
                pageBreak.setAttribute(
                  'style', 'margin-bottom: ' + margin + ';');
                // Adding the page break to the HTML
                htmlToConvert.appendChild(pageBreak);
                // Updating the global height
                height = node.clientHeight + 60;

              } else {
                let hasBroken = false;
                // If the first element does not break the page, we iterate throught all children to find the one who will
                // Adding 12px that correspond to the top paddign of the parent element (.form)
                height += 12;
                Array.from(questionElements).forEach(element => {
                  // The real element height comprises 20px of margin that need to be considerated in the global height
                  const elementHeight = element.clientHeight + 20;
                  // Updating the global height
                  height += elementHeight;
                  if (height > a4HeightPixels) {
                    height = height - (elementHeight);
                    // calculating the margin +20px = margin bottom of the question
                    const margin = ((a4HeightPixels - height) + 20 + 60).toString() + 'px';
                    height = elementHeight - 20 + 60;
                    // Applying the page break with a margin top
                    const pageBreak = document.createElement('DIV');
                    pageBreak.setAttribute('style', 'margin-top: ' + margin + ';');
                    element.prepend(pageBreak);
                    hasBroken = true;
                  }
                });
                // Adding the margin of 20px from the last child element, and the padding of 12px from the parent element
                height += 32;
                // If no page break have happend, this means that the sum of the questions heights are smaller than the parent height
                // this can happen if a parent element has only one question, a its title is long enought that it goes over multiple lines
                // if so, we do a standard page break of the size of the parent element
                if (!hasBroken) {
                  // removing the previous margins, they are comprised in the parent element height
                  height -= 44;
                  height = this.standardAddMargin(height, node, 'margin-bottom', 0, a4HeightPixels, htmlToConvert, false);
                }
              }
              // Adding the element to the final HTML
              htmlToConvert.appendChild(<HTMLElement>node.cloneNode(true));
            }
          }
          );
          // Setting a series of CSS attributes to the global HTML to export
          htmlToConvert.setAttribute(
            'style', 'position:relative; right:0; top:0; bottom:0; font-family:"Helvetica"; margin: 60px 40px 60px 30px; margin-bottom: 100px;text-align: left;');

          // use of the JSPDF lib to export the pdf
          doc.html(htmlToConvert, {
            // The scale, width and height are important in order to have the same render on any screen the export is made
            html2canvas: {
              scale: 0.5,
              width: '100%',
              height: '1684px'
            },
            callback: (pdf: jsPDF) => {
              // Formatting generation date and time
              const date = new Date();
              const dd = String(date.getDate()).padStart(2, '0');
              const mm = String(date.getMonth() + 1).padStart(2, '0');
              const hh = String(date.getHours()).padStart(2, '0') + 'h' + String(date.getMinutes()).padStart(2, '0');
              const yyyy = date.getFullYear();

              // Replacing spaces with dash in the analysis name
              const generationTime = dd + '-' + mm + '-' + yyyy + '-' + hh;
              const formatedAnalysisName = this.analysis.ral_name.split(' ').join('-');

              // Building the PDF name and extension
              const titlePDF = 'Je-Raconte-Mon-Histoire' + '_' + formatedAnalysisName + '_' + generationTime + '.pdf';
              doc.save(titlePDF);
              this.exportingPDF = false;
            },
          });
        }, 1000);


      }
    },
      error => {
        this.hasError = true;
      });
  }

  private standardAddMargin(height, element, marginType, marginToAdd, a4HeightPixels, htmlToConvert, isBefore) {
    // Removing the parent element height from the total height, we are now processing its children elements
    height -= element.clientHeight;
    // Calculating the margin nescessary to begin at the next A4 page and adding 60px of top margin to it
    const margin = ((a4HeightPixels - height) + marginToAdd).toString() + 'px';
    const pageBreak = document.createElement('DIV');
    // Applying the page break
    pageBreak.setAttribute(
      'style', marginType + ': ' + margin + ';');
    // Adding the page break to the HTML
    if (!isBefore) {
      htmlToConvert.appendChild(pageBreak);
    } else {
      htmlToConvert.prepend(pageBreak);
    }
    // Updating the global height
    height = element.clientHeight;
    return height;
  }
}
