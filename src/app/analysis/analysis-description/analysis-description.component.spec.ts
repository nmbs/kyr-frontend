import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalysisDescriptionComponent } from './analysis-description.component';
import {AnalysisService} from '../analysis.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfig } from '../../app.config';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { AnalysisGlobalService } from '../../analysis/analysisGlobal.service';
import { DescriptionGlobalService } from '../descriptionGlobal.service';
import { of } from 'rxjs';
import { MatDialog } from '@angular/material';
import { UserGlobalService } from 'src/app/services/userGlobal.service';

describe('AnalysisDescriptionComponent', () => {
  let component: AnalysisDescriptionComponent;
  let fixture: ComponentFixture<AnalysisDescriptionComponent>;

  const matDialogSpy = {};

  const userGlobalServiceMock = {
    user: {
      email: 'emial@email.com'
    }
  };

  beforeEach(async(() => {
    const appConfigSpy = jasmine.createSpyObj('AppConfig', {
      getConfig: function () {
        return '';
      }
    });

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [ AnalysisDescriptionComponent ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [{ provide: UserGlobalService, useValue: userGlobalServiceMock},
        AnalysisService,
        AnalysisGlobalService,
        DescriptionGlobalService,
        { provide: AppConfig, useValue: appConfigSpy },
        { provide: MatDialog, useValue: matDialogSpy },
        { provide: ActivatedRoute, useValue: { parent : { paramMap: of()}, paramMap: of()}}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalysisDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
