import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { AppConfig } from 'src/app/app.config';
import { HttpHelper } from 'src/app/helpers/http.helper';
import { Measure } from '../../models/measure/measure';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AddMeasuresService {
  private _api: string;
  private _urlMeasure: string;

  constructor(private _http: HttpClient, private appConfig: AppConfig) {
    this._api = appConfig.getConfig().apiAnalysisUrl;
    this._urlMeasure = this._api + '/measures';
  }

  // Get all measures by Analysis GUID
  public getMeasures(): Observable<Array<Measure>> {
    return this._http.get<Array<Measure>>(this._urlMeasure, { headers: HttpHelper.getHeaders() })
    .pipe(
      map(res => {
        return res;
      }),
      catchError(HttpHelper.handleError));
  }
}
