import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AddMeasuresComponent } from './add-measures.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { AppConfig } from 'src/app/app.config';
import { Analysis } from '../../api-models/analysis.model';
import { AnalysisGlobalService } from '../../analysisGlobal.service';
import { MatDialogRef } from '@angular/material';

const mockAnalysis: Analysis = {
  ral_id: 'string',
  ral_name: 'string',
  ral_created_date: null,
  ral_updated_date: null,
  finalUsers: 'string',
  nbUser: 'string',
  dataSensibility: 'string',
  dataTypes: 'string',
  userAccesses: 'string',
  availabilityWeeks: 'string',
  availabilityDays: 'string',
  traceKeepingLaw: 'string',
  traceKeepingApp: 'string',
  solutionTypes: 'string',
  maxInterruptionTime: 'string',
  impactDataLoss: 'string',
  ral_isBlocked: false,
  features: [
    {
      fet_id: 'string',
      ral_id: 'string',
      fet_label: 'string',
      fet_description: 'string',
      fet_sensitive_nature: 'string',
      fet_created_date: 'string'
    }],
  impacts: null,
  opinion: null,
  decision: null,
  residualRisk: null
};

const mockMeasuresAnalysis = [
  {
    ram_id: 'string',
    ral_id: 'string',
    fet_id: 'myguid',
    ram_isDisponibility: true,
    ram_isIntegrity: true,
    ram_isConfidenciality: true,
    ram_isProof: true,
    ram_cost: 5,
    ram_dt: 'string',
    ral_isBlocked: false,
    measureState: {
      mst_id: 'string',
      mst_name: 'string',
      mst_code: 'string',
    },
    measure: {
      mea_id: 'string',
      mea_name: 'string',
      mea_description: 'string',
      mea_isCustom: false,
      measureCategory: {
        mca_id: 'string',
        mca_name: 'string',
        mca_logo: 'string',
      }
    },
    isChecked: true
  }
];

const matDialogRefSpy = {};

class MockAnalysisGlobalService {
  analysis: Analysis = mockAnalysis;
}
describe('AddMeasuresComponent', () => {
  let component: AddMeasuresComponent;
  let fixture: ComponentFixture<AddMeasuresComponent>;

  beforeEach(async(() => {
    const appConfigSpy = jasmine.createSpyObj('AppConfig', {
      getConfig: function () {
        return '';
      }
    });
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [AddMeasuresComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: AnalysisGlobalService, useValue: new MockAnalysisGlobalService() },
        { provide: AppConfig, useValue: appConfigSpy },
        { provide: MatDialogRef, useValue: matDialogRefSpy }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMeasuresComponent);
    component = fixture.componentInstance;
    component.cannotBeAdded = mockMeasuresAnalysis;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
