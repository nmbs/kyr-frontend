import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { AddMeasuresService } from './add-measures.service';
import * as _ from 'lodash';
import { SearchListItem } from 'src/app/graphic-components/list-search/SearchListItem';
import { AnalysisMeasure } from '../../models/measure/analysisMeasure';
import { AnalysisGlobalService } from '../../analysisGlobal.service';
import { Measure } from '../../models/measure/measure';
import { MatDialogRef } from '@angular/material';


@Component({
  selector: 'app-add-measures',
  templateUrl: './add-measures.component.html',
  styleUrls: ['./add-measures.component.scss'],
  providers: [AddMeasuresService]
})
export class AddMeasuresComponent implements OnInit {

  @Output() closeEvent = new EventEmitter<boolean>();

  public featureName: string;
  public parentId: string;
  public cannotBeAdded: Array<AnalysisMeasure>;
  public modifyMode = false;

  public isDisplayed = true;
  public measuresList: Array<SearchListItem> = [];
  public analysisMeasuresToAdd: Array<AnalysisMeasure> = [];
  public analysisId: string;
  public step = 1;
  constructor(private addMeasuresService: AddMeasuresService,
    private analysisGlobalService: AnalysisGlobalService,
    private dialogRef: MatDialogRef<AddMeasuresComponent>) { }

  ngOnInit() {
    this.analysisId = this.analysisGlobalService.analysis.ral_id;
    // Getting all measures
    this.addMeasuresService.getMeasures().subscribe(res => {
      // Formatting the measures in a pivot format for the search list component
      res.forEach(item => {

        const t: SearchListItem = {
          str1: item.mea_name,
          subStr: _.truncate(item.mea_description, { 'length': 80, 'separator': ' ' }),
          guid: item.mea_id,
          obj: item
        };
        this.measuresList.push(t);

      });
      this.measuresList = _.sortBy(this.measuresList, m => m.str1);
    });
  }

  // Populates the 'measuresToAdd' List with newly created AnalysisMeasure typed object...
  // ... with the received list of measures
  public addMeasures(measuresToAdd: Array<Measure>) {
    measuresToAdd.forEach(measure => {
      const analysisMeasureToAdd: AnalysisMeasure = {
        fet_id: this.parentId,
        ral_id: this.analysisId,
        ram_id: null,
        ram_isDisponibility: false,
        ram_isIntegrity: false,
        ram_isConfidenciality: false,
        ram_isProof: false,
        ram_cost: 0,
        ram_dt: null,
        measureState: null,
        measure: measure
      };
      this.analysisMeasuresToAdd.push(analysisMeasureToAdd);
    });
    this.step = 2;
  }
  // Populates the 'measuresToAdd' List with newly created AnalysisMeasure typed object...
  // ... with the received list of custom labels
  public addCustomMeasures(customMeasuresToAdd: Array<string>) {
    customMeasuresToAdd.forEach(customMeasureStr => {
      const analysisMeasureToAdd: AnalysisMeasure = {
        fet_id: this.parentId,
        ral_id: this.analysisId,
        ram_id: null,
        ram_isDisponibility: false,
        ram_isIntegrity: false,
        ram_isConfidenciality: false,
        ram_isProof: false,
        ram_cost: 0,
        ram_dt: null,
        measureState: null,
        measure: {
          mea_id: null,
          mea_name: customMeasureStr,
          mea_description: null,
          mea_isCustom: true,
          measureCategory: null
        }
      };
      this.analysisMeasuresToAdd.push(analysisMeasureToAdd);
    });
  }

  // Filter the list of measures by removing the ones that are in the existing features
  public filterList() {
    let res;

    const listMeasuresId = this.cannotBeAdded.map(measureAnalysis => {
      return measureAnalysis.measure.mea_id;
    });

    if (this.measuresList) {
      res = this.measuresList.filter(x => {
        return listMeasuresId.indexOf(x.guid) < 0 && !x.obj.mea_isCustom;
      });
    }

    return res;
  }

  // Builds the title of the modal with the feature name
  public getTitle() {
    let res = '';
    if (this.featureName) {
      res = 'Ajoutez des mesures à ' + '<b>' + this.featureName + '</b>';
    }
    return res;
  }

  // Build the placeholder text with the measurelist size for the component's input
  public getPlaceholderText() {
    let res = '';
    if (this.measuresList && this.measuresList.length > 0) {
      res = 'Rechercher parmi ' + this.measuresList.length + ' mesures...';
    }
    return res;
  }

  // Closes the modal
  public close() {
    this.closeEvent.emit();
    this.isDisplayed = false;
    setTimeout(() => {
      this.dialogRef.close();
    }, 500);
  }
}
