import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AppConfig } from 'src/app/app.config';
import { MatDialogRef } from '@angular/material';
import { UserGlobalService } from 'src/app/services/userGlobal.service';
import { AnalysisGlobalService } from '../../analysisGlobal.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-onboarding-risks',
  templateUrl: './onboarding-risks.component.html',
  styleUrls: ['./onboarding-risks.component.scss']
})
export class OnboardingRisksComponent implements OnInit {

  private anim: any;
  public lottieOnboardingRisks: Object;
  isDisplayed = true;
  isChecked = false;
  _users: Array<User>;
  isOwner: boolean;

  @Output() closeEvent = new EventEmitter<boolean>();

  constructor(public dialogRef: MatDialogRef<OnboardingRisksComponent>,
    private appConfig: AppConfig,
    private userGlobalService: UserGlobalService) { }

  ngOnInit() {
    this.setIsOwner();
    this.lottieOnboardingRisks = {
      animationData: this.appConfig.getConfig().animationAssets['onboarding-risks'],
      renderer: 'svg',
      autoplay: true,
      loop: false
    };
  }

  public handleAnimation(anim: any) {
    this.anim = anim;
    this.anim.setSpeed(1);
  }

  public close() {
    this.closeEvent.emit();
    this.isDisplayed = false;
    setTimeout(() => {
      this.dialogRef.close();
    }, 500);
  }

  public check() {
    this.isChecked = !this.isChecked;
    if (this.isOwner) {
      localStorage.setItem(this.userGlobalService.user.email + '-onboarding-risks-owner', JSON.stringify(this.isChecked));
    } else {
      localStorage.setItem(this.userGlobalService.user.email + '-onboarding-risks', JSON.stringify(this.isChecked));
    }

  }

  // Check if the user is an owner and subscribe to users global
  private setIsOwner() {
    if (this.userGlobalService.users) {
      this._users = this.userGlobalService.users;
      const owner = this._users.find(usr => usr.role && usr.role.rol_code.toLowerCase() === 'owner');
      if (owner) {
        this.isOwner = owner.guid === this.userGlobalService.user.guid;
      }
    }
    this.userGlobalService.usersSubject.subscribe(users => {
      this._users = users;
      const owner = users.find(usr => usr.role && usr.role.rol_code.toLowerCase() === 'owner');
      if (owner) {
        this.isOwner = owner.guid === this.userGlobalService.user.guid;
      }
    });
  }

}
