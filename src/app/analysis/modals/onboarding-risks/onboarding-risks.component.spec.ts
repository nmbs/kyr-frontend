import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnboardingRisksComponent } from './onboarding-risks.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { UserGlobalService } from 'src/app/services/userGlobal.service';
import { AppConfig } from 'src/app/app.config';
import { MatDialogRef } from '@angular/material';
import { ReplaySubject, of } from 'rxjs';
import { User } from 'src/app/models/user';

const appConfigSpy = {
  getConfig: () => {
    return { animationAssets: {} };
  }
};
const matDialogRefSpy = {};

const usersSubject = new ReplaySubject<Array<User>>();

const userGlobalServiceMock = {
  user: {
    email: 'emial@email.com',
    guid: '1'
  },
  users: [
    {
      email: 'emial@email.com',
      guid: '1',
      firstName: 'Romain'
    },
    {
      email: 'email@email.com',
      guid: '2'
    }
  ],
  usersSubject: usersSubject
};

describe('OnboardingRisksComponent', () => {
  let component: OnboardingRisksComponent;
  let fixture: ComponentFixture<OnboardingRisksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnboardingRisksComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [{ provide: UserGlobalService, useValue: userGlobalServiceMock },
      { provide: AppConfig, useValue: appConfigSpy },
      { provide: MatDialogRef, useValue: matDialogRefSpy }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnboardingRisksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
