import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AppConfig } from 'src/app/app.config';
import { MatDialogRef } from '@angular/material';
import { UserGlobalService } from 'src/app/services/userGlobal.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-onboarding-measures',
  templateUrl: './onboarding-measures.component.html',
  styleUrls: ['./onboarding-measures.component.scss']
})
export class OnboardingMeasuresComponent implements OnInit {

  private anim: any;
  public lottieOnboardingMeasures: Object;
  isDisplayed = true;
  isChecked = false;
  _users: Array<User>;
  isOwner: boolean;

  @Output() closeEvent = new EventEmitter<boolean>();

  constructor(public dialogRef: MatDialogRef<OnboardingMeasuresComponent>,
    private appConfig: AppConfig,
    private userGlobalService: UserGlobalService) { }

  ngOnInit() {
    this.setIsOwner();
    this.lottieOnboardingMeasures = {
      animationData: this.appConfig.getConfig().animationAssets['onboarding-measures'],
      renderer: 'svg',
      autoplay: true,
      loop: false
    };
  }

  public handleAnimation(anim: any) {
    this.anim = anim;
    this.anim.setSpeed(1);
  }

  public close(choice: boolean) {
    this.closeEvent.emit();
    this.isDisplayed = false;
    setTimeout(() => {
      this.dialogRef.close(choice);
    }, 500);
  }

  public check() {
    this.isChecked = !this.isChecked;
    if (this.isOwner) {
      localStorage.setItem(this.userGlobalService.user.email + '-onboarding-measures-owner', JSON.stringify(this.isChecked));
    } else {
      localStorage.setItem(this.userGlobalService.user.email + '-onboarding-measures', JSON.stringify(this.isChecked));
    }

  }

  // Check if the user is an owner and subscribe to users global
  private setIsOwner() {
    if (this.userGlobalService.users) {
      this._users = this.userGlobalService.users;
      const owner = this._users.find(usr => usr.role && usr.role.rol_code.toLowerCase() === 'owner');
      if (owner) {
        this.isOwner = owner.guid === this.userGlobalService.user.guid;
      }
    }
    this.userGlobalService.usersSubject.subscribe(users => {
      this._users = users;
      const owner = users.find(usr => usr.role && usr.role.rol_code.toLowerCase() === 'owner');
      if (owner) {
        this.isOwner = owner.guid === this.userGlobalService.user.guid;
      }
    });
  }

}
