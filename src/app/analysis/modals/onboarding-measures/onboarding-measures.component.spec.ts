import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnboardingMeasuresComponent } from './onboarding-measures.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { UserGlobalService } from 'src/app/services/userGlobal.service';
import { AppConfig } from 'src/app/app.config';
import { MatDialogRef } from '@angular/material';
import { ReplaySubject } from 'rxjs';
import { User } from 'src/app/models/user';

const appConfigSpy = {
  getConfig: () => {
    return { animationAssets: {} };
  }
};
const matDialogRefSpy = {};

const usersSubject = new ReplaySubject<Array<User>>();

const userGlobalServiceMock = {
  user: {
    email: 'emial@email.com',
    guid: '1'
  },
  users: [
    {
      email: 'emial@email.com',
      guid: '1',
      firstName: 'Romain'
    },
    {
      email: 'email@email.com',
      guid: '2'
    }
  ],
  usersSubject: usersSubject
};

describe('OnboardingMeasuresComponent', () => {
  let component: OnboardingMeasuresComponent;
  let fixture: ComponentFixture<OnboardingMeasuresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnboardingMeasuresComponent ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [{ provide: UserGlobalService, useValue: userGlobalServiceMock },
      { provide: AppConfig, useValue: appConfigSpy },
      { provide: MatDialogRef, useValue: matDialogRefSpy }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnboardingMeasuresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
