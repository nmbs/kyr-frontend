import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { AppConfig } from 'src/app/app.config';
import { Router, ActivatedRoute } from '@angular/router';
import { AnalysisGlobalService } from '../../analysisGlobal.service';
import { UserGlobalService } from 'src/app/services/userGlobal.service';

@Component({
  selector: 'app-onboarding-feature',
  templateUrl: './onboarding-feature.component.html',
  styleUrls: ['./onboarding-feature.component.scss']
})
export class OnboardingFeatureComponent implements OnInit {

  isDisplayed = true;
  isChecked = false;

  @Output() closeEvent = new EventEmitter<boolean>();

  public lottieOnboardingFeature: Object;

  private anim: any;
  private pathFeaturesDicp: string;

  constructor(public dialogRef: MatDialogRef<OnboardingFeatureComponent>,
    private appConfig: AppConfig,
    private readonly router: Router,
    private analysisGlobalService: AnalysisGlobalService,
    private userGlobalService: UserGlobalService) { }

  ngOnInit() {
    this.pathFeaturesDicp = '/analyses/' + this.analysisGlobalService.analysis.ral_id + '/impact';

    this.lottieOnboardingFeature  = {
      animationData:  this.appConfig.getConfig().animationAssets['onboarding-feature'],
      renderer: 'svg',
      autoplay: true,
      loop: false
    };
  }

  public check() {
    this.isChecked = !this.isChecked;
    localStorage.setItem(this.userGlobalService.user.email + '-onboarding-feature', JSON.stringify(this.isChecked));
  }

  public handleAnimation(anim: any) {
    this.anim = anim;
    this.anim.setSpeed(1);
  }

  public close() {
    this.closeEvent.emit();
    this.isDisplayed = false;
    setTimeout(() => {
      this.dialogRef.close();
    }, 500);
  }

  public backToDicp() {
    this.router.navigate([this.pathFeaturesDicp, { isModify: false }]);
    this.close();
  }

  public getAnalysisName() {
    return this.analysisGlobalService.analysis.ral_name;
  }
}
