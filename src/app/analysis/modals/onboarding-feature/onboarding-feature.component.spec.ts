import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnboardingFeatureComponent } from './onboarding-feature.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { AppConfig } from 'src/app/app.config';
import { RouterTestingModule } from '@angular/router/testing';
import { AnalysisGlobalService } from '../../analysisGlobal.service';
import { UserGlobalService } from 'src/app/services/userGlobal.service';

describe('ModalExplicationFeatureComponent', () => {
  let component: OnboardingFeatureComponent;
  let fixture: ComponentFixture<OnboardingFeatureComponent>;

  const appConfigSpy = {
    getConfig: () => {
      return { animationAssets: {} };
    }
  };
  const matDialogRefSpy = {};

  const analysisGlobalServiceSpy = {analysis: {
    ral_id: '123'
  }};

  const userGlobalServiceMock = {
    user: {
      email: 'emial@email.com'
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [OnboardingFeatureComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [AnalysisGlobalService,
        { provide: UserGlobalService, useValue: userGlobalServiceMock},
        { provide: AppConfig, useValue: appConfigSpy },
        { provide: AnalysisGlobalService, useValue: analysisGlobalServiceSpy },
        { provide: MatDialogRef, useValue: matDialogRefSpy }
      ]

    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnboardingFeatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be true when check write in', () => {
    component.check();
    expect(component.isChecked).toBe(true);
  });
});
