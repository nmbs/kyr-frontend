import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnlockAnalysisComponent } from './unlock-analysis.component';
import { MatDialogRef } from '@angular/material';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('UnlockAnalysisComponent', () => {
  let component: UnlockAnalysisComponent;
  let fixture: ComponentFixture<UnlockAnalysisComponent>;
  const matDialogRefSpy = {};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnlockAnalysisComponent ],
      providers: [
        { provide: MatDialogRef, useValue: matDialogRefSpy }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnlockAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
