import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-unlock-analysis',
  templateUrl: './unlock-analysis.component.html',
  styleUrls: ['./unlock-analysis.component.scss']
})
export class UnlockAnalysisComponent implements OnInit {

  @Output() closeEvent = new EventEmitter<boolean>();
  public isDisplayed = true;
  public analysisName: string;

  constructor(public dialogRef: MatDialogRef<UnlockAnalysisComponent>) { }

  ngOnInit() {
  }

  public close(choice: boolean) {
    this.closeEvent.emit();
    this.isDisplayed = false;
    setTimeout(() => {
      this.dialogRef.close(choice);
    }, 500);
  }

}
