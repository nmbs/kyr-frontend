import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { AppConfig } from 'src/app/app.config';
import { Router } from '@angular/router';
import { AnalysisGlobalService } from '../../analysisGlobal.service';
import { UserGlobalService } from 'src/app/services/userGlobal.service';

@Component({
  selector: 'app-onboarding-impact',
  templateUrl: './onboarding-impact.component.html',
  styleUrls: ['./onboarding-impact.component.scss']
})
export class OnboardingImpactComponent implements OnInit {

  isDisplayed = true;
  isChecked = false;

  @Output() closeEvent = new EventEmitter<boolean>();

  public lottieOnboardingImpact: Object;

  private anim: any;
  private pathDescription: string;

  constructor(public dialogRef: MatDialogRef<OnboardingImpactComponent>,
    private appConfig: AppConfig,
    private readonly router: Router,
    private analysisGlobalService: AnalysisGlobalService,
    private userGlobalService: UserGlobalService) { }

  ngOnInit() {
    this.pathDescription = '/analyses/' + this.analysisGlobalService.analysis.ral_id + '/description';

    this.lottieOnboardingImpact  = {
      animationData:  this.appConfig.getConfig().animationAssets['onboarding-impact'],
      renderer: 'svg',
      autoplay: true,
      loop: false
    };
  }

  public close() {
    this.closeEvent.emit();
    this.isDisplayed = false;
    setTimeout(() => {
      this.dialogRef.close();
    }, 500);
  }

  public backToDescription() {
    this.router.navigate([this.pathDescription, { isModify: false }]);
    this.close();
  }

  public handleAnimation(anim: any) {
    this.anim = anim;
    this.anim.setSpeed(1);
  }

  public check() {
    this.isChecked = !this.isChecked;
    localStorage.setItem(this.userGlobalService.user.email + '-onboarding-impact', JSON.stringify(this.isChecked));
  }

  public getAnalysisName() {
    return this.analysisGlobalService.analysis.ral_name;
  }
}
