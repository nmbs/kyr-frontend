import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnboardingImpactComponent } from './onboarding-impact.component';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { AnalysisGlobalService } from '../../analysisGlobal.service';
import { MatDialogRef } from '@angular/material';
import { AppConfig } from 'src/app/app.config';
import { UserGlobalService } from 'src/app/services/userGlobal.service';

describe('OnboardingDescriptionComponent', () => {
  let component: OnboardingImpactComponent;
  let fixture: ComponentFixture<OnboardingImpactComponent>;

  const appConfigSpy = {
    getConfig: () => {
      return { animationAssets: {} };
    }
  };
  const matDialogRefSpy = {};

  const analysisGlobalServiceSpy = {analysis: {
    ral_id: '123'
  }};

  const userGlobalServiceMock = {
    user: {
      email: 'emial@email.com'
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [OnboardingImpactComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [{ provide: UserGlobalService, useValue: userGlobalServiceMock},
        { provide: AppConfig, useValue: appConfigSpy },
        { provide: AnalysisGlobalService, useValue: analysisGlobalServiceSpy },
        { provide: MatDialogRef, useValue: matDialogRefSpy }
      ]

    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnboardingImpactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be true when check write in', () => {
    component.check();
    expect(component.isChecked).toBe(true);
  });

});
