import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddModifyMeasureComponent } from './add-modify-measure.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { AnalysisGlobalService } from '../../analysisGlobal.service';
import { Analysis } from '../../api-models/analysis.model';
import { AppConfig } from 'src/app/app.config';
import { MatDialogRef } from '@angular/material';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Choice } from '../../models/choice.model';
import { AnalysisMeasureGlobalService } from '../../analysis-measure-global.service';

const measuresMock = [
  {
    ram_id: 'ram_id1',
    ral_id: 'ral_id1',
    fet_id: 'fet_id1',
    ram_isDisponibility: true,
    ram_isIntegrity: true,
    ram_isConfidenciality: true,
    ram_isProof: true,
    ram_cost: 5,
    ram_dt: 'ram_dt1',
    measureState: {
      mst_id: 'NOT_INPROGRESS',
      mst_name: 'mst_name1',
      mst_code: 'NOT_INPROGRESS',
    },
    measure: {
      mea_id: 'mea_id1',
      mea_name: 'mea_name1',
      mea_description: 'mea_description1',
      mea_isCustom: false,
      measureCategory: {
        mca_id: 'mca_id1',
        mca_name: 'mca_name1',
        mca_logo: 'mca_logo1',
      }
    },
    isChecked: true
  },
  {
    ram_id: 'ram_id2',
    ral_id: 'ral_id2',
    fet_id: 'fet_id2',
    ram_isDisponibility: true,
    ram_isIntegrity: true,
    ram_isConfidenciality: true,
    ram_isProof: true,
    ram_cost: 5,
    ram_dt: new Date().toISOString(),
    measureState: {
      mst_id: 'INPROGRESS',
      mst_name: 'mst_name2',
      mst_code: 'INPROGRESS',
    },
    measure: {
      mea_id: 'mea_id2',
      mea_name: 'mea_name2',
      mea_description: 'mea_description2',
      mea_isCustom: false,
      measureCategory: {
        mca_id: 'mca_id2',
        mca_name: 'mca_name2',
        mca_logo: 'mca_logo2',
      }
    },
    isChecked: true
  }
];

const mockChoices: Array<Choice> = [{
  cho_id: 'INPROGRESS',
  cho_name: '',
  cho_subtitle: '',
  cho_icon: '',
  cho_help: '',
  checked: null,
  date: null,
  status:  {
    mst_id: 'INPROGRESS',
    mst_name: 'mst_name2',
    mst_code: 'INPROGRESS',
  }
}, {
  cho_id: 'NOT_INPROGRESS',
  cho_name: 'Groupe',
  cho_subtitle: '',
  cho_icon: '',
  cho_help: '',
  checked: null,
  date: null,
  status:  {
    mst_id: 'NOT_INPROGRESS',
    mst_name: 'mst_name2',
    mst_code: 'NOT_INPROGRESS',
  }
}];

const mockAnalysis: Analysis = {
  ral_id: 'string',
  ral_name: 'string',
  ral_created_date: null,
  ral_updated_date: null,
  finalUsers: 'string',
  nbUser: 'string',
  dataSensibility: 'string',
  dataTypes: 'string',
  userAccesses: 'string',
  availabilityWeeks: 'string',
  availabilityDays: 'string',
  traceKeepingLaw: 'string',
  traceKeepingApp: 'string',
  solutionTypes: 'string',
  maxInterruptionTime: 'string',
  impactDataLoss: 'string',
  ral_isBlocked: false,
  features: [
    {
      fet_id: 'string',
      ral_id: 'string',
      fet_label: 'string',
      fet_description: 'string',
      fet_sensitive_nature: 'string',
      fet_created_date: 'string'
    }],
  impacts: null,
  opinion: null,
  decision: null,
  residualRisk: null
};
class MockAnalysisGlobalService {
  analysis: Analysis = mockAnalysis;
}

describe('AddModifyMeasureComponent', () => {
  let component: AddModifyMeasureComponent;
  let fixture: ComponentFixture<AddModifyMeasureComponent>;
  const matDialogRefSpy = {
    close: function () { }
  };

  beforeEach(async(() => {
    const appConfigSpy = jasmine.createSpyObj('AppConfig', {
      getConfig: function () {
        return '';
      }
    });

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [AddModifyMeasureComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        AnalysisMeasureGlobalService,
        { provide: AnalysisGlobalService, useValue: new MockAnalysisGlobalService() },
        { provide: AppConfig, useValue: appConfigSpy },
        { provide: MatDialogRef, useValue: matDialogRefSpy },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddModifyMeasureComponent);
    component = fixture.componentInstance;
    component.measures = measuresMock;
    component.currentMeasure = measuresMock[0];
    component.choicesDate = mockChoices;
    fixture.detectChanges();
  });

  it('should create ', () => {
    expect(component).toBeTruthy();
  });

  describe('Test selectChoiceExigences', () => {
    it('Check if currentMeasure is modified', () => {
      const choice = {
        cho_name: 'Disponibilité',
        checked: false,
        ch_id: 'ram_isDisponibility'
      };
      component.selectChoiceExigences(null, choice);
      expect(component.currentMeasure.ram_isDisponibility).toBeTruthy();
      component.selectChoiceExigences(null, choice);
      expect(component.currentMeasure.ram_isDisponibility).toBeFalsy();
    });
  });

  describe('Test selectChoiceDate', () => {
    it('Check if currentMeasure is modified', () => {
      const choice = {
        cho_name: 'Disponibilité',
        checked: false,
        ch_id: 'ram_isDisponibility',
        date: new Date(),
        status: {
          mst_id: 'string',
          mst_name: 'string',
          mst_code: 'string'
        }
      };
      component.selectChoiceDate(choice);
      expect(component.currentMeasure.measureState).toEqual(choice.status);
      expect(component.currentMeasure.ram_dt).toEqual(null);
      choice.status.mst_code = 'INPROGRESS';
      component.selectChoiceDate(choice);
      expect(component.currentMeasure.measureState).toEqual(choice.status);
      expect(component.currentMeasure.ram_dt).toEqual(choice.date.toISOString());
    });
  });

  describe('Test changeCostValue', () => {
    it('Check if currentMeasure is modified', () => {
      component.changeCostValue(4);
    });
  });

  describe('Test next', () => {
    it('Check if currentMeasure is changed and choice are reinit', () => {
      component.indexMeasure = 0;
      component.next();
      expect(component.indexMeasure).toEqual(1);
      expect(component.currentMeasure).toEqual(measuresMock[1]);
      expect(component.choicesExigences[0].checked).toBeTruthy();
      expect(component.choicesExigences[1].checked).toBeTruthy();
      expect(component.choicesExigences[2].checked).toBeTruthy();
      expect(component.choicesExigences[3].checked).toBeTruthy();
      expect(component.choicesDate[0].checked).toBeTruthy();
      expect(component.choicesDate[0].date).toEqual(mockChoices[0].date);
    });
  });

  describe('Test previous', () => {
    it('Check if currentMeasure is changed and choice are reinit', () => {
      component.indexMeasure = 1;
      component.currentMeasure = measuresMock[1];
      component.measures[0].measureState.mst_id = 'NOT_INPROGRESS';
      component.measures[0].measureState.mst_code = 'NOT_INPROGRESS';
      component.measures[0].ram_isDisponibility = true;
      component.previous();
      expect(component.indexMeasure).toEqual(0);
      expect(component.currentMeasure).toEqual(measuresMock[0]);
      expect(component.choicesExigences[0].checked).toBeTruthy();
      expect(component.choicesExigences[1].checked).toBeTruthy();
      expect(component.choicesExigences[2].checked).toBeTruthy();
      expect(component.choicesExigences[3].checked).toBeTruthy();
      expect(component.choicesDate[1].checked).toBeTruthy();
      expect(component.choicesDate[1].date).toEqual(null);
    });
  });
  describe('Test measures list checking', () => {
    it('Check if current measure is first measure of measures list', () => {
      expect(component.isFirstMeasure()).toEqual(true);
    });

    it('Check if current measure is not first measure of measures list', () => {
      const listLength = component.measures.length;
      component.currentMeasure = component.measures[listLength - 1];
      expect(component.isFirstMeasure()).toEqual(false);
    });

    it('Check if current measure is last measure of measures list', () => {
      const listLength = component.measures.length;
      component.currentMeasure = component.measures[listLength - 1];
      expect(component.isLastMeasure()).toEqual(true);
    });

    it('Check if current measure is last measure of measures list', () => {
      expect(component.isLastMeasure()).toEqual(false);
    });

    it('Check if delete first measure, current measure still first, list shrinks by one', () => {
      const lengthMeasures = component.measures.length;
      component.deleteMeasure();
      expect(lengthMeasures - 1).toEqual(component.measures.length);
      expect(component.currentMeasure).toEqual(component.measures[0]);
    });

    it('Check if delete last measure, current measure second to last of list, list shrinks by one', () => {
      const lengthMeasures = component.measures.length;
      component.currentMeasure = component.measures[lengthMeasures - 1];
      component.deleteMeasure();
      expect(lengthMeasures - 1).toEqual(component.measures.length);
      expect(component.currentMeasure).toEqual(component.measures[lengthMeasures - 2]);
    });
  });

});
