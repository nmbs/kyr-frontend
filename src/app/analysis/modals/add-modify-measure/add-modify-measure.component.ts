import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { AnalysisMeasure } from '../../models/measure/analysisMeasure';
import * as _ from 'lodash';
import { AnalysisMeasureService } from '../../analysis-measure/analysis-measure.service';
import { MeasureService } from '../../analysis-measure/measure.service';
import { Choice } from '../../models/choice.model';
import { MeasureCategory } from '../../models/measure/measureCategory';
import { AnalysisGlobalService } from '../../analysisGlobal.service';

const monthNames = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin',
  'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'
];

@Component({
  selector: 'app-add-modify-measure',
  templateUrl: './add-modify-measure.component.html',
  styleUrls: ['./add-modify-measure.component.scss'],
providers: [MeasureService, AnalysisMeasureService]
})
export class AddModifyMeasureComponent implements OnInit {

  isDisplayed = true;
  currentMeasure: AnalysisMeasure;
  measuresCategories: Array<MeasureCategory> = [];
  choicesExigences: Array<any> = [
    {
      cho_name: 'Disponibilité',
      checked: false,
      ch_id: 'ram_isDisponibility'
    },
    {
      cho_name: 'Intégrité',
      checked: false,
      ch_id: 'ram_isIntegrity'
    },
    {
      cho_name: 'Confidentialité',
      checked: false,
      ch_id: 'ram_isConfidenciality'
    },
    {
      cho_name: 'Preuve',
      checked: false,
      ch_id: 'ram_isProof'
    }
  ];

  choicesDate: Array<Choice> = [];

  public keysSlider: Array<any> = [
    {
      title: 0,
      subtitle: 'Gratuit'
    },
    {
      title: 1,
      subtitle: 'Economique'
    },
    {
      title: 2,
      subtitle: 'Coûteux'
    },
    {
      title: 3,
      subtitle: 'Très coûteux'
    }
  ];

  @Output() closeEvent = new EventEmitter<boolean>();

  @Input()
  public measures: Array<AnalysisMeasure>;

  @Input()
  public modifyMode: boolean;

  indexMeasure = 0;
  private _measureIncomingLabel;

  public featureName: string;

  constructor(public dialogRef: MatDialogRef<AddModifyMeasureComponent>,
    private analyseMeasureService: AnalysisMeasureService,
    private measureService: MeasureService,
    private analysisGlobalService: AnalysisGlobalService) {
  }

  ngOnInit() {
    if (this.modifyMode) {
      this.measures = _.cloneDeep(this.measures);
    }
    this.currentMeasure = this.measures[0];
    this.setFeature();
    this.measureService.getMeasuresCategories().subscribe(res => {
      res.forEach(measureCat => {
        this.measuresCategories.push(measureCat);
      });
    });
    this.analyseMeasureService.getMeasureState().subscribe(res => {
      res.forEach(measureState => {
        const choiceDate: Choice = {
          cho_id: measureState.mst_code,
          cho_name: measureState.mst_name,
          cho_subtitle: null,
          cho_icon: null,
          cho_help: null,
          checked: false,
          status: measureState
        };
        if (measureState.mst_code === 'INPROGRESS') {
          choiceDate.isDatePicker = true;
          this._measureIncomingLabel = measureState.mst_name;
          if (this.currentMeasure.ram_dt) {
            choiceDate.date = new Date(this.currentMeasure.ram_dt);
          }
        }
        if (this.currentMeasure.measureState === measureState) {
          choiceDate.checked = true;
        }
        this.choicesDate.push(choiceDate);
        if (this.modifyMode) {
          this.reinitChoices();
        }
      });
    });
  }

  public getLeftText(): string {
    const nbMeasures = this.measures.length;
    let text = nbMeasures.toString();
    if (nbMeasures > 1) {
      text += ' mesures sélectionnées';
    } else {
      text += ' mesure sélectionnée';
    }
    return text;
  }

  public close(measuresToAdd: Array<AnalysisMeasure>) {
    this.closeEvent.emit();
    this.isDisplayed = false;
    setTimeout(() => {
      this.dialogRef.close(measuresToAdd);
    }, 500);
  }

  public selectChoiceExigences(event, choice) {
    this.choicesExigences.forEach(cho => {
      if (cho.cho_name === choice.cho_name) {
        cho.checked = !cho.checked;
        this.currentMeasure[choice.ch_id] = cho.checked;
      }
    });
  }

  public selectChoiceDate(choice) {
    this.currentMeasure.measureState = choice.status;
    if (choice.status.mst_code === 'INPROGRESS') {
      this.currentMeasure.ram_dt = choice.date.toISOString();
    } else {
      this.currentMeasure.ram_dt = null;
    }
  }

  public changeCostValue(value) {
    this.currentMeasure.ram_cost = value.bulletNb;
  }

  public next() {
    if (this.indexMeasure < this.measures.length - 1) {
      this.indexMeasure++;
      this.currentMeasure = this.measures[this.indexMeasure];
      this.reinitChoices();
    }
    this.scrollToTop();
  }

  public previous() {
    if (this.indexMeasure > 0) {
      this.indexMeasure--;
      this.currentMeasure = this.measures[this.indexMeasure];
      this.reinitChoices();
    }
    this.scrollToTop();
  }

  public isLastMeasure(): boolean {
    return this.currentMeasure === this.measures[this.measures.length - 1];
  }

  public isFirstMeasure(): boolean {
    return this.currentMeasure === this.measures[0];
  }

  public createOrModifyMeasures() {
    if (this.modifyMode) {
      this.analyseMeasureService.updateMeasures(this.analysisGlobalService.analysis.ral_id, this.measures).subscribe(res => {
        this.close(this.measures);
      });
    } else {
      this.analyseMeasureService.createMeasures(this.analysisGlobalService.analysis.ral_id, this.measures).subscribe(res => {
        this.close(this.measures);
      });
    }
  }

  public deleteMeasure(): void {
    _.remove(this.measures, el => {
      return this.currentMeasure === el;
    });
    if (this.indexMeasure === this.measures.length) {
      this.indexMeasure--;
    }
    if (this.measures.length === 0) {
      this.close(this.measures);
    }
    this.currentMeasure = this.measures[this.indexMeasure];
  }

  // To choose right category to display in html select
  public byId(item1: MeasureCategory, item2: MeasureCategory): boolean {
    if (item1 && item2) {
      return item1.mca_id === item2.mca_id;
    }
    return !item2 && !item1;
  }

  private getDateString(date: Date): string {
    return monthNames[date.getMonth()] + ' ' + date.getFullYear();
  }

  private setFeature() {
    const feature = this.analysisGlobalService.analysis.features.find(f => f.fet_id === this.currentMeasure.fet_id);
    this.featureName = feature ? feature.fet_label : 'Bien principal';
  }

  private scrollToTop() {
    const el = document.getElementById('innerContent');
    el.scrollTo(0, 0);
    el.scroll(0, 0);
  }

  // Object choicesExigences and choicesDate reinit with new currentMeasure
  private reinitChoices() {
    this.setFeature();
    this.choicesExigences.forEach(choice => {
      choice.checked = this.currentMeasure[choice.ch_id];
    });

    this.choicesDate.forEach(choice => {
      choice.checked = this.currentMeasure.measureState && this.currentMeasure.measureState.mst_code === choice.cho_id;

      if (choice.cho_id === 'INPROGRESS') {
        if (this.currentMeasure.ram_dt) {
          choice.date = new Date(this.currentMeasure.ram_dt);
          choice.cho_name = this._measureIncomingLabel + ' ' + this.getDateString(choice.date);
        } else {
          choice.date = null;
          choice.cho_name = this._measureIncomingLabel;
        }
      } else {
        choice.date = null;
      }
    });
  }
}
