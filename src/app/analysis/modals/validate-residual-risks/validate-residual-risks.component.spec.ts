import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidateResidualRisksComponent } from './validate-residual-risks.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { AppConfig } from 'src/app/app.config';
import { MatDialogRef } from '@angular/material';

describe('ValidateResidualRisksComponent', () => {
  let component: ValidateResidualRisksComponent;
  let fixture: ComponentFixture<ValidateResidualRisksComponent>;

  const appConfigSpy = {
    getConfig: () => {
      return { animationAssets: {} };
    }
  };
  const matDialogRefSpy = {};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidateResidualRisksComponent ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: AppConfig, useValue: appConfigSpy },
        { provide: MatDialogRef, useValue: matDialogRefSpy }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidateResidualRisksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
