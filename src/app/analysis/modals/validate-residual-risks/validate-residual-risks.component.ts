import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { AppConfig } from 'src/app/app.config';

@Component({
  selector: 'app-validate-residual-risks',
  templateUrl: './validate-residual-risks.component.html',
  styleUrls: ['./validate-residual-risks.component.scss']
})
export class ValidateResidualRisksComponent implements OnInit {
  public displayOpinionWarning;
  public isDisplayed = true;

  @Output() closeEvent = new EventEmitter<boolean>();

  public lottieResidualRisk: Object;

  private anim: any;

  constructor(public dialogRef: MatDialogRef<ValidateResidualRisksComponent>,
    private appConfig: AppConfig) { }

  ngOnInit() {
    this.lottieResidualRisk  = {
      animationData:  this.appConfig.getConfig().animationAssets['popin-check'],
      renderer: 'svg',
      autoplay: true,
      loop: false
    };
  }

  public handleAnimation(anim: any) {
    this.anim = anim;
    this.anim.setSpeed(1);
  }

  public close(validate) {
    this.closeEvent.emit();
    this.isDisplayed = false;
    setTimeout(() => {
      this.dialogRef.close(validate);
    }, 500);
  }
}
