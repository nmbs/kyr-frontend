import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { Description } from './models/description.model';

@Injectable()
export class DescriptionGlobalService {

  public tellStorySubject = new ReplaySubject<Description>();
  public impactSubject = new ReplaySubject<Description>();
  public protectedAssetsSubject = new ReplaySubject<Description>();
  public measureSubject = new ReplaySubject<Description>();
  public riskSubject = new ReplaySubject<Description>();

  public stepIndexSubject = new ReplaySubject<number>();

  private _tellStory: Description;
  private _impact: Description;
  private _protectedAssets: Description;
  private _measures: Description;
  private _risks: Description;

  private _stepIndex: number;

  constructor() {
  }

  public set stepIndex(stepIndex: number) {
    this._stepIndex = stepIndex;
    this.stepIndexSubject.next(stepIndex);
  }

  public get stepIndex() {
    return this._stepIndex;
  }
  public set tellStory(tellStory: Description) {
    this._tellStory = tellStory;
    this.tellStorySubject.next(tellStory);
  }

  public get tellStory() {
    return this._tellStory;
  }

  public set impact(impact: Description) {
    this._impact = impact;
    this.impactSubject.next(impact);
  }

  public get impact() {
    return this._impact;
  }

  public set measures(measure: Description) {
    this._measures = measure;
    this.impactSubject.next(measure);
  }

  public get measures() {
    return this._measures;
  }

  public set risks(risk: Description) {
    this._risks = risk;
    this.riskSubject.next(risk);
  }

  public get risks() {
    return this._risks;
  }


  // ProtectedAssets regroups the 'impact' and 'feature' components in the same chapter in the sidebar
  public set protectedAssets(protectedAssets: Description) {
    this._protectedAssets = protectedAssets;
    this.protectedAssetsSubject.next(protectedAssets);
  }

  public get protectedAssets() {
    return this._protectedAssets;
  }
}
