import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { Analysis } from './api-models/analysis.model';
import { Feature } from './api-models/feature.model';

@Injectable()
export class AnalysisGlobalService {

  public analysisSubject = new ReplaySubject<Analysis>();
  public isSendingDoingQuerySubject = new ReplaySubject<boolean>();
  public hideSidebar = new ReplaySubject<boolean>();

  private _analysis: Analysis;
  private _isSendingDoingQuery = false;

  constructor() {
  }

  public hide(value: boolean) {
    this.hideSidebar.next(value);
  }
  public set analysis(analysis: Analysis) {
    this._analysis = analysis;
    this.analysisSubject.next(analysis);
  }

  public get analysis() {
    return this._analysis;
  }

  public set isSendingDoingQuery(value: boolean) {
    this._isSendingDoingQuery = value;
    this.isSendingDoingQuerySubject.next(value);
  }

  public get isSendingDoingQuery() {
    return this._isSendingDoingQuery;
  }
}
