import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeasureComponent } from './measure.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { UserGlobalService } from '../../services/userGlobal.service';
import { UserService } from '../../services/user.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

const mockAnalysisMeausure = {
  ram_id: 'a',
    ral_id: 'a',
    fet_id: 'a',
    ram_isDisponibility: true,
    ram_isIntegrity: true,
    ram_isConfidenciality: true,
    ram_isProof: true,
    ram_cost: 2,
    ram_dt: Date.now.toString(),
    measureState: null,
    measure: null,
    isChecked: false
};

describe('MeasureComponent', () => {
  let component: MeasureComponent;
  let fixture: ComponentFixture<MeasureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ MeasureComponent ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [UserGlobalService, UserService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeasureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('euroIsActive', () => {
    beforeEach(() => {
      component.measure = mockAnalysisMeausure;
    });

    it('active', () => {
      expect(component.euroIsActive(0)).toEqual('active');
    });

    it('inactive', () => {
      expect(component.euroIsActive(2)).toEqual('inactive');
    });

  });
});
