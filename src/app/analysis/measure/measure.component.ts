import { Component, OnInit, Input } from '@angular/core';
import { AnalysisMeasure } from '../models/measure/analysisMeasure';
import * as _ from 'lodash';

@Component({
  selector: 'app-measure',
  templateUrl: './measure.component.html',
  styleUrls: ['./measure.component.scss']
})
export class MeasureComponent implements OnInit {

  @Input()
  public isBlocked = false;

  public dicpArray = [{ 'property': 'ram_isDisponibility', 'letter': 'D' },
  { 'property': 'ram_isIntegrity', 'letter': 'I' },
  { 'property': 'ram_isConfidenciality', 'letter': 'C' },
  { 'property': 'ram_isProof', 'letter': 'P' }];

  public displayTooltip: false;
  public dt: Date;
  private _measure: AnalysisMeasure;
  constructor() {

  }

  @Input()
  public set measure(value: AnalysisMeasure) {
    this._measure = value;
    if (value && value.ram_dt) {
      this.dt = new Date(value.ram_dt);
    }
  }

  ngOnInit() {
  }

  public get measure() {
    return this._measure;
  }

  public euroIsActive(number) {
    return (this.measure.ram_cost && number + 1 < this.measure.ram_cost) ? 'active' : 'inactive';
  }

  public getName(name: string): string {
    return _.truncate(name, { 'length': 35, 'separator': ' ' });
  }

  public getPath(measure: AnalysisMeasure): string {
    return measure.measure.measureCategory ? measure.measure.measureCategory.mca_logo : 'unknown.svg';
  }
}
