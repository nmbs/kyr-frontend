import { Component, OnInit } from '@angular/core';
import { AnalysisGlobalService } from '../analysisGlobal.service';
import { DescriptionGlobalService } from '../descriptionGlobal.service';
import { Analysis } from '../api-models/analysis.model';
import { Description } from '../models/description.model';
import { AnalysisService } from '../analysis.service';

import { Step } from '../models/step.model';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

import { analysisDescription } from '../../../assets/analysis-description-form/analysis';
import { analysisProtectedAssets } from '../../../assets/analysis-protected-assets/protected-assets';

import { filter } from 'rxjs/operators';
import { User } from 'src/app/models/user';
import { UserGlobalService } from 'src/app/services/userGlobal.service';
import { MatDialog } from '@angular/material';
import { AddUsersComponent } from '../add-users/add-users.component';
import { rolePolicies } from '../../../assets/roles-policies/role-policies';
import { analysisMeasures } from 'src/assets/analysis-measure/measures';
import { analysisResidualRisk } from 'src/assets/analysis-residual-risk/residual-risk';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss'],
  providers: [AnalysisService]
})


export class SideBarComponent implements OnInit {

  public users: Array<User>;

  constructor(private analysisService: AnalysisService,
    private analysisGlobalService: AnalysisGlobalService,
    private descriptionGlobalService: DescriptionGlobalService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private userGlobalService: UserGlobalService,
    public dialog: MatDialog) {

  }

  public analysis: Analysis;
  public chapters: Array<Description> = [];
  public stepIndex: number;
  public svgCompletionPath: any;
  public path: Array<string> = [];
  private _analysisPath = '/analyses/';
  public displayAddUser = false;
  private _activeChapterIndex: number;
  private _percentage = [0, 0];
  private _roundPercentage = [null, null];
  public displayProgression = [];

  public displayAddUserModal = true;

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.path.push('/analyses/' + params.get('guid') + '/description');
      this.path.push('/analyses/' + params.get('guid') + '/impact');
      this.path.push('/analyses/' + params.get('guid') + '/measure');
      this.path.push('/analyses/' + params.get('guid') + '/residual-risk');
    });
    this.userGlobalService.usersSubject.subscribe(res => {
      this.users = res;
      this.users.sort((a: User, b: User) => (a.email > b.email) ? -1 : 1);
      const index = this.users.findIndex(usr => usr.guid === this.userGlobalService.user.guid);

      if (index > -1) {
        if (index < this.users.length - 1) {
          // Get current user and push it in the list to diplay first
          const user = this.users.splice(index, 1)[0];
          this.users.push(user);
          this.checkRole(user);
        } else {
          this.checkRole(this.users[this.users.length - 1]);
        }
      }
    });

    // Default is tell my story
    this._activeChapterIndex = 0;
    if (this.router.url.indexOf('impact') > -1 || this.router.url.indexOf('feature') > -1) {
      this._activeChapterIndex = 1;
    }
    if (this.router.url.indexOf('measure') > -1) {
      this._activeChapterIndex = 2;
    }
    if (this.router.url.indexOf('residual-risk') > -1) {
      this._activeChapterIndex = 3;
    }

    this.router.events.pipe(filter((e): e is NavigationEnd => e instanceof NavigationEnd)).subscribe(e => {
      this._activeChapterIndex = 0;
      // 'impact' and 'feature' and two sperate components with two different routing, but should be regrouped in the same chapter of the sidebar
      if (e.urlAfterRedirects.indexOf('impact') > -1 || e.urlAfterRedirects.indexOf('feature') > -1) {
        this._activeChapterIndex = 1;
      }
      if (e.urlAfterRedirects.indexOf('measure') > -1) {
        this._activeChapterIndex = 2;
      }
      if (this.router.url.indexOf('residual-risk') > -1) {
        this._activeChapterIndex = 3;
      }
    });

    this.analysis = this.analysisGlobalService.analysis;
    this.chapters.push(analysisDescription);
    this.chapters.push(analysisProtectedAssets);
    this.chapters.push(analysisMeasures);
    this.chapters.push(analysisResidualRisk);


    // ANALYSIS
    this.analysisGlobalService.analysisSubject.subscribe((res: Analysis) => {
      this.analysis = res;
      if (this.descriptionGlobalService.tellStory) {
        this._percentage[0] = this.analysisService.getCompletionPercentage(this.analysis, this.chapters[0]);
        this.setPercent(0);
        this.displayProgression[0] = true;
      }
    });

    // TELL MY STORY PART
    this.descriptionGlobalService.tellStorySubject.subscribe((tellStory: Description) => {
      this.chapters[0] = tellStory;
      if (this.analysis) {
        this._percentage[0] = this.analysisService.getCompletionPercentage(this.analysis, this.chapters[0]);
        this.setPercent(0);
        this.displayProgression[0] = true;
      }
    });

    // PROTECTED ASSETS PART
    this.descriptionGlobalService.protectedAssetsSubject.subscribe((protectedAssets: Description) => {
      this.chapters[1] = protectedAssets;
      if (this.analysis) {
        this._percentage[1] = this.analysisService.getCompletionPercentage(this.analysis, this.chapters[1]);
        this.setPercent(1);
        this.displayProgression[1] = false;
      }
    });

    // MEASURE PART
    this.descriptionGlobalService.measureSubject.subscribe((measure: Description) => {
      this.chapters[2] = measure;
      if (this.analysis) {
        this._percentage[2] = this.analysisService.getCompletionPercentage(this.analysis, this.chapters[2]);
        this.setPercent(2);
        this.displayProgression[2] = false;
      }
    });

    // RISK PART
    this.descriptionGlobalService.riskSubject.subscribe((risk: Description) => {
      this.chapters[3] = risk;
      if (this.analysis) {
        this._percentage[3] = this.analysisService.getCompletionPercentage(this.analysis, this.chapters[3]);
        this.setPercent(3);
        this.displayProgression[3] = false;

      }
    });

    // STEPS ACTIVATED
    this.descriptionGlobalService.stepIndexSubject.subscribe((res: number) => {
      this.stepIndex = res;
    });
  }

  openDialog(): void {
    this.dialog.open(AddUsersComponent);
  }

  private checkRole(user: User) {
    // If the user does not have the right role, we hide the 'add users' button
    // This is not a real security check, the api will block any query to the add or patch users anyway
    this.displayAddUser = rolePolicies.policies.find(r => r.role_code === user.role.rol_code).add_users;
  }
  public scrollTo(stepIndex, chapterIndex, step) {
    this.stepIndex = stepIndex;
    this.descriptionGlobalService.stepIndex = stepIndex;

    if (step.route) {
      // In certain configuration, multiple components with different routing should be regrouped under the same chapter in the sidebar
      // When this is the case, we specify it by filling the said route in the 'route' property (cf 'step' model)
      this.router.navigate([this._analysisPath + this.analysisGlobalService.analysis.ral_id + '/' + step.route, { isModify: true }]);
    }
  }

  public getRoundPercent(i) {
    return this._roundPercentage[i];
  }

  public getBorderStyle() {
    let borderStyle: any = {};
    const titleSize = 42;
    const subTitleSize = 34;
    const offset = 6;

    if (!this.chapters[this._activeChapterIndex].isModify || this.chapters[this._activeChapterIndex].steps.length === 0) {
      borderStyle = {
        top: (titleSize * this._activeChapterIndex + offset) + 'px'
      };
    } else {
      this.chapters[this._activeChapterIndex].steps.forEach((step: Step, index: number) => {
        if (step.isActive) {
          borderStyle = {
            top: (titleSize * (this._activeChapterIndex + 1) + index * subTitleSize) + 'px'
          };
        }
      });
    }
    return borderStyle;
  }

  public goToResume(i) {
    // stop previous navigate
    event.preventDefault();
    this.stepIndex = 0;
    this.descriptionGlobalService.stepIndex = 0;
    this.router.navigate([this.path[i], { isModify: false }]);
  }

  public getIconPath(i) {
    let pathToReturned = this.chapters[i].iconPath + '.svg';
    if (i === this._activeChapterIndex) {
      pathToReturned = this.chapters[i].iconPath + '-active.svg';
    }
    return pathToReturned;
  }

  public isActive(i) {
    return i === this._activeChapterIndex;
  }

  private setPercent(i) {
    const circumference = 2 * Math.PI * 8.5; // 2*PI*R
    // First Time wait to animate
    if (!this._roundPercentage[i]) {
      setTimeout(() => {
        this._roundPercentage[i] = Math.round(this._percentage[i] * circumference / 100);
      }, 300);
    } else {
      this._roundPercentage[i] = Math.round(this._percentage[i] * circumference / 100);
    }
  }

  public getContributorName(contributor: User): string {
    return this.userGlobalService.formatName(contributor.firstName, contributor.lastName);
  }

  public getNthLastEl(array: Array<any>, nb) {
    let arr = [];
    if (array) {
      arr = array.slice(Math.max(array.length - nb, 0), array.length);
      if (array.length > 3) {
        const numberPeople = '+' + (array.length - 3).toString();
        arr.unshift({ 'firstName': numberPeople });
      }
    }
    return arr;
  }

  public getNbTilesDiplay() {
    let nb: number;

    if (this.users.length > 3) {
      nb = 4;
    } else if (this.users.length === 3) {
      nb = 3;
    } else {
      nb = this.users.length;
    }

    return nb;
  }

}
