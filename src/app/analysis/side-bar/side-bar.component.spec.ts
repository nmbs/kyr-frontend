import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatDialog } from '@angular/material';
import { SideBarComponent } from './side-bar.component';
import { AnalysisService } from '../analysis.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { AppConfig } from '../../app.config';
import { AnalysisGlobalService } from '../analysisGlobal.service';
import { DescriptionGlobalService } from '../descriptionGlobal.service';
import { analysisDescription } from '../../../assets/analysis-description-form/analysis';
import { UserGlobalService } from 'src/app/services/userGlobal.service';

const description = analysisDescription;

describe('SideBarComponent', () => {
  let component: SideBarComponent;
  let fixture: ComponentFixture<SideBarComponent>;

  beforeEach(async(() => {
    const appConfigSpy = {
      getConfig: () => {
        return {};
      }
    };
    const matDialogSpy = {};
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [SideBarComponent],
      providers: [AnalysisService, AnalysisGlobalService, DescriptionGlobalService,
        { provide: MatDialog, useValue: matDialogSpy },
        { provide: AppConfig, useValue: appConfigSpy },
        UserGlobalService],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('getBorderStyle', () => {
    beforeEach(() => { });

    it('!isModify', () => {
      component.chapters.push(description);
      expect(component.getBorderStyle()).toEqual({
        top: '6px'
      });
    });
    it('isModify', () => {
      component.chapters.push(description);
      component.chapters[0].isModify = true;
      component.chapters[0].steps[0].isActive = true;
      expect(component.getBorderStyle()).toEqual({
        top: '42px'
      });
    });
  });
});
