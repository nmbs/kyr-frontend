// Modules
import { CommonModule } from '@angular/common';
import { AnalysisRoutingModule } from './analysis.routing';

import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { GraphicModule } from '../graphic-components/graphic.module';
import { FormCompModule } from '../form-components/formComp.module';

import { AnalysisComponent } from './analysis.component';
import { AnalysisListComponent } from './analysis-list/analysis-list.component';
import { AnalysisCreateComponent } from './analysis-create/analysis-create.component';
import { AnalysisDescriptionComponent } from './analysis-description/analysis-description.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { AnalysisImpactComponent } from './analysis-impact/analysis-impact.component';

import { DescriptionGlobalService } from './descriptionGlobal.service';
import { UserService } from '../services/user.service';
import { LottieAnimationViewModule } from 'ng-lottie';
import { AnalysisFeatureComponent } from './analysis-feature/analysis-feature.component';
import { AnalysisMeasureComponent } from './analysis-measure/analysis-measure.component';
import { OnboardingFeatureComponent } from './modals/onboarding-feature/onboarding-feature.component';
import { MatDialogModule, MatDialogRef } from '@angular/material';
import { MeasureComponent } from './measure/measure.component';
import { OnboardingImpactComponent } from './modals/onboarding-impact/onboarding-impact.component';
import { AnalysisMaxImpactsComponent } from './analysis-max-impacts/analysis-max-impacts.component';
import { AnalysisMeasureListComponent } from './analysis-measure-list/analysis-measure-list.component';
import { AddModifyMeasureComponent } from './modals/add-modify-measure/add-modify-measure.component';
import { AnalysisMeasureGlobalService } from './analysis-measure-global.service';
import { AddMeasuresComponent } from './modals/add-measures/add-measures.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { AnalysisResidualRiskComponent } from './analysis-residual-risk/analysis-residual-risk.component';
import { UnlockAnalysisComponent } from './modals/unlock-analysis/unlock-analysis.component';
import { AnalysisService } from './analysis.service';
import { ValidateResidualRisksComponent } from './modals/validate-residual-risks/validate-residual-risks.component';
import { OnboardingRisksComponent } from './modals/onboarding-risks/onboarding-risks.component';
import { OnboardingMeasuresComponent } from './modals/onboarding-measures/onboarding-measures.component';


@NgModule({
  declarations: [
    AnalysisComponent,
    AnalysisListComponent,
    AnalysisCreateComponent,
    AnalysisDescriptionComponent,
    SideBarComponent,
    AnalysisImpactComponent,
    AnalysisFeatureComponent,
    OnboardingImpactComponent,
    AnalysisMeasureComponent,
    OnboardingFeatureComponent,
    MeasureComponent,
    AnalysisMaxImpactsComponent,
    AnalysisMeasureListComponent,
    AddModifyMeasureComponent,
    AddMeasuresComponent,
    AnalysisResidualRiskComponent,
    UnlockAnalysisComponent,
    ValidateResidualRisksComponent,
    OnboardingRisksComponent,
    OnboardingMeasuresComponent
    ],
  imports: [
    CommonModule,
    AnalysisRoutingModule,
    FormsModule,
    GraphicModule,
    FormCompModule,
    MatDialogModule,
    LottieAnimationViewModule.forRoot(),
    ScrollingModule
  ],
  entryComponents: [
    OnboardingFeatureComponent,
    OnboardingImpactComponent,
    AddModifyMeasureComponent,
    AddMeasuresComponent,
    UnlockAnalysisComponent,
    ValidateResidualRisksComponent,
    OnboardingRisksComponent,
    OnboardingMeasuresComponent
  ],
  providers: [
    DescriptionGlobalService,
    UserService,
    AnalysisMeasureGlobalService,
    AnalysisService
  ]
})
export class AnalysisModule { }
