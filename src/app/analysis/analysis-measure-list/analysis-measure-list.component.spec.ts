import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AppConfig } from 'src/app/app.config';

import { AnalysisMeasureListComponent } from './analysis-measure-list.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { MatDialog } from '@angular/material';

describe('AnalysisMeasureListComponent', () => {
  let component: AnalysisMeasureListComponent;
  let fixture: ComponentFixture<AnalysisMeasureListComponent>;

  const measuresMock = [
    {
      ram_id: 'string',
      ral_id: 'string',
      fet_id: 'string',
      ram_isDisponibility: null,
      ram_isIntegrity: null,
      ram_isConfidenciality: null,
      ram_isProof: null,
      ram_cost: 0,
      ram_dt: 'string',
      measureState: null,
      measure: null,
      isChecked: true
    },
    {
      ram_id: 'string',
      ral_id: 'string',
      fet_id: 'string',
      ram_isDisponibility: null,
      ram_isIntegrity: null,
      ram_isConfidenciality: null,
      ram_isProof: null,
      ram_cost: 0,
      ram_dt: 'string',
      measureState: null,
      measure: null,
      isChecked: null
    }
  ];

  beforeEach(async(() => {
    const appConfigSpy = {
      getConfig: () => {
        return { animationAssets: {} };
      }
    };
    const matDialogSpy = {};
    TestBed.configureTestingModule({
      declarations: [AnalysisMeasureListComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: AppConfig, useValue: appConfigSpy },
        { provide: MatDialog, useValue: matDialogSpy }
       ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalysisMeasureListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.measures = measuresMock;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Test check', () => {
    it('measure.isChecked should equal to false', () => {
      component.check(null, component.measures[0]);
      expect(component.measures[0].isChecked).toEqual(false);
    });
    it('measure.isChecked should equal to true', () => {
      component.check(null, component.measures[1]);
      expect(component.measures[1].isChecked).toEqual(true);
    });
  });

  describe('Test formatTitle', () => {
    it('Should Uppercase the first letter of the first word', () => {
      const res = component.formatTitle('bonjOur MaDame');
      expect(res).toEqual('BonjOur MaDame');
    });
  });
});
