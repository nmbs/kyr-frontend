import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AnalysisMeasure } from '../models/measure/analysisMeasure';
import { AppConfig } from 'src/app/app.config';
import * as _ from 'lodash';
import { MatDialog } from '@angular/material';
import { AddMeasuresComponent } from '../modals/add-measures/add-measures.component';

enum AnimEvent {
  In = 1,
  Out = 2,
  Click = 3
}

@Component({
  selector: 'app-analysis-measure-list',
  templateUrl: './analysis-measure-list.component.html',
  styleUrls: ['./analysis-measure-list.component.scss']
})
export class AnalysisMeasureListComponent implements OnInit {
  @Output() clickEvent = new EventEmitter<void>();

  @Output() addMeasureEvent = new EventEmitter<void>();

  @Output() measureCheckedIdEvent = new EventEmitter<AnalysisMeasure>();

  @Input()
  public measures: Array<AnalysisMeasure>;

  @Input()
  public title: string;

  @Input()
  public parentId: string;

  @Input()
  public mainAsset: boolean;

  @Input()
  public isBlocked: boolean;

  @Input()
  public set cannotBeAdded(cannotBeAdded: Array<AnalysisMeasure>) {
    this._cannotBeAdded = cannotBeAdded;
    this.featureCannotBeAdded();
  }

  private _cannotBeAdded: Array<AnalysisMeasure>;
  public position = AnimEvent.In;
  public lottieConfigClick: Object;
  public lottieConfigHoverIn: Object;
  public lottieConfigHoverOut: Object;
  private animClick: any;
  private animHover: any;
  private animHoverOut: any;

  constructor(private appConfig: AppConfig,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.lottieConfigClick = {
      animationData: this.appConfig.getConfig().animationAssets['add-measure-click'],
      renderer: 'svg',
      autoplay: false,
      loop: false
    };

    this.lottieConfigHoverIn = {
      animationData: this.appConfig.getConfig().animationAssets['add-measure-hover-in'],
      renderer: 'svg',
      autoplay: false,
      loop: false
    };

    this.lottieConfigHoverOut = {
      animationData: this.appConfig.getConfig().animationAssets['add-measure-hover-out'],
      renderer: 'svg',
      autoplay: false,
      loop: false
    };
  }

  // If measure list is not the analysis but a feature then make sure cannotBeAddedMeasures actually owned by feature
  private featureCannotBeAdded() {
    if (!this.mainAsset && this._cannotBeAdded) {
      this._cannotBeAdded = this._cannotBeAdded.filter(
        analysisMeasure => analysisMeasure.fet_id === null || analysisMeasure.fet_id === this.parentId
      );
    }
  }
  public handleAnimationClick(anim: any) {
    this.animClick = anim;
    this.animClick.setSpeed(1);
  }

  public handleAnimationHoverIn(anim: any) {
    this.animHover = anim;
    this.animHover.setSpeed(1);
  }

  public handleAnimationHoverOut(anim: any) {
    this.animHoverOut = anim;
    this.animHoverOut.setSpeed(1);
  }

  // Call modal to trat add measure
  public addMeasure() {
    this.playClick();
    const dialogRef = this.dialog.open(AddMeasuresComponent);
    const instance = dialogRef.componentInstance;
    instance.featureName = this.formatTitle(this.title);
    instance.parentId = this.parentId;
    instance.cannotBeAdded = this._cannotBeAdded;
    // Let know analysis-measure when modal closed so reloads features with new measures
    dialogRef.afterClosed().subscribe(measuresToAdd => {
      if (measuresToAdd && measuresToAdd.length > 0) {
        this.addMeasureEvent.emit();
      }
    });
  }

  private playClick() {
    this.position = AnimEvent.Click;
    if (this.animClick) {
      this.animClick.stop();
      this.animClick.play();
    }
    this.clickEvent.emit();
  }

  public playHoverIn() {
    this.position = AnimEvent.In;
    if (this.animHover) {
      this.animHover.stop();
      this.animHover.play();
    }
  }

  public playHoverOut() {
    // Does not play if after click
    if (this.position !== AnimEvent.Click && this.animHoverOut) {
      this.position = AnimEvent.Out;
      this.animHoverOut.stop();
      this.animHoverOut.play();
    }
  }

  public check(event, measure: AnalysisMeasure) {
    if (!this.isBlocked) {
      if (measure.isChecked) {
        measure.isChecked = !measure.isChecked;
      } else {
        measure.isChecked = true;
      }
      this.measureCheckedIdEvent.emit(measure);
    }
  }

  // Uppercase the first letter of the first word
  public formatTitle(entry: string): string {
    return _.upperFirst(entry);
  }
}
