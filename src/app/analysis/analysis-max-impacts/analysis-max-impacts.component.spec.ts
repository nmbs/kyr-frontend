import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { AnalysisMaxImpactsComponent } from './analysis-max-impacts.component';

describe('AnalysisMaxImpactsComponent', () => {
  let component: AnalysisMaxImpactsComponent;
  let fixture: ComponentFixture<AnalysisMaxImpactsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AnalysisMaxImpactsComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalysisMaxImpactsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
