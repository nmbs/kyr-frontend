import { Component, OnInit, Input } from '@angular/core';
import { sliderSummaryDICPSmall } from '../../../assets/analysis-impact-form/sliders-summary';
@Component({
  selector: 'app-analysis-max-impacts',
  templateUrl: './analysis-max-impacts.component.html',
  styleUrls: ['./analysis-max-impacts.component.scss']
})
export class AnalysisMaxImpactsComponent implements OnInit {

  @Input()
  public guid: string;

  @Input()
  public analysisName: string;

  @Input()
  public smallVersion: boolean;

  @Input()
  public noSliders = false;


  public maxImpact: any;
  public slidersData = {
    sliders: sliderSummaryDICPSmall
  };

  constructor() { }

  ngOnInit() {
  }


}
