import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Analysis } from '../api-models/analysis.model';
import { AnalysisGlobalService } from '../analysisGlobal.service';
import { AppConfig } from 'src/app/app.config';
import { HttpHelper } from 'src/app/helpers/http.helper';
import { Feature } from '../api-models/feature.model';

@Injectable({
  providedIn: 'root'
})
export class AnalysisFeatureService {
  private _api: string;
  private _urlAnalysis: string;
  private _urlFeature: string;

  constructor(private _http: HttpClient, private appConfig: AppConfig,
    private analysisGlobalService: AnalysisGlobalService) {
    this._api = appConfig.getConfig().apiAnalysisUrl;
    this._urlAnalysis = this._api + '/analyses';
    this._urlFeature = this._api + '/features';
  }

  // Create a new feature
  public createFeature(feature: Feature): Observable<Analysis> {
    // Updates query status in the sidebar
    this.analysisGlobalService.isSendingDoingQuery = true;

    return this._http.post<Analysis>(this._urlAnalysis + '/' + feature.ral_id + '/features', feature, { headers: HttpHelper.getHeaders() })
      .pipe(
        map(res => {
          this.analysisGlobalService.analysis = res;
          // Updates query status in the sidebar
          this.analysisGlobalService.isSendingDoingQuery = false;

          return res;
        }),
        catchError(HttpHelper.handleError));
  }

  public patchFeature(body: any, api_path: string): Observable<Feature> {
    // Updates query status in the sidebar
    this.analysisGlobalService.isSendingDoingQuery = true;

    return this._http.patch<Feature>(this._api + api_path, body, { headers: HttpHelper.getHeaders() })
      .pipe(
        map(res => {
          const indexFeat = this.analysisGlobalService.analysis.features.findIndex(f => f.fet_id === res.fet_id);
          this.analysisGlobalService.analysis.features[indexFeat] = res;
          // Updates query status in the sidebar
          this.analysisGlobalService.isSendingDoingQuery = false;

          return res;
        }),
        catchError(HttpHelper.handleError)
      );
  }

  // Delete a feature
  public deleteFeature(feature: Feature): Observable<Analysis> {
    // Updates query status in the sidebar
    this.analysisGlobalService.isSendingDoingQuery = true;

    return this._http.delete<Feature>(this._urlFeature + '/' + feature.fet_id,
      { headers: HttpHelper.getHeaders() })
      .pipe(map(() => {
        const indexFeat = this.analysisGlobalService.analysis.features.findIndex(f => f.fet_id === feature.fet_id);
        if (indexFeat > -1) {
          this.analysisGlobalService.analysis.features.splice(indexFeat, 1);
        }
        // Updates query status in the sidebar
        this.analysisGlobalService.isSendingDoingQuery = false;

        return this.analysisGlobalService.analysis;
      }),
        catchError(HttpHelper.handleError));
  }
}
