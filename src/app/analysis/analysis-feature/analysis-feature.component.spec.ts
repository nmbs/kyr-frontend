import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalysisFeatureComponent } from './analysis-feature.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { AnalysisFeatureService } from './analysis-feature.service';
import { AnalysisService } from '../analysis.service';
import { AppConfig } from 'src/app/app.config';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AnalysisGlobalService } from '../analysisGlobal.service';
import { DescriptionGlobalService } from '../descriptionGlobal.service';

import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { Feature } from '../api-models/feature.model';
import { MatDialog } from '@angular/material';
import { UserGlobalService } from 'src/app/services/userGlobal.service';

const mockAnalysis = {
  ral_id: 'string',
  ral_name: 'string',
  ral_created_date: null,
  ral_updated_date: null,
  finalUsers: 'string',
  nbUser: 'string',
  dataSensibility: 'string',
  dataTypes: 'string',
  userAccesses: 'string',
  availabilityWeeks: 'string',
  availabilityDays: 'string',
  traceKeepingLaw: 'string',
  traceKeepingApp: 'string',
  solutionTypes: 'string',
  maxInterruptionTime: 'string',
  impactDataLoss: 'string',
  ral_isBlocked: false,
  features: [
    {
      fet_id: 'string',
      ral_id: 'string',
      fet_created_date: 'string',
      fet_label: 'string',
      fet_description: 'string',
      fet_sensitive_nature: 'string'
    }],
  impacts: null,
  opinion: null,
  decision: null,
  residualRisk: null
};

const mockStep = {
  title: 'string',
  subtitle: 'string',
  route: null,
  inputTitle: true,
  maxCharacterTitle: 0,
  isTitleFocused: true,
  placeHolderTitle: 'string',
  errorText: 'string',
  isErrorDisplayed: true,
  isActive: true,
  paging: () => '',
  questions: [],
  apiPath: () => '',
  id: 'string',
  deleteButton: true
};

const userGlobalServiceMock = {
  user: {
    email: 'emial@email.com'
  },
  users: [{ guid: 'guid', email: 'email', firstName: 'firstName', lastName: 'lastName', role: null }]
};

class MockAnalysisFeatureService {
  mockFeatureAnalysis = {
    ral_id: 'string',
    ral_name: 'string',
    ral_created_date: null,
    ral_updated_date: null,
    accessibilities: 'string',
    accessibilityHours: 'string',
    dataTypes: 'string',
    finalUsers: 'string',
    frequencyUsages: 'string',
    hostingTypes: 'string',
    issues: 'string',
    itRisks: 'string',
    nbUsers: 'string',
    solutionControls: 'string',
    solutionTargets: 'string',
    solutionTypes: 'string',
    userAccesses: 'string',
    userLocalisations: 'string',
    ral_isBlocked: false,
    features: [
      {
        fet_id: 'string',
        ral_id: 'string',
        fet_label: 'string',
        fet_description: 'string',
        fet_sensitive_nature: 'string'
      }],
    impacts: null,
    opinion: null,
    decision: null,
    residualRisk: null
  };

  createFeature(feature: Feature) {
    feature.fet_id = 'string2';
    this.mockFeatureAnalysis.features.push(feature);
    return of(this.mockFeatureAnalysis);
  }

  deleteFeature(feature: Feature) {
    return of(this.mockFeatureAnalysis);
  }
}

describe('AnalysisFeatureComponent', () => {
  let component: AnalysisFeatureComponent;
  let fixture: ComponentFixture<AnalysisFeatureComponent>;

  const matDialogSpy = {};

  beforeEach(async(() => {
    const appConfigSpy = jasmine.createSpyObj('AppConfig', {
      getConfig: function () {
        return '';
      }
    });

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [AnalysisFeatureComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [{ provide: UserGlobalService, useValue: userGlobalServiceMock},
        AnalysisGlobalService, AnalysisService, DescriptionGlobalService,
        { provide: AnalysisFeatureService, useValue: new MockAnalysisFeatureService()},
        { provide: AppConfig, useValue: appConfigSpy },
        { provide: MatDialog, useValue: matDialogSpy },
        { provide: ActivatedRoute, useValue: { parent: { paramMap: of() }, paramMap: of() } }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalysisFeatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.analysis = mockAnalysis;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Test create feature', () => {
    it('Should push new feature in step features and add id', () => {
      component.addFeature();
      expect(component.featureSteps[0].id).toEqual('string2');
    });
  });

  describe('Test delete feature', () => {
    it('Should delete feature in step feature', () => {
      const mockStepCopy = Object.assign({}, mockStep);
      component.featureSteps.push(mockStepCopy);
      component.deleteFeature(mockStepCopy);
      expect(component.featureSteps.length).toEqual(0);
    });
  });

  describe('Test disable button', () => {
    it('Should return true if one feature doesnt have a title', () => {
      const mockStepCopy = Object.assign({}, mockStep);
      mockStepCopy.title = null;
      component.featureSteps.push(mockStepCopy);
      expect(component.isAddButtonDisable()).toEqual(true);
    });

    it('Should return true if one feature has an empty title', () => {
      const mockStepCopy = Object.assign({}, mockStep);
      mockStepCopy.title = '';
      component.featureSteps.push(mockStepCopy);
      expect(component.isAddButtonDisable()).toEqual(true);
    });
  });
});
