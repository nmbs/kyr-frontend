import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AnalysisFeature } from '../../../assets/analysis-feature-form/features';
import { Feature } from '../api-models/feature.model';
import { Analysis } from '../api-models/analysis.model';
import { ActivatedRoute, Router } from '@angular/router';
import { AnalysisService } from '../analysis.service';
import { AnalysisFeatureService } from './analysis-feature.service';
import { DescriptionGlobalService } from '../descriptionGlobal.service';
import { Step } from '../models/step.model';
import * as _ from 'lodash';
import { MatDialog } from '@angular/material';
import { OnboardingFeatureComponent } from '../modals/onboarding-feature/onboarding-feature.component';
import { UserGlobalService } from 'src/app/services/userGlobal.service';

@Component({
  selector: 'app-analysis-feature',
  templateUrl: './analysis-feature.component.html',
  styleUrls: ['./analysis-feature.component.scss'],
  providers: [AnalysisService]
})
export class AnalysisFeatureComponent implements OnInit, AfterViewInit {
  private _analysis_id: string;
  private _analysisPath = '/analyses/';
  public analysisFeature = AnalysisFeature;
  // List of feature's model filled with features data
  public featureSteps: Array<Step> = [];
  public analysis: Analysis;
  public placeholder = true;
  public hasError = false;
  public noContentText: string;
  public addButtonDisable = false;
  public onboardingFeature = this.userGlobalService.user.email + '-onboarding-feature';

  constructor(private readonly route: ActivatedRoute,
    private analysisService: AnalysisService,
    private analysisFeatureService: AnalysisFeatureService,
    private readonly router: Router,
    private descriptionGlobalService: DescriptionGlobalService,
    public dialog: MatDialog,
    private userGlobalService: UserGlobalService) { }

  ngOnInit() {
    // Get GUID from the route params and get the analysis
    this.route.parent.paramMap.subscribe(params => {
      this._analysis_id = params.get('guid');
      this.analysisService.getAnalysis(this._analysis_id).subscribe(res => {
        this.analysis = res;
        if (this.analysis.ral_isBlocked) {
          this.router.navigate([this._analysisPath + this.analysis.ral_id + '/impact', { isModify: false }]);
        }
        this.noContentText = 'Vous pouvez à tout moment détailler les fonctionnalités sensibles de ' +
          this.analysis.ral_name
          + '. \nDétaillez également à quel point ces dernières sont importantes afin que nous puissions les protéger au mieux !';
        this.placeholder = false;
        // Init the list of step
        this.initFeatureSteps();
        // Check if the add button need to be disable
        this.addButtonDisable = this.isAddButtonDisable();
        this.route.paramMap.subscribe(param => {
          const anchor = param.get('anchor');
          // Check if there is an anchor param in the link
          if (anchor) {
            setTimeout(() => {
              // Scroll to the form number given by the anchor param
              this.scrollToForm(anchor);
            }, 100);
          } else {
            this.scrollToTop();
          }
          if (localStorage.getItem(this.onboardingFeature) === null ||
            localStorage.getItem(this.onboardingFeature) === 'false') {
            this.openDialog();
          }
        });
      });
    });
    this.updateSideBar();
  }

  ngAfterViewInit() {
  }

  private updateSideBar() {
    if (this.descriptionGlobalService.protectedAssets) {
      // Coloring the 'feature' anchor and uncoloring the 'impact' anchor
      this.descriptionGlobalService.protectedAssets.steps[0].isActive = false;
      this.descriptionGlobalService.protectedAssets.steps[1].isActive = true;
      // IsModify is always true of the feature page
      this.descriptionGlobalService.protectedAssets.isModify = true;
      // This is the second step of the chapter in the sidebar tree
      this.descriptionGlobalService.stepIndex = 1;
    }
  }

  // Scroll to the previous block
  public prev(index: number) {
    this.scrollToForm(index - 1);
  }

  // Scroll to the next block
  public next(index: number) {
    this.scrollToForm(index + 1);
  }

  private scrollToForm(index) {
    const element_to_scroll_to = document.getElementById('form' + index);
    if (element_to_scroll_to) {
      element_to_scroll_to.scrollIntoView({ behavior: 'smooth' });
    }
  }

  public setActive(step: Step, isActive: boolean) {
    step.isActive = isActive;
  }

  private scrollToTop() {
    window.scrollTo(0, 0);
    window.scroll(0, 0);
    scroll(0, 0);
  }

  // Init the feature steps list with the features data from the analysis
  public initFeatureSteps() {
    // Step number used for the paging
    let stepNumber = 1;
    // Get the only step in the feature model
    const featureStep = this.analysisFeature.steps[0];
    this.analysis.features = _.sortBy(this.analysis.features, 'fet_label');
    // Iterate through the features and fill the model
    this.analysis.features.forEach(feature => {
      // Clone the description and sensible question and set the value with db data
      const descriptionQuestion = Object.assign({}, featureStep.questions[0]);
      const sensibleNatureQuestion = Object.assign({}, featureStep.questions[1]);
      descriptionQuestion.value = feature.fet_description;
      sensibleNatureQuestion.value = feature.fet_sensitive_nature;
      // Fill and push the step in the list
      this.featureSteps.push({
        title: feature.fet_label,
        inputTitle: featureStep.inputTitle,
        isTitleFocused: featureStep.isTitleFocused,
        placeHolderTitle: featureStep.placeHolderTitle,
        errorText: featureStep.errorText,
        isErrorDisplayed: this.isTitleNullOrEmpty(feature.fet_label),
        subtitle: null,
        route: null,
        isActive: true,
        paging: featureStep.paging(stepNumber + '/' + this.analysis.features.length),
        apiPath: featureStep.apiPath,
        id: feature.fet_id,
        deleteButton: featureStep.deleteButton,
        questions: [descriptionQuestion, sensibleNatureQuestion],
        maxCharacterTitle: featureStep.maxCharacterTitle
      });
      stepNumber++;
    },
      error => {
        this.hasError = true;
      });
  }

  // Add a new feature in step list and in features list
  public addFeature() {
    const newAnalysisLength = this.analysis.features.length + 1;
    const featureStep = this.analysisFeature.steps[0];
    // Create the new step with empty value
    const newFeatureStep = {
      title: null,
      inputTitle: featureStep.inputTitle,
      isTitleFocused: true,
      placeHolderTitle: featureStep.placeHolderTitle,
      errorText: featureStep.errorText,
      maxCharacterTitle: featureStep.maxCharacterTitle,
      isErrorDisplayed: true,
      isActive: true,
      paging: featureStep.paging(newAnalysisLength + '/' + newAnalysisLength),
      apiPath: featureStep.apiPath,
      questions: featureStep.questions,
      deleteButton: featureStep.deleteButton,
    };
    this.featureSteps.push(newFeatureStep);

    // Create new feature with empty value
    const feature = {
      fet_id: null,
      ral_id: this._analysis_id,
      fet_created_date: null,
      fet_label: null,
      fet_description: null,
      fet_sensitive_nature: null
    };
    this.createFeature(feature, newFeatureStep);
  }

  // Create the feature in the db using service and set the ID of the feature to the step
  public createFeature(feature: Feature, step: Step) {
    this.analysisFeatureService.createFeature(feature).subscribe(res => {
      const newFeature = _.differenceBy(res.features, this.analysis.features, 'fet_id');
      this.analysis = res;
      step.id = newFeature[0].fet_id;
      this.addButtonDisable = this.isAddButtonDisable();
      // Update the paging of the remainings steps
      this.updatePaging();
      // Scroll to the new form
      this.scrollToForm(this.featureSteps.length - 1);
    });
  }

  // Delete the feature in the db + in the steps
  public deleteFeature(step: Step) {
    const feature = this.analysis.features.find(f => f.fet_id === step.id);
    if (feature !== null) {
      this.analysisFeatureService.deleteFeature(feature).subscribe(analysis => {
        this.analysis = analysis;
        this.removeStep(step);
        this.addButtonDisable = this.isAddButtonDisable();
        // Update the paging of the remainings steps
        this.updatePaging();
      });
    }
  }

  // Update the title of the given feature
  public updateTitleFeature(featureLabel: string, step: Step) {
    const requestBody = [{
      op: 'replace',
      path: 'fet_label',
      value: featureLabel
    }];
    const apiPath = step.apiPath(step.id);
    this.analysisFeatureService.patchFeature(requestBody, apiPath).subscribe(res => {
      // Check if the add button is still disable
      this.addButtonDisable = this.isAddButtonDisable();
      // Check if the error (title needed) is displayed
      step.isErrorDisplayed = this.isTitleNullOrEmpty(featureLabel);
      step.isTitleFocused = false;
    });
  }

  // Find and remove the step in the list
  private removeStep(step: Step) {
    const indexFeat = this.featureSteps.findIndex(f => f === step);
    if (indexFeat > -1) {
      if (indexFeat === this.featureSteps.length) {
        this.prev(indexFeat);
      } else {
        this.next(indexFeat);
      }
      this.featureSteps.splice(indexFeat, 1);
    }
  }

  // Add button is disable if at least one feature doesn't have a title
  public isAddButtonDisable(): boolean {
    const indexFeat = this.featureSteps.findIndex(f => this.isTitleNullOrEmpty(f.title));
    return indexFeat > -1;
  }

  private isTitleNullOrEmpty(title: string | Function): boolean {
    // Check if the title is either null or empty (contains only space or length is 0)
    return (typeof title === 'string' && title.replace(/\s/g, '').length === 0) || title === null;
  }

  // Update the paging of the steps
  private updatePaging() {
    let stepNumber = 1;
    const featureStepLength = this.featureSteps.length;
    this.featureSteps.forEach(step => {
      step.paging = this.analysisFeature.steps[0].paging(stepNumber + '/' + featureStepLength);
      stepNumber++;
    });
  }

  public goToRecap() {
    this.router.navigate(['/analyses/' + this._analysis_id + '/impact']);
  }

  public openDialog(): void {
    if (!this.analysis.ral_isBlocked) {
      this.dialog.open(OnboardingFeatureComponent);
    }
  }
}
