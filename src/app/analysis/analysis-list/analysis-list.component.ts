import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppConfig } from '../../app.config';
import { HomeService } from '../../home/home.service';
import { AnalysisLight } from '../../home/models/analysisLight';
import { User } from '../../models/user';
import { UserGlobalService } from 'src/app/services/userGlobal.service';
import * as _ from 'lodash';
import { MatDialog } from '@angular/material';
import { UnlockAnalysisComponent } from '../modals/unlock-analysis/unlock-analysis.component';
import { AnalysisService } from '../analysis.service';

@Component({
  selector: 'app-analysis-list',
  templateUrl: './analysis-list.component.html',
  styleUrls: ['./analysis-list.component.scss']
})
export class AnalysisListComponent implements OnInit {

  constructor(private readonly router: Router, private appConfig: AppConfig,
    private homeService: HomeService,
    private userGlobalService: UserGlobalService,
    public dialog: MatDialog, private analyseService: AnalysisService) {
  }

  public analyses: Array<AnalysisLight>;
  public placeholder = true;
  private iconPath = '/assets/img/icons/lock.svg';
  public listTooltipLock: Array<boolean> = [];
  public listTooltipDate: Array<boolean> = [];

  ngOnInit() {
    this.homeService.getAnalyses().subscribe(res => {
      // Sort the analyses according to the role the current user has in it
      this.analyses = this.sortAnalysesPerRoles(res);
      this.placeholder = false;
      this.analyses.forEach(analysis => {
        this.listTooltipLock.push(false);
        this.listTooltipDate.push(false);
      });
    });
  }

  // Naviagate to the stipulated URL with the given arguments
  public goTo(path: string, arg: any = null) {
    let navigateTo;
    if (arg) {
      navigateTo = [path, arg];
    } else {
      navigateTo = [path];
    }
    this.router.navigate(navigateTo);
  }

  // Go to the clicked analysis in recap mode
  public goToAnalysis(guid: string) {
    this.goTo('/analyses/' + guid + '/description', { isModify: false });
  }

  // Returns an array with the nb of users in param and the amount of users counted after this nb
  public getNthLastEl(array: Array<any>, nb): Array<any> {
    let arr = [];
    if (array) {
      arr = array.slice(Math.max(array.length - nb, 0), array.length);
      if (array.length > 2) {
        const numberPeople = '+' + (array.length - 2).toString();
        arr.unshift({ 'firstName': numberPeople });
      }
    }
    return arr;
  }

  public getIconPath(): string {
    return this.iconPath;
  }

  public getDecisionText(analysis: AnalysisLight): string {
    let text: string;
    if (analysis.isAccepted === true) {
      text = 'Validé';
    } else if (analysis.isAccepted === false) {
      text = 'Refusé';
    } else {
      text = 'En attente';
    }
    return text;
  }

  public getBorderStyle(analysis: AnalysisLight) {
    let borderStyle: any = {};

    if (analysis.isAccepted) {
      borderStyle = {
        color: '#58be00',
        'background-color': '#e6f8d6'
      };
    } else if (analysis.isAccepted === false) {
      borderStyle = {
        color: '#f30238',
        'background-color': '#fdd6df'
      };
    } else {
      borderStyle = {
        color: '#677d91',
        'background-color': '#ebeef1'
      };
    }
    return borderStyle;
  }

  public triggerHelp(index: number): void {
    this.listTooltipLock[index] = !this.listTooltipLock[index];
  }

  public openUnblockAnalysisDialog(event, index: number): void {
    event.stopPropagation();
    if (this.isUserOwner(index) && this.analyses[index].isBlocked) {
      const dialogRef = this.dialog.open(UnlockAnalysisComponent);
      const instance = dialogRef.componentInstance;
      instance.analysisName = this.analyses[index].name;
      dialogRef.afterClosed().subscribe(choice => {
        if (choice) {
          this.analyseService.unblockAnalysis(this.analyses[index].id).subscribe(res => {
            this.analyses[index].isBlocked = res.ral_isBlocked;
            this.analyses[index].isAccepted = res.decision.dec_isAccepted;
          });
        }
      });
    }
  }

  private sortAnalysesPerRoles(entryList: Array<AnalysisLight>): Array<AnalysisLight> {
    const currentUser = this.userGlobalService.user;
    let res = [];

    // Get the analyses where the current user is the owner
    const ownerList = this.filterListPerRole(entryList, currentUser, 'owner');
    // Get the analyses where the current user is an admin
    const adminList = this.filterListPerRole(entryList, currentUser, 'ral_admin');
    // Get the analyses where the current user is a contributor
    const contributorList = this.filterListPerRole(entryList, currentUser, 'write');
    // Get the rest of the analyses
    const others = _.sortBy(entryList.filter(ral => !ral.users.some(us => us.guid === currentUser.guid)), ral => ral.name);

    res = res.concat(ownerList, adminList, contributorList, others);

    return res;
  }

  // Returns a new list with only the users who have the entry rol, sorted alphabetically by analysis name
  private filterListPerRole(entryList: Array<AnalysisLight>, currentUser: User, role: string): Array<AnalysisLight> {
    return _.sortBy(entryList.filter(ral => ral.users.some(us => us.guid === currentUser.guid && us.role.rol_code.toLowerCase() === role)),
      ral => ral.name.toLowerCase());
  }

  // Returns a list of user without the owner
  public filterOutOwner(entryList: Array<User>): Array<User> {
    return entryList.filter(user => user.role.rol_code.toLowerCase() !== 'owner');
  }

  public displayUsers(users: Array<User>): boolean {
    return !(users.length < 1);
  }

  // Formats and returns the name of the entry user object
  public getContributorName(contributor: User): string {
    return this.userGlobalService.formatName(contributor.firstName, contributor.lastName);
  }

  // Returns the user with the 'owner' role from a list of users
  public getOwner(array: Array<User>): User {
    return array.filter(u => u.role.rol_code.toLowerCase() === 'owner')[0];
  }

  // Formats and returns the name of the user with the 'owner' role from a list of users
  public getOwnerName(array: Array<User>): string {
    let res = null;
    const owner = this.getOwner(array);
    if (owner) {
      res = this.userGlobalService.formatName(owner.firstName, owner.lastName);
    }
    return res;
  }

  private isUserOwner(index: number): boolean {
    const user = this.analyses[index].users.find(us => us.guid === this.userGlobalService.user.guid && us.role.rol_code.toLowerCase() === 'owner');
    return user !== null && user !== undefined;
  }

  public tooltipText(index: number): string {
    let text: string;
    if (this.isUserOwner(index)) {
      text = 'Débloquer l\'analyse de risque';
    } else {
      text = 'Analyse bloquée';
    }
    return text;
  }

  public triggerDate(index: number) {
    this.listTooltipDate[index] = !this.listTooltipDate[index];
  }

  public getDateDecision(index: number): string {
    const date = new Date(this.analyses[index].decision_date);
    let month = (date.getUTCMonth() + 1).toString(); // months from 1-12
    if (month.length < 2) {
      month = '0' + month;
    }
    let day = date.getUTCDate().toString();
    if (day.length < 2) {
      day = '0' + day;
    }
    const year = date.getUTCFullYear();
    return day + '/' + month + '/' + year;
  }
}
