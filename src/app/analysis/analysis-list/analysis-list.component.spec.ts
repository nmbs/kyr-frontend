import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { AppConfig } from '../../app.config';
import { AnalysisLight } from '../../home/models/analysisLight';
import { UserGlobalService } from 'src/app/services/userGlobal.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AnalysisListComponent } from './analysis-list.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatDialogRef, MatDialog } from '@angular/material';
import { AnalysisService } from '../analysis.service';
import { Analysis } from '../api-models/analysis.model';
import { AnalysisGlobalService } from '../analysisGlobal.service';


const analysesList: Array<AnalysisLight> = [
  {
    id: 'id1',
    name: 'AAAA',
    entity: 'EEEE',
    users: [
      {
        guid: 'userId1',
        email: 'firstname1.lastname1@user1.com',
        firstName: 'firstname1',
        lastName: 'lastname1',
        role: {
          rol_code: 'OWNER',
          rol_description: '',
          rol_id: 'ownerId',
          rol_label: ''
        }
      },
      {
        guid: 'userId2',
        email: 'firstname2.lastname2@user2.com',
        firstName: 'firstname2',
        lastName: 'lastname2',
        role: {
          rol_code: 'RAL_ADMIN',
          rol_description: '',
          rol_id: 'adminId',
          rol_label: ''
        }
      }
    ],
    isBlocked: null,
    isAccepted: null,
    decision_date: '2019-06-27T12:17:42'
  },
  {
    id: 'id2',
    name: 'BBBB',
    entity: 'EEEE',
    users: [
      {
        guid: 'userId1',
        email: 'firstname1.lastname1@user1.com',
        firstName: 'firstname1',
        lastName: 'lastname1',
        role: {
          rol_code: 'RAL_ADMIN',
          rol_description: '',
          rol_id: 'adminId',
          rol_label: ''
        }
      },
      {
        guid: '1234',
        email: 'firstname2.lastname2@user2.com',
        firstName: 'firstname2',
        lastName: 'lastname2',
        role: {
          rol_code: 'OWNER',
          rol_description: '',
          rol_id: 'ownerId',
          rol_label: ''
        }
      }
    ],
    isBlocked: null,
    isAccepted: true,
    decision_date: '2019-06-27T12:17:42'
  },
  {
    id: 'id3',
    name: 'CCCC',
    entity: 'EEEE',
    users: [
      {
        guid: 'userId1',
        email: 'firstname1.lastname1@user1.com',
        firstName: 'firstname1',
        lastName: 'lastname1',
        role: {
          rol_code: 'WRITE',
          rol_description: '',
          rol_id: 'contributorId',
          rol_label: ''
        }
      }
    ],
    isBlocked: null,
    isAccepted: false,
    decision_date: '2019-06-27T12:17:42'
  },
  {
    id: 'id4',
    name: 'DDDD',
    entity: 'EEEE',
    users: [
      {
        guid: 'userId3',
        email: 'firstname3.lastname3@user3.com',
        firstName: 'firstname3',
        lastName: 'lastname3',
        role: {
          rol_code: 'WRITE',
          rol_description: '',
          rol_id: 'contributorId',
          rol_label: ''
        }
      }
    ],
    isBlocked: null,
    isAccepted: null,
    decision_date: '2019-06-27T12:17:42'
  }
];

const mockUserGlobalService = {
  user: {
    guid: '1234'
  },
  formatName: (firstName, lastName) => {

    let name = '';
    if (firstName) {
      name +=  firstName + ' ';
    }

    if (lastName) {
      name +=  lastName;
    }

    return name.trim();
  }
};

const mockAnalysis: Analysis = {
  ral_id: 'string',
  ral_name: 'string',
  ral_created_date: null,
  ral_updated_date: null,
  finalUsers: 'string',
  nbUser: 'string',
  dataSensibility: 'string',
  dataTypes: 'string',
  userAccesses: 'string',
  availabilityWeeks: 'string',
  availabilityDays: 'string',
  traceKeepingLaw: 'string',
  traceKeepingApp: 'string',
  solutionTypes: 'string',
  maxInterruptionTime: 'string',
  impactDataLoss: 'string',
  ral_isBlocked: true,
  features: [
    {
      fet_id: 'string',
      ral_id: 'string',
      fet_label: 'string',
      fet_description: 'string',
      fet_sensitive_nature: 'string',
      fet_created_date: 'string'
    }],
  impacts: null,
  opinion: null,
  decision: null,
  residualRisk: null
};

class MockAnalysisGlobalService {
  analysis: Analysis = mockAnalysis;
}

const matDialogRefSpy = {};
const matDialogSpy = {};

describe('AnalysisListComponent', () => {
  let component: AnalysisListComponent;
  let fixture: ComponentFixture<AnalysisListComponent>;

  beforeEach(async(() => {
    const appConfigSpy = {
      getConfig: () => {
        return { animationAssets: {} };
      }
    };
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        ScrollingModule,
        HttpClientTestingModule
      ],
      declarations: [
        AnalysisListComponent
      ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: AppConfig, useValue: appConfigSpy },
        { provide: UserGlobalService, useValue: mockUserGlobalService },
        { provide: MatDialogRef, useValue: matDialogRefSpy },
        { provide: MatDialog, useValue: matDialogSpy },
        AnalysisService,
        { provide: AnalysisGlobalService, useValue: new MockAnalysisGlobalService() }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalysisListComponent);
    component = fixture.componentInstance;
    component.analyses = analysesList;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Test filterOutOwner', () => {
    it('res should not contains an owner', () => {
      const res = component.filterOutOwner(analysesList[0].users);
      expect(res.some(us => us.role.rol_code.toLowerCase() === 'owner')).toEqual(false);
    });
  });

  describe('Test displayUsers', () => {
    it('res to equal true', () => {
      const res = component.displayUsers(analysesList[0].users);
      expect(res).toEqual(true);
    });
  });

  describe('Test getContributorName', () => {
    it('res to equal Firstname1 Lastname1', () => {
      const res = component.getContributorName(analysesList[0].users[0]);
      expect(res).toEqual('firstname1 lastname1');
    });
  });

  describe('Test getOwner', () => {
    it('res.firstName to equal firstname1', () => {
      const res = component.getOwner(analysesList[0].users);
      expect(res.firstName).toEqual('firstname1');
    });
  });

  describe('Test getOwnerName', () => {
    it('res to equal Firstname1 Lastname1', () => {
      const res = component.getOwnerName(analysesList[0].users);
      expect(res).toEqual('firstname1 lastname1');
    });
  });

  describe('Test getDecisionText', () => {
    it('res to equal En attente when isAccepted is null', () => {
      const res = component.getDecisionText(analysesList[0]);
      expect(res).toEqual('En attente');
    });
    it('res to equal En attente when isAccepted is true', () => {
      const res = component.getDecisionText(analysesList[1]);
      expect(res).toEqual('Validé');
    });
    it('res to equal En attente when isAccepted is false', () => {
      const res = component.getDecisionText(analysesList[2]);
      expect(res).toEqual('Refusé');
    });
  });

  describe('Test getBorderStyle', () => {
    it('res to equal En attente when isAccepted is null', () => {
      const res = component.getBorderStyle(analysesList[0]);
      const rep = { 'color': '#677d91', 'background-color': '#ebeef1' };
      expect(res).toEqual(rep);
    });
    it('res to equal En attente when isAccepted is true', () => {
      const res = component.getBorderStyle(analysesList[1]);
      const rep = {
        'color': '#58be00',
        'background-color': '#e6f8d6'
      };
      expect(res).toEqual(rep);
    });
    it('res to equal En attente when isAccepted is false', () => {
      const res = component.getBorderStyle(analysesList[2]);
      const rep = {'color': '#f30238',
        'background-color': '#fdd6df'};
      expect(res).toEqual(rep);
    });
  });

  describe('get date', () => {
    it('should return date', () => {
      const date = component.getDateDecision(0);
      expect(date).toEqual('27/06/2019');
    });
  });

  describe('tooltip is owner', () => {
    it('should return Analyse bloquée', () => {
      const text = component.tooltipText(0);
      expect(text).toEqual('Analyse bloquée');
    });
    it('should return Débloquer l\'analyse de risque', () => {
      const text = component.tooltipText(1);
      expect(text).toEqual('Débloquer l\'analyse de risque');
    });
  });
});
