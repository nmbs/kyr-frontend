import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpHelper } from '../helpers/http.helper';
import { AnalysisGlobalService } from './analysisGlobal.service';
import { AppConfig } from '../app.config';
import { Analysis } from './api-models/analysis.model';
import { Description } from './models/description.model';
import { isArray } from 'util';
import { QuestionType } from './models/questionType.model';

@Injectable()
export class AnalysisService {

  private _api: string;
  private _urlAnalysis: string;

  constructor(private _http: HttpClient, private appConfig: AppConfig, private analysisGlobalService: AnalysisGlobalService) {
    this._api = appConfig.getConfig().apiAnalysisUrl;
    this._urlAnalysis = this._api + '/analyses';
  }

  // Create a new analysis
  public createAnalysis(analysis: Analysis): Observable<Analysis> {
    return this._http.post<Analysis>(this._urlAnalysis, analysis, { headers: HttpHelper.getHeaders() })
      .pipe(
        map(res => {
          this.analysisGlobalService.analysis = res;
          return res;
        }),
        catchError(HttpHelper.handleError));
  }

  // Get analysis by Id
  public getAnalysis(analysis_id: string): Observable<Analysis> {
    return this._http.get<Analysis>(this._urlAnalysis + '/' + analysis_id, { headers: HttpHelper.getHeaders() })
      .pipe(
        map(res => {
          res.nbUser = (res.nbUser) ? [res.nbUser] : null;
          res.maxInterruptionTime = (res.maxInterruptionTime) ? [res.maxInterruptionTime] : null;
          res.impactDataLoss = (res.impactDataLoss) ? [res.impactDataLoss] : null;
          res.traceKeepingApp = (res.traceKeepingApp) ? [res.traceKeepingApp] : null;
          res.traceKeepingLaw = (res.traceKeepingLaw) ? [res.traceKeepingLaw] : null;
          res.dataSensibility = (res.dataSensibility) ? [res.dataSensibility] : null;
          this.analysisGlobalService.analysis = res;
          return res;
        }),
        catchError(HttpHelper.handleError)
      );
  }

  // Update only one filed of an analysis
  public patchAnalysis(analysis_id: string, body: any, api_path: string): Observable<Analysis> {
    const path = (api_path) ? this._urlAnalysis + '/' + analysis_id + api_path : this._urlAnalysis + '/' + analysis_id;
    this.analysisGlobalService.isSendingDoingQuery = true;

    return this._http.patch<Analysis>(path, body, { headers: HttpHelper.getHeaders() })
      .pipe(
        map(res => {
          this.analysisGlobalService.analysis = res;
          this.analysisGlobalService.isSendingDoingQuery = false;
          return res;
        }),
        catchError(HttpHelper.handleError)
      );
  }

  public getCompletionPercentage(analysis: Analysis, description: Description) {
    let nbQuestion = 0;
    let nbAnswer = 0;

    description.steps.forEach(step => {
      step.questions.forEach(question => {
        if (question.countProgress) {
          if (question.type !== QuestionType.QCM_TOGGLE) {
            nbQuestion++;
            if (analysis[question.propertyName] && !isArray(analysis[question.propertyName])
              || isArray(analysis[question.propertyName]) && analysis[question.propertyName].length > 0) {
              nbAnswer++;
            }
          } else {
            question.questions.forEach(questionToggle => {
              nbQuestion++;
              if (analysis[questionToggle.propertyName] && !isArray(analysis[questionToggle.propertyName])
                || isArray(analysis[questionToggle.propertyName]) && analysis[questionToggle.propertyName].length > 0) {
                nbAnswer++;
              }
            });
          }
        }
      });
    });

    return nbAnswer / nbQuestion * 100;
  }

  public unblockAnalysis(guid: string): Observable<Analysis> {
    return this._http.post<Analysis>(this._urlAnalysis + '/' + guid + '/isBlocked/false', { headers: HttpHelper.getHeaders() })
      .pipe(catchError(HttpHelper.handleError));
  }
}
