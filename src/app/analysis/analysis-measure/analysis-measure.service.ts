import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { AppConfig } from 'src/app/app.config';
import { HttpHelper } from 'src/app/helpers/http.helper';
import { AnalysisMeasure } from '../models/measure/analysisMeasure';
import { Observable } from 'rxjs';
import { AnalysisMeasureGlobalService } from '../analysis-measure-global.service';
import { MeasureState } from '../models/measure/measureState';
import * as _ from 'lodash';

@Injectable()
export class AnalysisMeasureService {
  private _api: string;
  private _urlMeasure: string;
  private _urlMeasureState: string;

  constructor(private _http: HttpClient, appConfig: AppConfig, private analysisMeasureGlobalService: AnalysisMeasureGlobalService) {
    this._api = appConfig.getConfig().apiAnalysisUrl;
    this._urlMeasure = this._api + '/analysisMeasures';
    this._urlMeasureState = this._api + '/MeasureStates';
  }

  // Get all categories
  public getMeasureState() {
    return this._http.get<Array<MeasureState>>(this._urlMeasureState, { headers: HttpHelper.getHeaders() })
    .pipe(
      map(res => {
        return res;
      }),
      catchError(HttpHelper.handleError));
  }

  // Get all measures by Analysis GUID
  public getMeasures(analysis_id: string) {
    return this._http.get<Array<AnalysisMeasure>>(this._urlMeasure + '/analysis/' + analysis_id, { headers: HttpHelper.getHeaders() })
      .pipe(
        map(res => {
          this.analysisMeasureGlobalService.analysisMeasureList = res;
          return res;
        }),
        catchError(HttpHelper.handleError));
  }

  // Delete list measures by id
  public deleteMeasures(analysis_id: string, list_measures_id: Array<string>) {
    return this._http.request<Array<AnalysisMeasure>>('delete', this._urlMeasure + '/analysis/' + analysis_id,
      { body: list_measures_id, headers: HttpHelper.getHeaders() })
      .pipe(
        map(res => {
          this.analysisMeasureGlobalService.analysisMeasureList = this.analysisMeasureGlobalService.analysisMeasureList.filter(
            measure => !list_measures_id.some(id => id === measure.ram_id)
            );
          return res;
        }),
        catchError(HttpHelper.handleError));
  }

  // Create a new measures
  public createMeasures(analysis_id: string, measures_list: Array<AnalysisMeasure>): Observable<Array<AnalysisMeasure>> {
    return this._http.post<Array<AnalysisMeasure>>(this._urlMeasure + '/analysis/' + analysis_id, measures_list, { headers: HttpHelper.getHeaders() })
      .pipe(
        map(res => {
          this.analysisMeasureGlobalService.analysisMeasureList = this.analysisMeasureGlobalService.analysisMeasureList.concat(res);
          return res;
        }),
        catchError(HttpHelper.handleError));
  }

  // Update measures
  public updateMeasures(analysis_id: string, measures_list: Array<AnalysisMeasure>): Observable<Array<AnalysisMeasure>> {
    return this._http.put<Array<AnalysisMeasure>>(this._urlMeasure + '/analysis/' + analysis_id, measures_list, { headers: HttpHelper.getHeaders() })
      .pipe(
        map(res => {
          const analysisMeasureGlobal = _.cloneDeep(this.analysisMeasureGlobalService.analysisMeasureList);
          res.forEach(measure => {
            const i = analysisMeasureGlobal.findIndex(m => m.ram_id === measure.ram_id);
            if (i > -1) {
              analysisMeasureGlobal[i] = measure;
            }
          });
          this.analysisMeasureGlobalService.analysisMeasureList = analysisMeasureGlobal;
          return res;
        }),
        catchError(HttpHelper.handleError));
  }
}
