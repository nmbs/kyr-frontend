import { Component, OnInit } from '@angular/core';
import { AnalysisService } from '../analysis.service';
import { AnalysisMeasureService } from './analysis-measure.service';
import { AnalysisMeasure } from '../models/measure/analysisMeasure';
import { ActivatedRoute, Router } from '@angular/router';
import { DescriptionGlobalService } from '../descriptionGlobal.service';
import { Analysis } from '../api-models/analysis.model';
import { analysisMeasures } from 'src/assets/analysis-measure/measures';
import * as _ from 'lodash';
import { MatDialog } from '@angular/material';
import { AnalysisMeasureGlobalService } from '../analysis-measure-global.service';
import { AddMeasuresComponent } from '../modals/add-measures/add-measures.component';
import html2canvas from '../../../assets/html2canvas/html2canvas.js';
import * as jsPDF from 'jspdf/dist/jspdf.debug.js';
import { Measure } from '../models/measure/measure';
import { UserGlobalService } from 'src/app/services/userGlobal.service';
import { OnboardingMeasuresComponent } from '../modals/onboarding-measures/onboarding-measures.component';

@Component({
  selector: 'app-analysis-measure',
  templateUrl: './analysis-measure.component.html',
  styleUrls: ['./analysis-measure.component.scss'],
  providers: [AnalysisService, AnalysisMeasureService]
})
export class AnalysisMeasureComponent implements OnInit {
  private _analysis_id: string;
  private _users;
  public analysis: Analysis;
  // All measures
  public measures: Array<AnalysisMeasure>;
  // Measure not linked to a feature
  public measuresAnalysis: Array<AnalysisMeasure>;
  // List of group of measure, grouped by features id
  public measuresFeatures = [];
  public checkedMeasures: Array<AnalysisMeasure> = [];
  public nbMeasureSelected = '';
  public analysisMeasures = analysisMeasures;
  public isBottomBarDisplayed = false;
  public cannotBeAdded: Array<AnalysisMeasure> = [];
  public exportingPDF = false;
  private _analysisPath = '/analyses/';
  public isBlocked = false;
  public isOwner = false;
  public onboardingMeasures = this.userGlobalService.user.email + '-onboarding-measures';
  public onboardingMeasuresOwner = this.userGlobalService.user.email + '-onboarding-measures-owner';

  constructor(private readonly router: Router,
    private readonly route: ActivatedRoute,
    private analysisService: AnalysisService,
    private analysisMeasureService: AnalysisMeasureService,
    private descriptionGlobalService: DescriptionGlobalService,
    public dialog: MatDialog,
    private analysisMeasureGlobalService: AnalysisMeasureGlobalService,
    private userGlobalService: UserGlobalService) {
    this.descriptionGlobalService.measures = analysisMeasures;
  }

  ngOnInit() {
    // Get GUID from the route params and get the analysis
    this.route.parent.paramMap.subscribe(params => {
      this._analysis_id = params.get('guid');
      this.analysisService.getAnalysis(this._analysis_id).subscribe(res => {
        this.analysis = res;
        this.setIsOwner();
        this.isBlocked = this.analysis.ral_isBlocked;
        this.getMeasuresAnalyse();
        if ((!this.isOwner && localStorage.getItem(this.onboardingMeasures) === null ||
          localStorage.getItem(this.onboardingMeasures) === 'false') || (this.isOwner && localStorage.getItem(this.onboardingMeasuresOwner) === null ||
            localStorage.getItem(this.onboardingMeasuresOwner) === 'false')) {
          this.openDialog();
        }
      });
    });

    if (this.descriptionGlobalService.measures) {
      this.descriptionGlobalService.measures.isModify = false;
    }
  }

  // Return a feature name from its GUID
  private getName(featureId: string): string {
    let res = this.analysis.features.filter(f => f.fet_id === featureId)[0].fet_label;
    if (res == null || res.length < 1) {
      res = 'Fonctionnalité sans titre';
    }
    return res;
  }

  // Call modal with checked measures
  public modify() {
    const dialogRef = this.dialog.open(AddMeasuresComponent);
    const instance = dialogRef.componentInstance;
    instance.analysisMeasuresToAdd = this.checkedMeasures;
    instance.step = 2;
    instance.modifyMode = true;
    dialogRef.afterClosed().subscribe(measuresToDelete => {
      this.checkedMeasures.forEach(measure => {
        measure.isChecked = false;
      });
      this.hideBottomBar();
    });
  }

  // Update the checked measure list
  public updateCheckedMeasures(measure: AnalysisMeasure) {
    // Try to remove the id from the list
    const checkedMeasure = _.remove(this.checkedMeasures, function (n) {
      return n.ram_id === measure.ram_id;
    });
    // If the returned list is empty, it means no measure has been removed, so it's added in the list
    if (checkedMeasure.length === 0) {
      this.checkedMeasures.push(measure);
    }
    // If there is at least one measure in the checked list, the bottom bar is displayed
    if (this.checkedMeasures.length > 0) {
      this.isBottomBarDisplayed = true;
    } else {
      this.isBottomBarDisplayed = false;
    }
    this.updateNbMeasuresSelected();
  }

  // OnClick button delete, delete all the measure in the checked measure list
  public deleteMeasures() {
    const list_measures_id = this.getListMeasuresId(this.checkedMeasures);
    this.analysisMeasureService.deleteMeasures(this.analysis.ral_id, list_measures_id).subscribe(res => {
      this.hideBottomBar();
    });
  }

  public goToResidualRisksModification() {
    this.router.navigate([this._analysisPath + this.analysis.ral_id + '/residual-risk', { isModify: true }]);
  }

  private getListMeasuresId(list: Array<AnalysisMeasure>) {
    const list_measures_id: string[] = [];
    list.forEach(measure => {
      list_measures_id.push(measure.ram_id);
    });
    return list_measures_id;
  }

  private updateNbMeasuresSelected() {
    const nbCheckedMeasure = this.checkedMeasures.length;
    if (nbCheckedMeasure <= 1) {
      this.nbMeasureSelected = `${nbCheckedMeasure} mesure sélectionnée`;
    } else {
      this.nbMeasureSelected = `${nbCheckedMeasure} mesures sélectionnées`;
    }
  }

  public getMeasuresAnalyse() {
    // Get all measures linked to the Analysis and its features
    this.analysisMeasureService.getMeasures(this._analysis_id).subscribe(resMeasures => {
      this.measures = resMeasures;
      this.analysisMeasureGlobalService.analysisMeasureList = this.measures;
      this.analysisMeasureGlobalService.analysisMeasureListSubject.subscribe(res => {
        this.measures = res;
        if (this.measures) {
          this.updateMeasures();
        }
      });
    });
  }

  public openDialog(): void {
    if (!this.analysis.ral_isBlocked) {
      const dialogRef = this.dialog.open(OnboardingMeasuresComponent);
      dialogRef.afterClosed().subscribe(choice => {
        if (choice) {
          this.goToResidualRisksModification();
        }
      });
    }
  }

  private updateMeasures() {
    // Measures linked to the analysis have no feature ID
    this.measuresAnalysis = _.sortBy(this.measures.filter(m => m.fet_id == null), ma => ma.measure.mea_name);

    this.measuresFeatures = [];

    // Build map of featureid => list measure
    this.analysis.features.forEach(feat => {
      this.measuresFeatures.push({
        'id': feat.fet_id,
        'name': this.getName(feat.fet_id),
        'measures': _.sortBy(this.measures.filter(m => m.fet_id === feat.fet_id), m => m.measure.mea_name)
      });
    });

    this.measuresFeatures = _.sortBy(this.measuresFeatures, 'name');
    this.cannotBeAdded = this.measures;
  }

  private hideBottomBar() {
    this.isBottomBarDisplayed = false;
    this.checkedMeasures = [];
    this.updateNbMeasuresSelected();
  }

  public getLitteralCost(value: number): string {
    let res = '';

    switch (value) {
      case 1: {
        res = 'Economique';
        break;
      }
      case 2: {
        res = 'Couteux';
        break;
      }
      case 3: {
        res = 'Très couteux';
        break;
      }
      default: {
        res = 'Gratuit';
        break;
      }
    }
    return res;
  }

  public getLitteralImpacts(obj: AnalysisMeasure) {
    const litteralImpactList =
      ['la confidentialité', 'la disponibilité', 'l\'intégrité', 'la preuve'];
    const pivot = [];

    if (obj.ram_isConfidenciality) {
      pivot.push(litteralImpactList[0]);
    }
    if (obj.ram_isDisponibility) {
      pivot.push(litteralImpactList[1]);
    }
    if (obj.ram_isIntegrity) {
      pivot.push(litteralImpactList[2]);
    }
    if (obj.ram_isProof) {
      pivot.push(litteralImpactList[3]);
    }
    const arrayLenght = pivot.length;
    let i = 1;
    let res = 'Impacte ';
    pivot.forEach(item => {
      if (i !== 1 && i < arrayLenght) {
        res += ', ' + item;
      } else if (i !== 1 && i === arrayLenght) {
        res += ' et ' + item;
      } else {
        res += item;
      }
      i++;
    });
    if (pivot.length === 0) {
      res = null;
    }
    return res;
  }

  public exportPDF() {
    // exportingPDF display the html that will be selected and imported into the PDF document
    // Even when true, the HTML is invisible because displayed out of the page in position absolute
    // and in top and left position greater than the size of the page
    if (!this.exportingPDF) {
      this.exportingPDF = true;
      // Adding promise and html2canvas to window in order for the html2canvas and jsPDF libs to work
      (<any>window).html2canvas = html2canvas;
      (<any>window).promise = Promise;
      // Creating the base blank PDF
      const doc = new jsPDF({
        unit: 'pt',
        format: [595.28, 841.89],
        orientation: 'portrait'
      });
      // Defining the height of an A4 page (841.89 * 2)
      const a4HeightPixels = 1684;
      // Set timeout is nescessary in order for the PDF obj to be created before the generation begins
      setTimeout(() => {
        // Below is the calculus of the page breaks
        // Init the base height to 60 to compensate the base margin-top that will be applied at hte beginning of the first page
        let height = 60;
        // Selecting the html to export
        const elementToExport = document.getElementById('pdfContainer').children;
        // Creating an empty HTML with pdfContainer id to then fill it with each html element of the selected full html
        // and integrate page breaks when the height of an A4 page is reached
        const htmlToConvert: HTMLElement = <HTMLElement>document.getElementById('pdfContainer').cloneNode(false);

        Array.from(elementToExport).forEach(node => {
          // Adding the height of the current element to the global height
          height += node.clientHeight;

          if (height < a4HeightPixels) {
            // Adding element to thehtml to export, because the total height is lesser than the A4 page height
            htmlToConvert.appendChild(<HTMLElement>node.cloneNode(true));
          } else {

            // Removing the parent element height from the total height, we are now processing its children elements
            height -= node.clientHeight;
            // Calculating the margin nescessary to begin at the next A4 page and adding 60px of top margin to it
            const margin = (a4HeightPixels - height).toString() + 'px';
            // Updating the global height
            height = node.clientHeight;
            const pageBreak = document.createElement('DIV');
            // Applying the page break
            pageBreak.setAttribute(
              'style', 'margin-bottom: ' + margin + ';');
            htmlToConvert.appendChild(pageBreak);
            // Adding the element to the final HTML
            htmlToConvert.appendChild(<HTMLElement>node.cloneNode(true));
          }
        }
        );
        // Setting a series of CSS attribute to the global HTML to export
        htmlToConvert.setAttribute(
          'style', 'position:relative; right:0; top:0; bottom:0; font-family:"Helvetica"; margin: 60px 20px; text-align: left;');

        // use of the JSPDF lib to export the pdf
        doc.html(htmlToConvert, {
          // The scale, width and height is important in order to have the same render on any screen the export is made
          html2canvas: {
            scale: 0.5,
            width: '100%',
            height: '1684px'
          },
          callback: (pdf: jsPDF) => {
            // Formatting generation date and time
            const date = new Date();
            const dd = String(date.getDate()).padStart(2, '0');
            const mm = String(date.getMonth() + 1).padStart(2, '0');
            const hh = String(date.getHours()).padStart(2, '0') + 'h' + String(date.getMinutes()).padStart(2, '0');
            const yyyy = date.getFullYear();

            // Replacing spaces with dash in the analysis name
            const generationTime = dd + '-' + mm + '-' + yyyy + '-' + hh;
            const formatedAnalysisName = this.analysis.ral_name.split(' ').join('-');

            // Building the PDF name and extension
            const titlePDF = 'Comment-Me-Protège-t-on' + '_' + formatedAnalysisName + '_' + generationTime + '.pdf';
            doc.save(titlePDF);
            this.exportingPDF = false;
          },
        });
      }, 1000);
    }
  }

  // Check if the user is an owner and subscribe to users global
  private setIsOwner() {
    if (this.userGlobalService.users) {
      this._users = this.userGlobalService.users;
      const owner = this._users.find(usr => usr.role && usr.role.rol_code.toLowerCase() === 'owner');
      if (owner) {
        this.isOwner = owner.guid === this.userGlobalService.user.guid;
      }
    }
    this.userGlobalService.usersSubject.subscribe(users => {
      this._users = users;
      const owner = users.find(usr => usr.role && usr.role.rol_code.toLowerCase() === 'owner');
      if (owner) {
        this.isOwner = owner.guid === this.userGlobalService.user.guid;
      }
    });
  }
}
