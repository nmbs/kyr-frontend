import { Injectable } from '@angular/core';
import { AppConfig } from 'src/app/app.config';
import { HttpClient } from '@angular/common/http';
import { HttpHelper } from 'src/app/helpers/http.helper';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { MeasureCategory } from '../models/measure/measureCategory';

@Injectable()
export class MeasureService {
  private _api: string;
  private _urlMeasure: string;

  constructor(private _http: HttpClient, appConfig: AppConfig) {
    this._api = appConfig.getConfig().apiAnalysisUrl;
    this._urlMeasure = this._api + '/measurecategories';
  }

  // Get all measures categories by Analysis GUID
  public getMeasuresCategories() {
    return this._http.get<Array<MeasureCategory>>(this._urlMeasure, { headers: HttpHelper.getHeaders() })
      .pipe(
        map(res => {
          return res;
        }),
        catchError(HttpHelper.handleError));
  }

}
