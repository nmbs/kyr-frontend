import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalysisMeasureComponent } from './analysis-measure.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppConfig } from 'src/app/app.config';
import { AnalysisGlobalService } from '../analysisGlobal.service';
import { AnalysisService } from '../analysis.service';
import { AnalysisMeasureService } from './analysis-measure.service';

import { DescriptionGlobalService } from '../descriptionGlobal.service';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { UserGlobalService } from 'src/app/services/userGlobal.service';
import { MatDialog } from '@angular/material';
import { AnalysisMeasureGlobalService } from '../analysis-measure-global.service';

const mockAnalysis = {
  ral_id: 'string',
  ral_name: 'string',
  ral_created_date: null,
  ral_updated_date: null,
  finalUsers: 'string',
  nbUser: 'string',
  dataSensibility: 'string',
  dataTypes: 'string',
  userAccesses: 'string',
  availabilityWeeks: 'string',
  availabilityDays: 'string',
  traceKeepingLaw: 'string',
  traceKeepingApp: 'string',
  solutionTypes: 'string',
  maxInterruptionTime: 'string',
  impactDataLoss: 'string',
  ral_isBlocked: false,
  features: [
    {
      fet_id: 'myguid',
      ral_id: 'string',
      fet_created_date: 'string',
      fet_label: 'featureName',
      fet_description: 'string',
      fet_sensitive_nature: 'string'
    }],
  impacts: null,
  opinion: null,
  decision: null,
  residualRisk: null
};

const mockMeasureAnalysis = {
  ram_id: 'string',
  ral_id: 'string',
  fet_id: 'myguid',
  ram_isDisponibility: true,
  ram_isIntegrity: true,
  ram_isConfidenciality: true,
  ram_isProof: true,
  ram_cost: 5,
  ram_dt: 'string',
  ral_isBlocked: false,
  measureState: {
    mst_id: 'string',
    mst_name: 'string',
    mst_code: 'string',
  },
  measure: {
    mea_id: 'string',
    mea_name: 'string',
    mea_description: 'string',
    mea_isCustom: false,
    measureCategory: {
      mca_id: 'string',
      mca_name: 'string',
      mca_logo: 'string',
    }
  },
  isChecked: true
};

const measure1 = Object.assign({}, mockMeasureAnalysis);
const measure2 = Object.assign({}, mockMeasureAnalysis);
const measure3 = Object.assign({}, mockMeasureAnalysis);
measure1.ram_id = 'measure1';
measure1.fet_id = null;
measure2.ram_id = 'measure2';
measure2.fet_id = null;
measure3.ram_id = 'measure3';

const userGlobalServiceMock = {
  user: {
    email: 'emial@email.com',
    guid: '1'
  },
  users: [
    {
      email: 'emial@email.com',
      guid: '1',
      firstName: 'Romain'
    },
    {
      email: 'email@email.com',
      guid: '2'
    }
  ]
};

class MockAnalysisMeasureService {
  getMeasures(analysis_id: string) {
    const listMeasures = [];
    listMeasures.push(measure1);
    listMeasures.push(measure2);
    listMeasures.push(measure3);
    return of(listMeasures);
  }

  deleteMeasures(analysis_id: string, list_measures_id: Array<string>) {
    return of([]);
  }
}

describe('AnalysisMeasureComponent', () => {
  let component: AnalysisMeasureComponent;
  let fixture: ComponentFixture<AnalysisMeasureComponent>;

  beforeEach(async(() => {
    const appConfigSpy = jasmine.createSpyObj('AppConfig', {
      getConfig: function () {
        return '';
      }
    });
    const matDialogSpy = {};

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [AnalysisMeasureComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [AnalysisGlobalService, AnalysisService, DescriptionGlobalService, UserService, { provide: UserGlobalService, useValue: userGlobalServiceMock },
        AnalysisMeasureGlobalService,
        { provide: AnalysisMeasureService, useValue: new MockAnalysisMeasureService() },
        { provide: AppConfig, useValue: appConfigSpy },
        { provide: MatDialog, useValue: matDialogSpy },
        { provide: ActivatedRoute, useValue: { parent: { paramMap: of() }, paramMap: of() } }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalysisMeasureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.analysis = mockAnalysis;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Test update checked list', () => {
    it('Checked if an id is add when list is empty, and then remove it', () => {
      const mockMeasure123 = Object.assign({}, mockMeasureAnalysis);
      mockMeasure123.ram_id = 'Test123';
      const mockMeasure124 = Object.assign({}, mockMeasureAnalysis);
      mockMeasure124.ram_id = 'Test124';
      const mockMeasure125 = Object.assign({}, mockMeasureAnalysis);
      mockMeasure125.ram_id = 'Test125';
      component.updateCheckedMeasures(mockMeasure123);
      component.updateCheckedMeasures(mockMeasure124);
      component.updateCheckedMeasures(mockMeasure125);
      expect(component.checkedMeasures).toEqual([mockMeasure123, mockMeasure124, mockMeasure125]);
      component.updateCheckedMeasures(mockMeasure123);
      expect(component.checkedMeasures).toEqual([mockMeasure124, mockMeasure125]);
      expect(component.nbMeasureSelected).toEqual('2 mesures sélectionnées');
      component.updateCheckedMeasures(mockMeasure124);
      expect(component.checkedMeasures).toEqual([mockMeasure125]);
      expect(component.nbMeasureSelected).toEqual('1 mesure sélectionnée');
    });
  });
});
