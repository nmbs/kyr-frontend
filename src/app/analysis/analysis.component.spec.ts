import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalysisComponent } from './analysis.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AnalysisService } from './analysis.service';
import { AppConfig } from '../app.config';
import { RouterTestingModule } from '@angular/router/testing';
import { AnalysisGlobalService } from '../analysis/analysisGlobal.service';
import { UserGlobalService } from '../services/userGlobal.service';
import { UserService } from '../services/user.service';
import { Observable, of } from 'rxjs';
import { User } from '../models/user';

const mockUserMapping = { usr_id: 'guid', usr_email: 'email', usr_firstname: 'firstName', usr_lastname: 'lastName' };

class MockUserService {
  getUser(email: string): Observable<any> {
    return of(mockUserMapping);
  }

  getUsersByAnalyseId(analyse_id): Observable<Array<any>> {
    return of([mockUserMapping]);
  }
}

describe('AnalysisComponent', () => {
  let component: AnalysisComponent;
  let fixture: ComponentFixture<AnalysisComponent>;

  beforeEach(async(() => {
    const appConfigSpy = jasmine.createSpyObj('AppConfig', {
      getConfig: function () {
        return '';
      }
    });
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [AnalysisComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [AnalysisService, AnalysisGlobalService, { provide: AppConfig, useValue: appConfigSpy },
        UserGlobalService,
        { provide: UserService, useValue: new MockUserService() }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
