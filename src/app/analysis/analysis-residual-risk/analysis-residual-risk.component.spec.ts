import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalysisResidualRiskComponent } from './analysis-residual-risk.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfig } from 'src/app/app.config';
import { AnalysisGlobalService } from '../analysisGlobal.service';
import { Analysis } from '../api-models/analysis.model';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { UserGlobalService } from 'src/app/services/userGlobal.service';
import { DescriptionGlobalService } from '../descriptionGlobal.service';
import { MatDialog } from '@angular/material';

const mockAnalysis: Analysis = {
  ral_isBlocked: false,
  ral_id: 'string',
  ral_name: 'string',
  ral_created_date: null,
  ral_updated_date: null,
  finalUsers: 'string',
  nbUser: 'string',
  dataSensibility: 'string',
  dataTypes: 'string',
  userAccesses: 'string',
  availabilityWeeks: 'string',
  availabilityDays: 'string',
  traceKeepingLaw: 'string',
  traceKeepingApp: 'string',
  solutionTypes: 'string',
  maxInterruptionTime: 'string',
  impactDataLoss: 'string',
  features: [
    {
      fet_id: 'string',
      ral_id: 'string',
      fet_label: 'string',
      fet_description: 'string',
      fet_sensitive_nature: 'string',
      fet_created_date: 'string'
    }],
  impacts: null,
  opinion: {
    opi_id: 'string',
    opi_description: 'string',
    opi_date: new Date(),
    usr_id: '3'
  },
  decision: {
    dec_id: 'string',
    dec_description: 'string',
    dec_date: new Date(),
    dec_isAccepted: true,
    usr_id: '3'
  },
  residualRisk: {
    rri_id: 'string',
    rri_description: 'string',
    rri_date: new Date(),
    usr_id: '3'
  }

};

const userGlobalServiceMock = {
  user: {
    email: 'emial@email.com',
    guid: '1'
  },
  users: [
    {
      email: 'emial@email.com',
      guid: '1',
      firstName: 'Romain'
    },
    {
      email: 'email@email.com',
      guid: '2'
    }
  ]
};

class MockAnalysisGlobalService {
  analysis: Analysis = mockAnalysis;
}

describe('AnalysisResidualRiskComponent', () => {
  let component: AnalysisResidualRiskComponent;
  let fixture: ComponentFixture<AnalysisResidualRiskComponent>;

  beforeEach(async(() => {
    const appConfigSpy = jasmine.createSpyObj('AppConfig', {
      getConfig: function () {
        return '';
      }
    });
    const matDialogRefSpy = {
      close: function() {}
    };

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [AnalysisResidualRiskComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        DescriptionGlobalService,
        { provide: AnalysisGlobalService, useValue: new MockAnalysisGlobalService() },
        { provide: AppConfig, useValue: appConfigSpy },
        { provide: ActivatedRoute, useValue: { parent: { paramMap: of() }, paramMap: of() } },
        { provide: UserGlobalService, useValue: userGlobalServiceMock },
        { provide: MatDialog, useValue: matDialogRefSpy }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalysisResidualRiskComponent);
    component = fixture.componentInstance;
    component.analysis = mockAnalysis;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Test init values', () => {
    it('Init values and check if var are set', () => {
      component.initValues();
      expect(component.rrText).toEqual('string');
      expect(component.decisionText).toEqual('string');
      expect(component.opinionText).toEqual('string');
      expect(component.rrUserName).toEqual(null);
      expect(component.opinionUserName).toEqual(null);
      expect(component.decisionUserName).toEqual(null);
      expect(component.isValidate).toBeTruthy();
      expect(component.tagText).toEqual(component.choicesValidation[0].ch_id);
      expect(component.choicesValidation[0].checked).toBeTruthy();
    });
  });

  describe('Test disable buttons', () => {
    it('Check rr button', () => {
      expect(component.isResidualRiskButtonDisable()).toBeTruthy();
      component.rrText = 'false';
      expect(component.isResidualRiskButtonDisable()).toBeFalsy();
      component.rrText = null;
      expect(component.isResidualRiskButtonDisable()).toBeTruthy();
    });

    it('Check opinion button', () => {
      expect(component.isOpinionButtonDisable()).toBeTruthy();
      component.opinionText = 'false';
      expect(component.isOpinionButtonDisable()).toBeFalsy();
      component.opinionText = null;
      expect(component.isOpinionButtonDisable()).toBeTruthy();
    });

    it('Check decision button', () => {
      component.decisionText = 'string';
      component.isValidate = true;
      expect(component.isDecisionButtonDisable()).toBeFalsy();
      component.decisionText = null;
      expect(component.isDecisionButtonDisable()).toBeFalsy();
      component.isValidate = null;
      expect(component.isDecisionButtonDisable()).toBeTruthy();
    });
  });

  describe('Test selectDecision', () => {
    it('Test nominal case', () => {
      component.isValidate = true;
      const choice = {
        cho_name: 'Acceptation',
        checked: true,
        ch_id: 'Validé'
      };
      component.selectDecision(true, choice);
      expect(component.isValidate).toEqual(null);
      expect(component.tagText).toEqual(null);
      choice.checked = false;
      component.selectDecision(true, choice);
      expect(component.isValidate).toEqual(true);
      expect(component.tagText).toEqual(choice.ch_id);
    });
  });
});
