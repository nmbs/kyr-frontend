import { Component, OnInit } from '@angular/core';
import { Analysis } from '../api-models/analysis.model';
import { ActivatedRoute, Router } from '@angular/router';
import { AnalysisService } from '../analysis.service';
import { analysisResidualRisk } from 'src/assets/analysis-residual-risk/residual-risk';
import { UserGlobalService } from 'src/app/services/userGlobal.service';
import { DescriptionGlobalService } from '../descriptionGlobal.service';
import { MatDialog } from '@angular/material';
import { ValidateResidualRisksComponent } from '../modals/validate-residual-risks/validate-residual-risks.component';

import html2canvas from '../../../assets/html2canvas/html2canvas.js';
import * as jsPDF from 'jspdf/dist/jspdf.debug.js';
import { OnboardingRisksComponent } from '../modals/onboarding-risks/onboarding-risks.component';

@Component({
  selector: 'app-analysis-residual-risk',
  templateUrl: './analysis-residual-risk.component.html',
  styleUrls: ['./analysis-residual-risk.component.scss'],
  providers: [AnalysisService]
})
export class AnalysisResidualRiskComponent implements OnInit {
  private _analysis_id: string;
  public analysis: Analysis;
  public analysisRiskResidual = analysisResidualRisk;
  public exportingPDF = false;

  public isOwner = false;

  choicesValidation: Array<any> = [
    {
      cho_name: 'Acceptation',
      checked: false,
      ch_id: 'Validé'
    },
    {
      cho_name: 'Refus',
      checked: false,
      ch_id: 'Refusé'
    }
  ];

  public isValidate = null;
  public tagText = null;

  public rrText: string;
  public rrUserName: string;
  public opinionText: string;
  public opinionUserName: string;
  public decisionText: string;
  public decisionUserName: string;
  public onboardingRisks = this.userGlobalService.user.email + '-onboarding-risks';
  public onboardingRisksOwner = this.userGlobalService.user.email + '-onboarding-risks-owner';

  private _users;

  constructor(private readonly route: ActivatedRoute,
    private analysisService: AnalysisService,
    private readonly router: Router,
    private userGlobalService: UserGlobalService,
    private descriptionGlobalService: DescriptionGlobalService,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.route.parent.paramMap.subscribe(parentParams => {
      this._analysis_id = parentParams.get('guid');
      this.analysisService.getAnalysis(this._analysis_id).subscribe(res => {
        this.analysis = res;

        this.setIsOwner();
        this.initValues();

        if (this.route.paramMap) {
          this.route.paramMap.subscribe(params => {
            const isModifyParamm = params.get('isModify');
            if (isModifyParamm) {
              if (isModifyParamm === 'false' &&
                ((!this.isOwner && !this.analysis.residualRisk) || (this.isOwner && !this.analysis.decision))) {
                this.changeMode(true);
              } else {
                if (isModifyParamm === 'true' && this.analysis.ral_isBlocked) {
                  this.analysisRiskResidual.isModify = false;
                } else {
                  this.analysisRiskResidual.isModify = isModifyParamm === 'true';
                }
              }
            }
          });
        }
        if ((!this.isOwner && localStorage.getItem(this.onboardingRisks) === null ||
          localStorage.getItem(this.onboardingRisks) === 'false') ||
          (this.analysis && this.analysis.residualRisk && this.isOwner && localStorage.getItem(this.onboardingRisksOwner) === null ||
            localStorage.getItem(this.onboardingRisksOwner) === 'false')) {
          this.openDialog();
        }
      });
    });

    this.descriptionGlobalService.risks = analysisResidualRisk;
  }

  // Init residual, opinion and decision text and userName (if exist)
  public initValues() {
    if (this.analysis.residualRisk) {
      this.rrText = this.analysis.residualRisk.rri_description;
      this.rrUserName = this.getUserName(this.analysis.residualRisk.usr_id);
    }
    if (this.analysis.opinion) {
      this.opinionText = this.analysis.opinion.opi_description;
      this.opinionUserName = this.getUserName(this.analysis.opinion.usr_id);
    }
    if (this.analysis.decision) {
      this.decisionText = this.analysis.decision.dec_description;
      this.decisionUserName = this.getUserName(this.analysis.decision.usr_id);
      if (this.analysis.decision.dec_isAccepted !== null) {
        this.isValidate = this.analysis.decision.dec_isAccepted;
        // Check the correct choice depending on the decision
        if (this.analysis.decision.dec_isAccepted) {
          this.tagText = this.choicesValidation[0].ch_id;
          this.choicesValidation[0].checked = true;
        } else {
          this.tagText = this.choicesValidation[1].ch_id;
          this.choicesValidation[1].checked = true;
        }
      }
    }
  }


  // Residual risk text part
  public onKeyResidualRisk(text) {
    this.rrText = text;
  }

  // Update (or create) risk analysis on click validate
  public updateRiskAnalysis() {
    let residualRisk;
    if (this.analysis.residualRisk) {
      residualRisk = Object.assign({}, this.analysis.residualRisk);
      residualRisk.rri_description = this.rrText;
      residualRisk.rri_date = new Date();
      residualRisk.usr_id = this.userGlobalService.user.guid;
    } else {
      residualRisk = {
        rri_id: null,
        rri_description: this.rrText,
        rri_date: new Date(),
        usr_id: this.userGlobalService.user.guid
      };
    }
    this.rrUserName = this.getUserName(residualRisk.usr_id);
    this.analysisService.patchAnalysis(this._analysis_id, residualRisk, '/residualrisk').subscribe(res => {
      this.analysis = res;
    });
  }

  public isResidualRiskButtonDisable() {
    // Residual risk is disable if there is no text or no changes in the text
    return !this.rrText || (this.analysis.residualRisk && this.rrText === this.analysis.residualRisk.rri_description);
  }

  // Opinion text part
  public onKeyOpinion(text) {
    this.opinionText = text;
  }

  // Update (or create) an opinion
  public updateOpinion() {
    let opinion;
    if (this.analysis.opinion) {
      opinion = Object.assign({}, this.analysis.opinion);
      opinion.opi_description = this.opinionText;
      opinion.opi_date = new Date();
      opinion.usr_id = this.userGlobalService.user.guid;
    } else {
      opinion = {
        opi_id: null,
        opi_description: this.opinionText,
        opi_date: new Date(),
        usr_id: this.userGlobalService.user.guid
      };
    }
    this.opinionUserName = this.getUserName(opinion.usr_id);
    this.analysisService.patchAnalysis(this._analysis_id, opinion, '/opinion').subscribe(res => {
      this.analysis = res;
    });
  }

  public isOpinionButtonDisable() {
    // Residual risk is disable if there is no text or no changes in the text
    return !this.opinionText || (this.analysis.opinion && this.opinionText === this.analysis.opinion.opi_description);
  }

  public getOpinionText() {
    // Default text for opinion if no text for the recap
    return this.opinionText ? this.opinionText : 'Pas d\'avis laissé.';
  }

  // Validate residual risk section
  public onKeyDecision(text) {
    this.decisionText = text;
  }

  public openModalValidateResidualRisk() {
    // Only open the modal if the decision is validation
    if (this.isValidate) {
      const dialogRef = this.dialog.open(ValidateResidualRisksComponent);
      const instance = dialogRef.componentInstance;
      instance.displayOpinionWarning = !this.analysis.opinion;
      dialogRef.afterClosed().subscribe(isValidate => {
        if (isValidate) {
          this.validateResidualRisk();
        }
      });
    } else {
      this.validateResidualRisk();
    }
  }

  // Create or update the decision
  public validateResidualRisk() {
    let decision;
    // If there no decision (validate/deny), the date is null
    const date = this.isValidate !== null ? new Date() : null;
    if (this.analysis.decision) {
      decision = Object.assign({}, this.analysis.decision);
      decision.dec_description = this.decisionText;
      decision.dec_date = date;
      decision.dec_isAccepted = this.isValidate;
      decision.usr_id = this.userGlobalService.user.guid;
    } else {
      decision = {
        dec_id: null,
        dec_description: this.decisionText,
        dec_date: date,
        dec_isAccepted: this.isValidate,
        usr_id: this.userGlobalService.user.guid
      };
    }
    this.decisionUserName = this.getUserName(decision.usr_id);
    this.analysisService.patchAnalysis(this._analysis_id, decision, '/decision').subscribe(res => {
      this.analysis = res;
      // Change to recap mode
      this.changeMode(false);
    });
  }

  // Change decision on click on tiles
  public selectDecision(decision, choice) {
    // check if the choice was already checked
    const isChoiceAlreadyChecked = choice.checked;
    this.choicesValidation.forEach(element => {
      element.checked = false;
    });

    // If already checked, empty everything
    if (isChoiceAlreadyChecked) {
      this.isValidate = null;
      this.tagText = null;
    } else {
      this.isValidate = decision;
      this.tagText = choice.ch_id;
      choice.checked = !choice.checked;
    }
  }

  public getDecisionText() {
    return this.decisionText ? this.decisionText : this.isValidate !== null ?
      'Pas de commentaire lié à cette décision' : 'Aucune décision n\'a été prise pour le moment';
  }

  // Decision button is disable if there is no comment or checked decision
  public isDecisionButtonDisable() {
    return !this.decisionText && this.isValidate === null;
  }

  private scrollToContainer(elId) {
    const el = document.getElementById(elId);
    const textAreaList = el.getElementsByClassName('customInput');
    setTimeout(() => {
      setTimeout(() => {
        el.scrollIntoView({ behavior: 'smooth' });
      }, 0);
      const textArea = <HTMLInputElement>textAreaList[0];
      textArea.focus();
    }, 0);
  }

  // Change mode by changing isModify param (and scroll to el if given)
  public changeMode(modifyMode, el?) {
    const currentUrl = this.router.url.split(';');
    this.router.navigate([currentUrl[0], { isModify: modifyMode }]);
    if (el) {
      this.scrollToContainer(el);
    } else {
      this.scrollToTop();
    }
    this.analysisRiskResidual.isModify = modifyMode;
  }

  private scrollToTop() {
    window.scrollTo(0, 0);
    window.scroll(0, 0);
    scroll(0, 0);
  }

  // Get formatted name of the id
  private getUserName(userId) {
    if (this._users) {
      const userResidualRisk = this.userGlobalService.users.find(usr => usr.guid === userId);
      return userResidualRisk ?
        this.userGlobalService.formatName(userResidualRisk.firstName, userResidualRisk.lastName) : null;
    }
    return null;
  }

  // Check if the user is an owner and subscribe to users global
  private setIsOwner() {
    if (this.userGlobalService.users) {
      this._users = this.userGlobalService.users;
      const owner = this._users.find(usr => usr.role && usr.role.rol_code.toLowerCase() === 'owner');
      if (owner) {
        this.isOwner = owner.guid === this.userGlobalService.user.guid;
      }
    }
    this.userGlobalService.usersSubject.subscribe(users => {
      this._users = users;
      const owner = users.find(usr => usr.role && usr.role.rol_code.toLowerCase() === 'owner');
      if (owner) {
        this.isOwner = owner.guid === this.userGlobalService.user.guid;
      }
    });
  }

  public exportPDF() {
    // exportingPDF display the html that will be selected and imported into the PDF document
    // Even when true, the HTML is invisible because displayed out of the page in position absolute
    // and in top and left position greater than the size of the page
    if (!this.exportingPDF) {
      this.exportingPDF = true;
      // Adding promise and html2canvas to window in order for the html2canvas and jsPDF libs to work
      (<any>window).html2canvas = html2canvas;
      (<any>window).promise = Promise;
      // Creating the base blank PDF
      const doc = new jsPDF({
        unit: 'pt',
        format: [595.28, 841.89],
        orientation: 'portrait'
      });
      // Defining the height of an A4 page (841.89 * 2)
      const a4HeightPixels = 1684;
      // Set timeout is nescessary in order for the PDF obj to be created before the generation begins
      setTimeout(() => {
        // Below is the calculus of the page breaks
        // Init the base height to 60 to compensate the base margin-top that will be applied at hte beginning of the first page
        let height = 60;
        // Selecting the html to export
        const elementToExport = document.getElementById('pdfContainer').children;
        // Creating an empty HTML with pdfContainer id to then fill it with each html element of the selected full html
        // and integrate page breaks when the height of an A4 page is reached
        const htmlToConvert: HTMLElement = <HTMLElement>document.getElementById('pdfContainer').cloneNode(false);

        Array.from(elementToExport).forEach(node => {
          // Adding the height of the current element to the global height
          height += node.clientHeight;

          if (height < a4HeightPixels) {
            // Adding element to thehtml to export, because the total height is lesser than the A4 page height
            htmlToConvert.appendChild(<HTMLElement>node.cloneNode(true));
          } else {

            // Removing the parent element height from the total height, we are now processing its children elements
            height -= node.clientHeight;
            // Calculating the margin nescessary to begin at the next A4 page and adding 60px of top margin to it
            const margin = ((a4HeightPixels - height) + 60).toString() + 'px';
            // Updating the global height
            height = node.clientHeight;
            const pageBreak = document.createElement('DIV');
            // Applying the page break
            pageBreak.setAttribute(
              'style', 'margin-bottom: ' + margin + ';');
            htmlToConvert.appendChild(pageBreak);
            // Adding the element to the final HTML
            htmlToConvert.appendChild(<HTMLElement>node.cloneNode(true));
          }
        }
        );
        // Setting a series of CSS attribute to the global HTML to export
        htmlToConvert.setAttribute(
          'style', 'position:relative; right:0; top:0; bottom:0; font-family:"Helvetica"; margin: 60px 20px; padding-right: 40px; text-align: left;');

        // use of the JSPDF lib to export the pdf
        doc.html(htmlToConvert, {
          // The scale, width and height is important in order to have the same render on any screen the export is made
          html2canvas: {
            scale: 0.5,
            width: '1190px',
            height: '1684px'
          },
          callback: (pdf: jsPDF) => {
            // Formatting generation date and time
            const date = new Date();
            const dd = String(date.getDate()).padStart(2, '0');
            const mm = String(date.getMonth() + 1).padStart(2, '0');
            const hh = String(date.getHours()).padStart(2, '0') + 'h' + String(date.getMinutes()).padStart(2, '0');
            const yyyy = date.getFullYear();

            // Replacing spaces with dash in the analysis name
            const generationTime = dd + '-' + mm + '-' + yyyy + '-' + hh;
            const formatedAnalysisName = this.analysis.ral_name.split(' ').join('-');

            // Building the PDF name and extension
            const titlePDF = 'Ma-Prise-De-Risque' + '_' + formatedAnalysisName + '_' + generationTime + '.pdf';
            doc.save(titlePDF);
            this.exportingPDF = false;
          },
        });
      }, 1000);
    }
  }

  public openDialog(): void {
    if (!this.analysis.ral_isBlocked) {
      this.dialog.open(OnboardingRisksComponent);
    }
  }
}
