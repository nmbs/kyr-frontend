import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {AnalysisService} from '../analysis.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfig } from '../../app.config';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { DescriptionGlobalService } from '../descriptionGlobal.service';
import { AnalysisGlobalService } from '../../analysis/analysisGlobal.service';

import { AnalysisImpactComponent } from './analysis-impact.component';

describe('AnalysisImpactComponent', () => {
  let component: AnalysisImpactComponent;
  let fixture: ComponentFixture<AnalysisImpactComponent>;

  beforeEach(async(() => {
    const appConfigSpy = jasmine.createSpyObj('AppConfig', {
      getConfig: function () {
        return '';
      }
    });
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [ AnalysisImpactComponent ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [AnalysisService, DescriptionGlobalService, AnalysisGlobalService,
        { provide: AppConfig, useValue: appConfigSpy },
        { provide: ActivatedRoute, useValue: { parent : { paramMap: of()}}}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalysisImpactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
