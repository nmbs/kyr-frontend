import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { analysisImpact } from '../../../assets/analysis-impact-form/impacts';
import { analysisProtectedAssets } from '../../../assets/analysis-protected-assets/protected-assets';

import { Analysis } from '../api-models/analysis.model';
import { analysisImpactReadOnly } from '../../../assets/analysis-impact-form/impact-read-only';
import { AnalysisService } from '../analysis.service';
import { DescriptionGlobalService } from '../descriptionGlobal.service';
import { Step } from '../models/step.model';
import { analysisFeatureSummary } from 'src/assets/analysis-feature-form/features-summary';
import { AnalysisFeatureService } from '../analysis-feature/analysis-feature.service';
import * as _ from 'lodash';
import html2canvas from '../../../assets/html2canvas/html2canvas.js';
import * as jsPDF from 'jspdf/dist/jspdf.debug.js';

@Component({
  selector: 'app-analysis-impact',
  templateUrl: './analysis-impact.component.html',
  styleUrls: ['./analysis-impact.component.scss'],
  providers: [AnalysisService]
})
export class AnalysisImpactComponent implements OnInit, AfterViewInit {

  public analysisImpact = analysisImpact;
  public analysisImpactReadOnly = analysisImpactReadOnly;
  public analysisFeaturesSummary = analysisFeatureSummary;
  public featureSummary = Array<Step>();
  public exportingPDF = false;
  private _analysis_id: string;
  public analysis: Analysis;

  public isBlocked = false;

  private _analysisPath = '/analyses/';

  constructor(private readonly route: ActivatedRoute,
    private analysisService: AnalysisService,
    private readonly router: Router,
    private descriptionGlobalService: DescriptionGlobalService,
    private analysisFeatureService: AnalysisFeatureService) {
    this.analysisImpact.isModify = false;
    this.descriptionGlobalService.impact = analysisImpact;
    this.descriptionGlobalService.protectedAssets = analysisProtectedAssets;

  }

  ngOnInit() {
    // Get the Analysis Obj according to the GUID present in the URL
    this.route.parent.paramMap.subscribe(params => {
      this._analysis_id = params.get('guid');
      this.analysisService.getAnalysis(this._analysis_id).subscribe(res => {
        this.analysis = res;
        this.isBlocked = this.analysis.ral_isBlocked;
        if (this.isBlocked) {
          this.analysisImpact.isModify = false;
        }
        this.analysis.impacts.forEach(impact => {
          this.analysis[impact.imp.imp_label + '_impact'] = impact.ira_comment;
        });
        this.initFeatureSteps();
        // Get isModify param from the route params
        if (this.route.paramMap) {
          if (!this.isBlocked) {
            this.route.paramMap.subscribe(parameters => {
              const isModifyParamm = parameters.get('isModify');
              if (isModifyParamm) {
                this.analysisImpact.isModify = parameters.get('isModify') === 'true';
              }
              this.updateSidebar();
            });
          } else {
            this.descriptionGlobalService.protectedAssets.isModify = false;
          }
        }
      });
    });
  }

  ngAfterViewInit() {
    this.scrollToTop();
  }

  // Scroll to the previous block
  public prev(index: number) {
    this.scrollToForm(index - 1);
  }

  // Scroll to the next block
  public next(index: number) {
    this.scrollToForm(index + 1);
  }

  public goToRecap() {
    this.analysisImpact.isModify = false;
    this.scrollToTop();

  }

  public modify(isModify: boolean, step: Step) {
    this.analysisImpact.isModify = isModify;
    const currentUrl = this.router.url.split(';');
    this.router.navigate([currentUrl[0], { isModify: true }]);

    // Updating the SideBar
    this.descriptionGlobalService.impact.isModify = this.analysisImpact.isModify;
    this.descriptionGlobalService.protectedAssets.isModify = this.analysisImpact.isModify;

    // Focusing on the first impact
    this.scrollToTop();
  }

  public modifyFeature(param) {
    this.router.navigate(['/analyses/' + this._analysis_id + '/feature', { anchor: param }]);
  }

  // We update the sidebar according to the isModify query param
  // If we are in modification mode, then the sidebar should only display the chapter
  private updateSidebar() {
    // Impact is always the first step of this chapter
    this.descriptionGlobalService.protectedAssets.steps[0].isActive = true;
    this.descriptionGlobalService.stepIndex = 0;
    this.descriptionGlobalService.protectedAssets.isModify = this.analysisImpact.isModify;
    // Indicates that the 'feature' step should be inactive in the sidebar
    this.descriptionGlobalService.protectedAssets.steps[1].isActive = false;
  }

  private scrollToTop() {
    window.scrollTo(0, 0);
    window.scroll(0, 0);
    scroll(0, 0);
    // Focus on the first impact
    this.analysisImpact.steps[0].isActive = true;
  }

  private scrollToForm(index) {
    const element_to_scroll_to = document.getElementById('form' + index);
    if (element_to_scroll_to != null) {
      element_to_scroll_to.scrollIntoView({ behavior: 'smooth' });
    }
  }

  public goTo(path: string) {
    this.router.navigate([this._analysisPath + this.analysis.ral_id + path]);
  }

  // Init the feature steps list with the features data from the analysis
  public initFeatureSteps() {
    this.analysis.features = _.sortBy(this.analysis.features, 'fet_label');

    this.analysis.features.forEach(feature => {
      const featureSummaryStep = this.analysisFeaturesSummary.steps[0];
      const descriptionQuestion = Object.assign({}, featureSummaryStep.questions[0]);
      const sensibleNatureQuestion = Object.assign({}, featureSummaryStep.questions[1]);
      descriptionQuestion.value = feature.fet_description;
      sensibleNatureQuestion.value = feature.fet_sensitive_nature;
      const titleFeature = (feature.fet_label.replace(/\s/g, '').length === 0 || feature.fet_label === null) ?
        featureSummaryStep.title : feature.fet_label;
      // Iterate through the features and fill the model to push a new
      this.featureSummary.push({
        title: titleFeature,
        inputTitle: featureSummaryStep.inputTitle,
        isTitleFocused: featureSummaryStep.isTitleFocused,
        placeHolderTitle: featureSummaryStep.placeHolderTitle,
        errorText: featureSummaryStep.errorText,
        isErrorDisplayed: false,
        isActive: true,
        paging: featureSummaryStep.paging,
        apiPath: featureSummaryStep.apiPath,
        id: feature.fet_id,
        deleteButton: true,
        questions: [descriptionQuestion, sensibleNatureQuestion],
        maxCharacterTitle: featureSummaryStep.maxCharacterTitle
      });
    },
      error => {
      });
  }

  // Delete the feature in the db + in the steps
  public deleteFeature(step: Step) {
    const feature = this.analysis.features.find(f => f.fet_id === step.id);
    if (feature !== null) {
      this.analysisFeatureService.deleteFeature(feature).subscribe(analysis => {
        this.analysis = analysis;
        this.removeStep(step);
      });
    }
  }

  // Find and remove the step in the list
  private removeStep(step: Step) {
    const indexFeat = this.featureSummary.findIndex(f => f === step);
    if (indexFeat > -1) {
      this.featureSummary.splice(indexFeat, 1);
    }
  }

  public exportPDF() {
    // exportingPDF display the html that will be selected and imported into the PDF document
    // Even when true, the HTML is invisible because displayed out of the page in position absolute
    // and in top and left position greater than the size of the page
    if (!this.exportingPDF) {
      this.exportingPDF = true;
      // Adding promise and html2canvas to window in order for the html2canvas and jsPDF libs to work
      (<any>window).html2canvas = html2canvas;
      (<any>window).promise = Promise;
      // Creating the base blank PDF
      const doc = new jsPDF({
        unit: 'pt',
        format: [595.28, 841.89],
        orientation: 'portrait'
      });
      // Defining the height of an A4 page (841.89 * 2)
      const a4HeightPixels = 1684;
      // Set timeout is nescessary in order for the PDF obj to be created before the generation begins
      setTimeout(() => {
        // Below is the calculus of the page breaks
        // Init the base height to 60 to compensate the base margin-top that will be applied at hte beginning of the first page
        let height = 60;
        // Selecting the html to export
        const elementToExport = document.getElementById('pdfContainer').children;
        // Creating an empty HTML with pdfContainer id to then fill it with each html element of the selected full html
        // and integrate page breaks when the height of an A4 page is reached
        const htmlToConvert: HTMLElement = <HTMLElement>document.getElementById('pdfContainer').cloneNode(false);

        Array.from(elementToExport).forEach(node => {
          // Adding the height of the current element to the global height
          height += node.clientHeight;

          if (height < a4HeightPixels) {
            // Adding element to thehtml to export, because the total height is lesser than the A4 page height
            htmlToConvert.appendChild(<HTMLElement>node.cloneNode(true));
          } else {
            // Removing the parent element height from the total height, we are now processing its children elements
            height -= node.clientHeight;
            // Calculating the margin nescessary to begin at the next A4 page and adding 60px of top margin to it
            const margin = (a4HeightPixels - height).toString() + 'px';
            const pageBreak = document.createElement('DIV');
            // Applying the page break
            pageBreak.setAttribute(
              'style', 'margin-bottom: ' + margin + ';');
            // Adding the page break to the HTML
            htmlToConvert.appendChild(pageBreak);
            // Adding the element to the final HTML
            htmlToConvert.appendChild(<HTMLElement>node.cloneNode(true));
            // Updating the global height
            height = node.clientHeight;
          }
        }
        );
        // Setting a series of CSS attribute to the global HTML to export
        htmlToConvert.setAttribute(
          'style', 'position:relative; right:0; top:0; bottom:0; font-family:"Helvetica"; margin: 60px 30px; margin-bottom: 100px;text-align: left;');

        // use of the JSPDF lib to export the pdf
        doc.html(htmlToConvert, {
          // The scale, width and height is important in order to have the same render on any screen the export is made
          html2canvas: {
            scale: 0.5,
            width: '100%',
            height: '1684px'
          },
          callback: (pdf: jsPDF) => {
            // Formatting generation date and time
            const date = new Date();
            const dd = String(date.getDate()).padStart(2, '0');
            const mm = String(date.getMonth() + 1).padStart(2, '0');
            const hh = String(date.getHours()).padStart(2, '0') + 'h' + String(date.getMinutes()).padStart(2, '0');
            const yyyy = date.getFullYear();

            // Replacing spaces with dash in the analysis name
            const generationTime = dd + '-' + mm + '-' + yyyy + '-' + hh;
            const formatedAnalysisName = this.analysis.ral_name.split(' ').join('-');

            // Building the PDF name and extension
            const titlePDF = 'Ce-Que-Je-Veux-Protéger' + '_' + formatedAnalysisName + '_' + generationTime + '.pdf';
            doc.save(titlePDF);
            this.exportingPDF = false;
          },
        });
      }, 1000);


    }
  }
}
