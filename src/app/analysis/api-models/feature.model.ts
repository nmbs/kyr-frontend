export interface Feature {
    fet_id: string;
    ral_id: string;
    fet_created_date: string;
    fet_label: string;
    fet_description: string;
    fet_sensitive_nature: string;
}
