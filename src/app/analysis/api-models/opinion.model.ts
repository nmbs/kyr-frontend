export interface Opinion {
    opi_id: string;
    opi_description: string;
    opi_date: Date;
    usr_id: string;
}
