export interface ResidualRisk {
    rri_id: string;
    rri_description: string;
    rri_date: Date;
    usr_id: string;
}
