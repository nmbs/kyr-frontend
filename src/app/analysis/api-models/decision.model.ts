export interface Decision {
    dec_id: string;
    dec_description: string;
    dec_date: Date;
    dec_isAccepted: boolean;
    usr_id: string;
}
