import { Feature } from './feature.model';
import { Impact } from '../models/impact.model';
import { Opinion } from './opinion.model';
import { Decision } from './decision.model';
import { ResidualRisk } from './residual-risk.model';

export interface Analysis {
    ral_id: string;
    ral_name: string;
    ral_created_date: Date;
    ral_updated_date: Date;
    ral_isBlocked: boolean;
    finalUsers: any;
    nbUser: any;
    dataSensibility: any;
    dataTypes: any;
    userAccesses: any;
    availabilityWeeks: any;
    availabilityDays: any;
    traceKeepingLaw: any;
    traceKeepingApp: any;
    solutionTypes: any;
    maxInterruptionTime: any;
    impactDataLoss: any;
    features: Array<Feature>;
    impacts: Array<Impact>;
    opinion: Opinion;
    decision: Decision;
    residualRisk: ResidualRisk;
}
