import { TestBed } from '@angular/core/testing';

import { HttpHelper } from './http.helper';
import { HttpHeaders } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

describe('HttpHelper', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [RouterTestingModule]
  }));

  it('should be created', () => {
    const service: HttpHelper = TestBed.get(HttpHelper);
    expect(service).toBeTruthy();
  });

  describe('getHeaders', () => {
    beforeEach(() => {
    });

    it('should choice active icon', () => {
      const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json').set('Cache-Control', 'no-cache').set('Pragma', 'no-cache');
      expect(HttpHelper.getHeaders()).toEqual(headers);
    });
  });

});
