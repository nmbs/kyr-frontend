
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

/**
 * This interceptor serves the purpose of fixing some cache problems that can occur on IE11
 */
@Injectable()
export class CacheInterceptor implements HttpInterceptor {

    constructor(protected router: Router) {

    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const httpRequest = req;
        // Make changes to the request here if needed
        return next.handle(httpRequest).pipe(tap((event: HttpEvent<any>) => {
        }, (err: any) => {
            if (err instanceof HttpErrorResponse && err.status !== 401) {
                // Handle error codes
                const errorStatus = err.status === 404 ? err.status : 'default';
                this.router.navigate(['/error', errorStatus]);
            }
        }));
    }
}
