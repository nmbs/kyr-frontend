import { Injectable } from '@angular/core';
import { HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class  HttpHelper {

  constructor() { }

  // Get header with good params
  public static getHeaders() {
    const contentType = 'application/json';
    const headers = new HttpHeaders().set('Content-Type', contentType).set('Accept', contentType).set('Cache-Control', 'no-cache').set('Pragma', 'no-cache');
    return headers;
  }

  public static handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `API returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}
