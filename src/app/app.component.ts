import { Component, OnInit } from '@angular/core';
import { UserGlobalService } from './services/userGlobal.service';
import { User } from './models/user';
import { UserService } from './services/user.service';
import { KeycloakService } from 'keycloak-angular';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  private name: string;
  private _user: User;

  constructor(private userGlobalService: UserGlobalService,
    private userService: UserService,
    private keycloakService: KeycloakService) {
  }

  public get user(): User {
    return this._user;
  }

  ngOnInit() {
    this.keycloakService.loadUserProfile().then(loadUserProfile => {
      this.userService.getUser(loadUserProfile.email).subscribe(res => {
        this.userGlobalService.user = res;
      });
      this.userGlobalService.userSubject.subscribe((res: User) => {
        this._user = res;
        this.name = this.userGlobalService.formatName(this.userGlobalService.user.firstName, this.userGlobalService.user.lastName);
      });
    });
  }
}
