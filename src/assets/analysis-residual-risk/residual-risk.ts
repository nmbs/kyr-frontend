import { Description } from '../../app/analysis/models/description.model';

// This class is meant to describe the chapter containing the residual risk page
export let analysisResidualRisk: Description = {
    title: 'Ma prise de risque',
    isModify: false,
    iconPath: '/assets/img/nav/risks',
    steps: []
};
