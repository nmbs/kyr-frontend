
import { RolesPolicies } from 'src/app/models/rolePolicies';

export let rolePolicies: RolesPolicies = {
    policies: [
        {
            role_code: 'OWNER',
            add_users: true,
            read_only: false
        },
        {
            role_code: 'RAL_ADMIN',
            add_users: true,
            read_only: false
        },
        {
            role_code: 'WRITE',
            add_users: false,
            read_only: false
        },
        {
            role_code: 'READER',
            add_users: false,
            read_only: false
        }
    ]

};
