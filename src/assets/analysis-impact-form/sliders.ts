import { Slider } from '../../app/analysis/models/slider.model';

export let sliderDICP: Array<Slider> = [
    { title: item => `${item} est lent ou inaccessible ?`, subtitle: () => null, valueToChange: 'd', startingValue: 0 },
    { title: item => `${item} utilise ou produit des données érronées/incomplètes ?`, subtitle: () => null, valueToChange: 'i', startingValue: 0 },
    { title: () => 'Les données sont accessibles au delà des personnes autorisées ?', subtitle: () => null, valueToChange: 'c', startingValue: 0 },
    { title: () => 'Il n\'est pas possible de justifier ou suivre certaines opérations ?', subtitle: null, valueToChange: 'p', startingValue: 0 }
];
