import { sliderSummaryDICP } from './sliders-summary';
import { QuestionType } from 'src/app/analysis/models/questionType.model';

export let analysisImpactReadOnly = {
    title: item => 'Mes exigences',
    isModify: false,
    iconPath: '/assets/img/nav/protection',
    questions: [{
        type : QuestionType.QCM_SLIDER,
        sliders: sliderSummaryDICP,
        isOptional: false
    },
    {
        title: 'Commentaire financier :',
        type: QuestionType.TEXT_INPUT,
        apiPath: '/impacts',
        propertyName: 'financial_impact',
        nbPlaceholder: 1,
        isPatch: false,
        isOptional: true
    },
    {
        title: 'Commentaire image :',
        type: QuestionType.TEXT_INPUT,
        apiPath: '/impacts',
        propertyName: 'image_impact',
        nbPlaceholder: 1,
        isPatch: false,
        isOptional: true
    }
    ]
};
