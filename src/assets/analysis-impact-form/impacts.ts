import { Description } from '../../app/analysis/models/description.model';
import { QuestionType } from '../../app/analysis/models/questionType.model';
import { sliderDICP } from './sliders';

export let analysisImpact: Description = {
    title: 'Ce que je veux protéger',
    isModify: false,
    iconPath: '/assets/img/nav/protection',
    steps: [
        {
            title: 'Impact financier',
            inputTitle: false,
            isTitleFocused: false,
            isErrorDisplayed: false,
            isActive: true,
            apiPath: null,
            deleteButton: false,
            questions: [
                {
                    title: 'Quel serait l’impact financier si...',
                    type: QuestionType.QCM_SLIDER,
                    apiPath: '/impacts',
                    propertyName: 'financial_impact',
                    nbPlaceholder: 0,
                    sliders: sliderDICP,
                    isPatch: true,
                    isOptional: false
                },
                {
                    title: 'Précisez l’impact financier :',
                    subTitle: () => '(Facultatif)',
                    type: QuestionType.TEXT_INPUT,
                    apiPath: '/impacts',
                    propertyName: 'financial_impact',
                    nbPlaceholder: 1,
                    isPatch: false,
                    isOptional: false
                }
            ]
        },
        {
            title: 'Impact d\'image',
            inputTitle: false,
            isTitleFocused: false,
            isErrorDisplayed: false,
            isActive: false,
            apiPath: null,
            deleteButton: false,
            questions: [
                {
                    title: 'Quel serait l’impact si...',
                    type: QuestionType.QCM_SLIDER,
                    apiPath: '/impacts',
                    propertyName: 'image_impact',
                    nbPlaceholder: 0,
                    sliders: sliderDICP,
                    isPatch: true,
                    isOptional: false
                },
                {
                    title: 'Précisez l’impact sur l’image :',
                    subTitle: () => '(Facultatif)',
                    type: QuestionType.TEXT_INPUT,
                    apiPath: '/impacts',
                    propertyName: 'image_impact',
                    nbPlaceholder: 1,
                    isPatch: false,
                    isOptional: false
                }
            ]
        }
    ]
};
