import { Slider } from '../../app/analysis/models/slider.model';

export let sliderSummaryDICP: Array<Slider> = [
    { title: () => 'Disponibilité', subtitle: item => `Si ${item} n’était plus accessible ?`, valueToChange: 'd', startingValue: 0 },
    { title: () => 'Intégrité', subtitle: item => `Si les données de ${item} étaient erronées ?`, valueToChange: 'i', startingValue: 0 },
    { title: () => 'Confidentialité', subtitle: item => `Si les données de ${item} étaient rendues publiques ?`, valueToChange: 'c', startingValue: 0 },
    { title: () => 'Preuve', subtitle: item => `Si vous ne pouviez plus retracer l’historique de ${item} ?`, valueToChange: 'p', startingValue: 0 }
];

export let sliderSummaryDICPSmall: Array<Slider> = [
    { title: () => 'Disponibilité', subtitle: item => `Si ${item} n’était plus accessible ?`, valueToChange: 'd', startingValue: 0 },
    { title: () => 'Intégrité', subtitle: item => `Si les données de ${item} étaient erronées ?`, valueToChange: 'i', startingValue: 0 },
    { title: () => 'Confidentialité', subtitle: item => `Si les données de ${item} étaient rendues publiques ?`, valueToChange: 'c', startingValue: 0 },
    { title: () => 'Preuve', subtitle: item => `Si vous ne pouviez plus retracer l’historique de ${item} ?`, valueToChange: 'p', startingValue: 0 }
];
