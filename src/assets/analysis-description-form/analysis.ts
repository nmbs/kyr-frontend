import { Description } from '../../app/analysis/models/description.model';
import { QuestionType } from '../../app/analysis/models/questionType.model';

export let analysisDescription: Description = {
    title: 'Je raconte mon histoire',
    isModify: false,
    iconPath: '/assets/img/nav/history',
    steps: [
        {
            title: 'Pourquoi',
            inputTitle: false,
            isTitleFocused: false,
            isErrorDisplayed: false,
            isActive: false,
            deleteButton: false,
            questions: [
                {
                    title: (item) => `Expliquez-nous en quoi consiste ${item}`,
                    recapTitle: (item) => `${item} consiste en :`,
                    subTitle: () => 'Indiquez en quelques mots les enjeux de la solution',
                    type: QuestionType.TEXT_INPUT,
                    propertyName: 'ral_goal',
                    placeholder: 'Exemple : opportunité business, obligation réglementaire, opportunité stratégique...',
                    nbPlaceholder: 1,
                    isPatch: true,
                    isOptional: false,
                    countProgress: true
                }
            ]
        }, {
            title: 'Qui',
            inputTitle: false,
            isTitleFocused: false,
            isErrorDisplayed: false,
            isActive: false,
            deleteButton: false,
            questions: [
                {
                    title: (item) => `Parlez-nous des utilisateurs de ${item}. Sont-ils`,
                    recapTitle: (item) => `Les utilisateurs de ${item} sont :`,
                    subTitle: () => 'Plusieurs réponses possibles',
                    type: QuestionType.QCM_MULTIPLE_CHOICE_HOR,
                    apiPath: '/finalusers',
                    propertyName: 'finalUsers',
                    nbPlaceholder: 3,
                    isPatch: true,
                    isOptional: false,
                    countProgress: true
                },
                {
                    title: 'Connaissez-vous déjà le nombre d’utilisateurs ?',
                    recapTitle: (item) => `Le nombre d’utilisateur à utiliser ${item} sera :`,
                    type: QuestionType.QCM_VER,
                    apiPath: '/NbUsers',
                    propertyName: 'nbUser',
                    nbPlaceholder: 4,
                    isPatch: true,
                    isOptional: false,
                    countProgress: true
                },  {
                    title: 'Dans le cas où des données à caractère personnel sont demandées aux utilisateurs, sont-elles',
                    recapTitle: `Les données à caractère personnel demandées aux utilisateurs sont :`,
                    type: QuestionType.QCM_VER,
                    apiPath: '/datasensibilities',
                    propertyName: 'dataSensibility',
                    nbPlaceholder: 3,
                    isPatch: true,
                    isOptional: false,
                    countProgress: true
                },  {
                    title: 'Savez-vous quels autres types de données pourront être collectées et manipulées ?',
                    recapTitle: `Les autres types de données qui pourront être collectées et manipulées seront :`,
                    type: QuestionType.QCM_MULTIPLE_CHOICE_HOR,
                    apiPath: '/dataTypes',
                    propertyName: 'dataTypes',
                    nbPlaceholder: 6,
                    isPatch: true,
                    isOptional: false,
                    countProgress: true
                }, {
                    title: 'Précisez les types de données',
                    recapTitle: `Précisions sur les autres types de données :`,
                    subTitle: () => 'Facultatif',
                    type: QuestionType.TEXT_INPUT,
                    propertyName: 'ral_dataSensibility',
                    nbPlaceholder: 1,
                    isPatch: true,
                    isOptional: false,
                    countProgress: false
                }, {
                    title: (item) => `Les utilisateurs accèderont à ${item} depuis`,
                    recapTitle: (item) => `Les utilisateurs accèderont à ${item} depuis :`,
                    type: QuestionType.QCM_MULTIPLE_CHOICE_HOR,
                    apiPath: '/useraccesses',
                    propertyName: 'userAccesses',
                    nbPlaceholder: 3,
                    isPatch: true,
                    isOptional: false,
                    countProgress: true
                }, {
                    title: 'Précisez le lieu :',
                    recapTitle: `Précisions sur le lieu : `,
                    subTitle: () => 'Facultatif',
                    type: QuestionType.TEXT_INPUT_SMALL,
                    placeholder: 'Exemple : Chine, Suisse, DOM-TOM...',
                    propertyName: 'ral_localisation',
                    nbPlaceholder: 1,
                    isPatch: true,
                    isOptional: false,
                    countProgress: false
                }, {
                    toggleLabel: (item) => `Il est primordial que ${item} soit accessible 7j/7 H24`,
                    recapTitle: (item) => `Il n’est pas primordial que ${item} soit accessible 7j/7 H24 :`,
                    type: QuestionType.QCM_TOGGLE,
                    propertyName: 'ral_alwaysAccessible',
                    nbPlaceholder: 1,
                    isPatch: true,
                    isOptional: false,
                    questions: [
                        {
                            title: 'Indiquez dans ce cas quels sont les moments pendant lesquels il doit être accessible',
                            recapTitle: (item) => `Moments où ${item} doit être accessible :`,
                            subTitle: () => 'Plusieurs réponses possibles',
                            type: QuestionType.QCM_MULTIPLE_CHOICE_HOR_THIN,
                            apiPath: '/availabilityweeks',
                            propertyName: 'availabilityWeeks',
                            nbPlaceholder: 7,
                            isPatch: true,
                            isOptional: false,
                            countProgress: true
                        }, {
                            type: QuestionType.QCM_MULTIPLE_CHOICE_HOR,
                            apiPath: '/availabilitydays',
                            propertyName: 'availabilityDays',
                            nbPlaceholder: 2,
                            isPatch: true,
                            isOptional: false,
                            countProgress: true
                        }, {
                            title: (item) => `Précisions concernant la disponibilité de ${item} :`,
                            recapTitle: (item) => `Précisions sur la disponibilité de ${item} :`,
                            subTitle: () => 'Facultatif',
                            type: QuestionType.TEXT_INPUT,
                            propertyName: 'ral_availability',
                            nbPlaceholder: 1,
                            isPatch: true,
                            isOptional: false,
                            countProgress: false
                        }
                    ]
                }
            ]
        }, {
            title: 'Quoi',
            inputTitle: false,
            isTitleFocused: false,
            isErrorDisplayed: false,
            isActive: false,
            deleteButton: false,
            questions: [
                {
                    title: (item) => `Dans certains cas de figures les réglementations nous imposent de conserver des traces (ex : Signature électronique, RGPD…)
                     Est-ce le cas pour ${item} ?`,
                    recapTitle: (item) => `La réglementation impose-t-elle de garder des traces de ${item} ?`,
                    type: QuestionType.QCM_VER,
                    apiPath: '/tracekeepinglaws',
                    propertyName: 'traceKeepingLaw',
                    nbPlaceholder: 3,
                    isPatch: true,
                    isOptional: false,
                    countProgress: true
                }, {
                    title: (item) =>  `J'ai besoin de conserver des traces pour le bon fonctionnement de ${item} et en cas de contestation (ex : réquisition judiciaire)`,
                    recapTitle: (item) =>  `Des traces de ${item} doivent être gardées pour son bon fonctionnement ou en cas de contestation :`,
                    type: QuestionType.QCM_VER,
                    apiPath: '/tracekeepingapps',
                    propertyName: 'traceKeepingApp',
                    nbPlaceholder: 3,
                    isPatch: true,
                    isOptional: false,
                    countProgress: true
                }, {
                    title: 'Précisez :',
                    recapTitle: (item) => `Précisions sur les traces à garder de ${item} :`,
                    type: QuestionType.TEXT_INPUT,
                    propertyName: 'ral_traceKeeping',
                    nbPlaceholder: 1,
                    isPatch: true,
                    isOptional: false,
                    countProgress: false
                }
            ]
        }, {
            title: 'Comment',
            inputTitle: false,
            isTitleFocused: false,
            isErrorDisplayed: false,
            isActive: false,
            deleteButton: false,
            questions: [
                {
                    title: (item) => `Un type de solution est-il envisagé pour ${item} ?`,
                    recapTitle: (item) => `Type de solution envisagé pour ${item} :`,
                    type: QuestionType.QCM_VER,
                    apiPath: '/solutiontypes',
                    propertyName: 'solutionTypes',
                    nbPlaceholder: 4,
                    isPatch: true,
                    isOptional: false,
                    countProgress: true
                }, {
                    title: 'Précisez la solution externe :',
                    recapTitle: `Précisions sur la solution externe :`,
                    type: QuestionType.TEXT_INPUT,
                    propertyName: 'ral_solutionType',
                    nbPlaceholder: 1,
                    isPatch: true,
                    isOptional: false,
                    countProgress: false
                }
            ]
        }, {
            title: 'Et si…',
            inputTitle: false,
            isTitleFocused: false,
            isErrorDisplayed: false,
            isActive: false,
            deleteButton: false,
            questions: [
                {
                    title: 'Si un sinistre majeur intervenait, quel serait le Délai d’Interruption Maximal Admissible pour l’activité métier ?',
                    recapTitle: `Délai d’Interruption Maximal Admissible en cas de sinistre majeur :`,
                    type: QuestionType.QCM_HOR_THIN,
                    apiPath: '/maxinterruptiontimes',
                    propertyName: 'maxInterruptionTime',
                    nbPlaceholder: 5,
                    isPatch: true,
                    isOptional: false,
                    countProgress: true
                }, {
                    title: 'De même, quelles seraient les conséquences pour votre activité si les données étaient totalement perdues ?',
                    recapTitle: `Conséquence pour l’activité en cas de perte de données :`,
                    type: QuestionType.QCM_VER,
                    apiPath: '/impactdatalosses',
                    propertyName: 'impactDataLoss',
                    nbPlaceholder: 6,
                    isPatch: true,
                    isOptional: false,
                    countProgress: true
                }
            ]
        }, {
            title: 'Et pour finir…',
            inputTitle: false,
            isTitleFocused: false,
            isErrorDisplayed: false,
            isActive: false,
            deleteButton: false,
            questions: [
                {
                    title: 'Compléments d’information :',
                    recapTitle: `Compléments d’information :`,
                    subTitle: () => 'Si vous souhaitez ajouter quelque chose que nous n’avons pas abordé ensemble, c’est à vous',
                    type: QuestionType.TEXT_INPUT,
                    propertyName: 'ral_otherInfo',
                    nbPlaceholder: 1,
                    isPatch: true,
                    isOptional: false,
                    countProgress: false
                }
            ]
        }
    ]
};
