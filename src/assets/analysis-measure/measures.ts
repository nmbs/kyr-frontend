import { Description } from '../../app/analysis/models/description.model';

// This class is meant to describe the chapter containing the measure page
export let analysisMeasures: Description = {
    title: 'Comment me protège-t-on ?',
    isModify: true,
    iconPath: '/assets/img/nav/measures',
    steps: []
};
