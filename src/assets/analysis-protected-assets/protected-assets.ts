import { Description } from '../../app/analysis/models/description.model';

// This class is meant to describe the chapter containing the Impact page and the Feature page
// Impact and Feature are 2 separate component, but should be displayed in the same chapter in the sidebar
// The 'route' param indicates that we should navigate to the said component, as opposed to scroll to a part of the page, which is the normal behaviour of the sidebar
export let analysisProtectedAssets: Description = {
    title: 'Ce que je veux protéger',
    isModify: false,
    iconPath: '/assets/img/nav/protection',
    steps: [
        {
            title: 'Mes exigences',
            subtitle: null,
            inputTitle: false,
            maxCharacterTitle: null,
            isTitleFocused: false,
            placeHolderTitle: null,
            errorText: null,
            isErrorDisplayed: null,
            isActive: true,
            route: 'impact',
            paging: null,
            apiPath: null,
            id: null,
            deleteButton: null,
            questions: [
                {
                    title: null,
                    subTitle: null,
                    toggleLabel: null,
                    type: null,
                    apiPath: null,
                    propertyName: null,
                    nbPlaceholder: null,
                    sliders: null,
                    value: null,
                    isPatch: null,
                    isOptional: false
                }
            ]
        },
        {
            title: 'Mes fonctionnalités sensibles',
            subtitle: null,
            inputTitle: false,
            maxCharacterTitle: null,
            isTitleFocused: false,
            placeHolderTitle: null,
            errorText: null,
            isErrorDisplayed: null,
            isActive: true,
            route: 'feature',
            paging: null,
            apiPath: null,
            id: null,
            deleteButton: null,
            questions: [
                {
                    title: null,
                    subTitle: null,
                    toggleLabel: null,
                    type: null,
                    apiPath: null,
                    propertyName: null,
                    nbPlaceholder: null,
                    sliders: null,
                    value: null,
                    isPatch: null,
                    isOptional: false
                }
            ]
        }
    ]
};
