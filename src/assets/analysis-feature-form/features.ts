import { Description } from '../../app/analysis/models/description.model';
import { QuestionType } from '../../app/analysis/models/questionType.model';

export let AnalysisFeature: Description = {
    title: 'Mes fonctionnalités sensibles',
    isModify: true,
    iconPath: '/assets/img/nav/protection',
    steps: [
        {
            title: null,
            inputTitle: true,
            maxCharacterTitle: 36,
            isTitleFocused: false,
            placeHolderTitle: 'Titre de la fonctionnalité',
            errorText: 'Vous devez obligatoirement remplir le titre pour créer d\'autres fonctionnalités',
            isErrorDisplayed: true,
            isActive: true,
            apiPath: (featureId: string) => `/features/${featureId}`,
            paging: paging => `Fonctionnalité  ${paging}`,
            deleteButton: true,
            questions: [
                {
                    title: 'Description :',
                    subTitle: () => 'Ajoutez des commentaires sur cette fonctionnalité (facultatif)',
                    type: QuestionType.TEXT_INPUT,
                    propertyName: 'fet_description',
                    nbPlaceholder: 1,
                    isPatch: true,
                    isOptional: false
                },
                {
                    title: 'Caractère sensible :',
                    subTitle: () => 'Expliquez-nous en quoi cette fonctionnalité est sensible (facultatif)',
                    type: QuestionType.TEXT_INPUT,
                    propertyName: 'fet_sensitive_nature',
                    nbPlaceholder: 1,
                    isPatch: true,
                    isOptional: false
                }
            ]
        }
    ]
};
