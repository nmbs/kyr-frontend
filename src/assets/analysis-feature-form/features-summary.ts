import { Description } from '../../app/analysis/models/description.model';
import { QuestionType } from '../../app/analysis/models/questionType.model';

export let analysisFeatureSummary: Description = {
    title: null,
    isModify: false,
    iconPath: '/assets/img/nav/protection',
    steps: [
        {
            title: 'Fonctionnalité sans titre',
            inputTitle: false,
            isTitleFocused: false,
            placeHolderTitle: null,
            errorText: null,
            isErrorDisplayed: false,
            isActive: true,
            paging: null,
            deleteButton: true,
            maxCharacterTitle: null,
            questions: [
                {
                    title: 'Description :',
                    subTitle: null,
                    type: QuestionType.TEXT_INPUT,
                    propertyName: 'fet_description',
                    nbPlaceholder: 1,
                    isPatch: true,
                    isOptional: false
                },
                {
                    title: 'Caractère sensible :',
                    subTitle: null,
                    type: QuestionType.TEXT_INPUT,
                    propertyName: 'fet_sensitive_nature',
                    nbPlaceholder: 1,
                    isPatch: true,
                    isOptional: false
                }
            ]
        }
    ]
};
