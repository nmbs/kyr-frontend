# KyrFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test-headless` to execute the unit tests with chrome headless via [Karma](https://karma-runner.github.io).


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## PDF Export: lib usage and precisions
For the PDF export, we use the [JsPDF lib](https://github.com/MrRio/jsPDF)<br/><br/>
This lib allows you to export pdf, mainly with custom elements and images.<br/>
For the purpose of this project, we needed to export batches of HTML to PDF. JsPDF allows to do so by using the [HTML method](http://raw.githack.com/MrRio/jsPDF/master/docs/module-html.html#~html).<br/>
This method was new at the time of the development of the project, and presented the following problem:<br/><br/>
The Html2Canvas dependency that it needs made this method not work properly by using the last version. It is needed to use a [specific version to use it](https://github.com/MrRio/jsPDF/issues/2487).<br/>
Even so, by using this specific version, a bug was recurrent, and needed to be patch. In this regard, the patch has been made, and the patched dependency is stocked in assets/html2canvas/html2canvas.js
